%---%
    \subsection{Basic sets}

    A \textbf{set} is an unordered collection of distinct items. For example:
    
    \begin{center}
        $ A = \{ 1, 2, 3, 4 \} $
        \tab
        $ B = \{ \$2.99, \$3.95 \} $ 
        \tab
        $ C = \{ red, orange, yellow \}$
    \end{center}
    
    Because a set is \textbf{unordered}, the sets $\{ 1, 2, 3, 4 \}$ and $\{ 2, 4, 1, 3 \}$ are considered equivalent.
    Additionally, $ \{ 1, 2 \} $ and $ \{ 1, 1, 2 \} $ are also considered equivalent since duplicate values don't matter.
    
    ~\\
    The individual items within a set are called \textbf{elements}.
    
    \begin{center}
        \begin{tabular}{ c c c c c c c }
            $D$         & $=$   & $\{$  & $a$,          & $b$,              & $c$           & $\}$  \\
            Set name    &       &       & First element & Second element    & Third element
        \end{tabular}
    \end{center}
    
    When we list all elements of a set like above, it's known as the \textbf{roster method} of describing sets.
    
    A set that contains no elements is known as an \textbf{empty set}, and can be written with either
    
    \begin{center}
        $\{ \}$ \tab or \tab $\emptyset$
    \end{center}
    
    %---%
    \subsection{Element in a set $\in$, not in a set $\not\in$}
    
    We can specify whether an element is or is not a member of a given set with these symbols.
    For example, let's take these sets:
    
    \begin{center}
        $A = \{ -1, 0, 1, 2 \}$ \tab $B = \{1, 2, 3\}$ \tab $C = \{2, 4, 6\}$
    \end{center}
    
    We can show that $1 \in A$ (1 is in the set $A$), $1 \in B$ (1 is in the set $B$), 
    and $1 \not\in C$ (1 is not in the set $C$).
    
    %---%
    \subsection{Set cardinality}
    
    The \textbf{cardinality} of a set is the count of distinct items within a set.
    Given a set $A$, we can write ``the cardinality of $A$'' as $|A|$.
    
    \begin{center}
        \begin{tabular}{l l l}
            \textbf{Set}                & \textbf{Cardinality} \\ \hline
            $A = \emptyset$             & $|A| = 0$ \\
            $B = \{ 1, 2, 3 \}$         & $|B| = 3$ \\
            $C = \{ cat, cat, dog \}$   & $|C| = 2$ & We only count each \underline{distinct} item. \\
            $\mathbb{Z}$                & $|\mathbb{Z}| = $ infinite
        \end{tabular}
    \end{center}
    
    Note that in some books, $n(A)$ is the notation used to indicate the cardinality of set $A$.
    
    %---%
    \subsection{Common sets $\mathbb{Z}$, $\mathbb{N}$, $\mathbb{Q}$ and $\mathbb{R}$}
    
    In mathematics you will see the following sets pretty regularly:
    
    \begin{center}
        \begin{tabular}{ l p{12cm} }
            $\mathbb{Z}$    & The set of all \textbf{integers} \\
                            & Integers are whole numbers, including positive, negative, and zero. \\
                            & Example: $\{ ..., -2, -1, 0, 1, 2, ... \}$ \\ \\
            $\mathbb{N}$    & The set of all \textbf{natural numbers} \\
                            & Natural numbers are ``counting numbers'', whole numbers 0 and up. \\ 
                            & Example: $\{0, 1, 2, 3, ...\}$ \\ \\
            $\mathbb{Q}$    & The set of all \textbf{rational numbers} \\ 
                            & Rational numbers are numbers that can be represented as a ratio. \\
                            & Example values: $\frac{1}{2}, \frac{5}{1}, \frac{5}{3}$ \\ \\
            $\mathbb{R}$    & The set of all \textbf{real numbers} \\
                            & Real numbers, which include the sets above, plus numbers with unending strings of digits after a decimal point. \\
                            & Example values: $1, \frac{1}{2}, \pi, \sqrt{3}$
        \end{tabular}
    \end{center}
    
    You can further restrict these sets by adding a $+$ superscript to mean ``positive only'',
    or a $\geq 0$ superscript to mean ``non-negative''. ~\\ 
    For example: $\mathbb{Z}^{\geq 0}$, $\mathbb{Q}^{+}$
    
    %---%
    \subsection{Subsets}
    
    When working with multiple sets, we may be interested to inspect how
    two sets are related to each other in different ways.
    
    \begin{center}
        \begin{tabular}{l l}
            $A \subseteq B$ & $A$ is a \textbf{subset} of $B$ if all elements from $A$ are also in $B$. \\ \\
            $A \subset B$   & $A$ is a \textbf{proper subset} of $B$ if all elements from $A$ are in $B$, \\
                            & but also $A \neq B$ (they don't share \textit{all} of the same elements).     \\ \\
            $A = B$ & The two sets $A$ and $B$ are \textbf{equal} if all elements of $A$ are also in $B$. \\
                    & If $A \subseteq B$ and $B \subseteq A$, that means $A = B$. \\ \\
        \end{tabular}
    \end{center}
    
    \subsection{Universal set}
    
    When working with multiple sets, we use the \textbf{universal set} $U$ to be a set that contains
    \textit{all elements} of \textit{all sets}.
    
    \subsubsection{Sets of sets}
    
    A set can also contain a set \textit{within} it. For example, let's say we have a set of pizzas available at a restaurant:
    \begin{center}
        \{ cheese \}, \{ cheese, pepperoni \}, \{ cheese, olives, onions, tomatoes \} \\
        \{ cheese, pineapple, ham \}, \{ cheese, sausage, pepperoni, bacon \}
    \end{center}
    
    And we could have a set that contains which pizzas are vegetarian:
    \begin{center}
        $V =$ \{ \{ cheese \}, \{ cheese, olives, onions, tomatoes \} \}
    \end{center}
    
    A set can contain sets as well as non-set elements all in one:
    \begin{center}
        $V =$ \{ cookie, pasta, \{ cheese \}, \{ cheese, olives, onions, tomatoes \} \}
    \end{center}
    
    And, a set can even contain empty sets within it, so note that
    \begin{center}
        $\{ \{ \} \}$   \tab ...or can be written as... \tab $ \{ \emptyset \} $
    \end{center}
    is not the same as an empty set - it is a set of one element, and that one element is an empty set.
    
    
    %---%
    \subsection{Set-builder notation}
    For large sets or sets with an infinite amount of elements, it wouldn't
    make sense to try to define the set by writing out all elements. In this case,
    we can use \textbf{set-builder notation} to define a set.
    
    \subsection{Property-description style}
    
    Let's say we want to specify the set of all even integers.
    We have a starting point with $\mathbb{Z}$, but that includes everything.
    We can use the \textbf{property-description} of set-builder notation like this:
    
    \begin{center}
        $ \{ x \in \mathbb{Z} : x$ is even$ \} $
    \end{center}
    
    For the first part, we define the wider domain that our element $x$ belongs in.
    The second part descripts additional properties that restrict the elements that
    belong in the set we're building.
    
    \subsection{Form-description style}
    
    Now let's specify the set of all even integers, but using the \textbf{form-description} style:
    
    \begin{center}
        $ \{ 2x : x \in \mathbb{Z} \}$
    \end{center}
    
    This is more concise, and specifies the \textit{form} elements take in the set.
    If you're familiar with number theory, 2 times any integer is an even integer,
    so given any $x$ in the domain $\mathbb{Z}$, $2x$ will be even.
    
