In this section, we're going to look at laws for propositional logic
and how two propositional statements can be logically equivalent to each other.

\subsection{Logical Equivalences}

The following are logical equivalences we will be covering
.\footnote{
From https://en.wikipedia.org/wiki/Propositional\_calculus\#Basic\_and\_derived\_argument\_forms, 
https://en.wikipedia.org/wiki/Tautology\_(logic)\#Definition\_and\_examples,
https://en.wikipedia.org/wiki/Contradiction
}

\begin{center}
  \begin{tabular}{| l | c c c | } \hline
    Commutation         & $p \lor q$            & $\equiv$    & $q \lor p$
    \\                  & $p \land q$           & $\equiv$    &$q \land p$         
    \\ \hline
    Association         & $p \lor (q \lor r)$   & $\equiv$    & $(p \lor q) \lor r$
    \\                  & $p \land (q \land r)$ & $\equiv$    & $(p \land q) \land r$
    \\ \hline
    Distribution        & $p \land (q \lor r)$  & $\equiv$    & $(p \land q) \lor (p \land r)$
    \\                  & $p \lor (q \land r)$  & $\equiv$    & $(p \lor q) \land (p \lor r)$
    \\ \hline
    DeMorgan's Theorem  & $\neg(p \land q)$     & $\equiv$    & $\neg p \lor \neg q$
    \\                  & $\neg(p \lor q)$      & $\equiv$    & $\neg p \land \neg q$
    \\ \hline
    Double negative     & $p$                   & $\equiv$    & $\neg(\neg p)$
    \\ \hline
    Tautology           & $p \lor \neg p$       & $\equiv$    & true
    \\ \hline
    Contradiction       & $p \land \neg p$      & $\equiv$    & false
    \\ \hline
  \end{tabular}
\end{center}

\newpage
\subsubsection{Law of Commutation}
  The Commutative Property shows that you can rearrange the
  propositional variables in a statement like $p \lor q$
  to $q \lor p$ and they will be logically equivalent.
  This is similar to the commutative property in algebra, where something
  like $a + b = b + a$.
  \begin{center}
    \begin{tabular}{ c c c  }
      $p \lor q$            & $\equiv$    & $q \lor p$
      \\
      $p \land q$           & $\equiv$    &$q \land p$
    \end{tabular}
  \end{center}
  
  \paragraph{Example:}
    ~\\ Given the propositional variables:
    
    \begin{itemize}
      \item   $c$: I have a computer
      \item   $p$: I have a phone
    \end{itemize} 
    
    The statement $c \land p$, ``I have a computer and I have a phone'',
    is logically equivalent to $p \land c$, ``I have a phone and I have a computer.''

\hrulefill
\subsubsection{Law of Association}
  The Associative Property shows that, when working with all $\lor$ or all $\land$ in
  a propositional statement (not $\lor$ and $\land$ mixed together),
  evaluating any two propositional variables can be done in any order.
  In algebra, this is like $(1 + 2) + 3 = 1 + (2 + 3)$ or $1 \cdot (2 \cdot 3) = (1 \cdot 2) \cdot 3)$.

  \begin{center}
    \begin{tabular}{ c c c  }
      $p \lor (q \lor r)$   & $\equiv$    & $(p \lor q) \lor r$
      \\
      $p \land (q \land r)$ & $\equiv$    & $(p \land q) \land r$
    \end{tabular}
  \end{center}
  
  \paragraph{Example:}
    ~\\ Given the propositional variables:
    
    \begin{itemize}
      \item   $c$: I go to class
      \item   $n$: I take notes
      \item   $s$: I study
    \end{itemize}
    
    The statement $c \land (n \land s)$, ``I go to class and I take notes and I study.'',
    is logically equivalent to $(c \land n) \land s$, ``I go to class and I take notes and I study.''

\newpage
\subsubsection{Law of Distribution}
  The Distributive Property shows that when we have a statement like
  $p \lor (q \land r)$, we expand the statement by ``distributing''
  the outer variable to both of the internal variables: $(p \lor q) \land (p \land r)$.
  In algebra, we have the distributive property with multiplication, like
  $x(y+z) = xy + xz$.

  \begin{center}
    \begin{tabular}{ c c c  }
      $p \land (q \lor r)$  & $\equiv$    & $(p \land q) \lor (p \land r)$
      \\
      $p \lor (q \land r)$  & $\equiv$    & $(p \lor q) \land (p \lor r)$
    \end{tabular}
  \end{center}
  
  \paragraph{Example:}
    ~\\ Given the propositional variables:
    
    \begin{itemize}
      \item   $b$: We have bread
      \item   $m$: We have meat
      \item   $c$: We have cheese
    \end{itemize}
    
    The statement $b \land (m \lor c)$, ``We have bread AND we have meat or cheese'',
    is logically equivalent to $(b \land m) \lor (b \land c)$, ``We have bread and meat OR we have bread and cheese.''
    
    ~\\ (And rememeber that our ``OR'' is NOT EXCLUSIVE, so in this case, there could be bread, meat, AND cheese.)


\newpage
\subsubsection{DeMorgan's Law}
    DeMorgan's laws describe the result of the negation of the statement $(p \lor q)$,
    which comes out to $\neg p \land \neg q$,
    and the negation of the statement $(p \land q)$, which comes out to $\neg p \lor \neg q$.

  \begin{center}
    \begin{tabular}{ c c c  }
      $\neg(p \land q)$     & $\equiv$    & $\neg p \lor \neg q$
      \\
      $\neg(p \lor q)$      & $\equiv$    & $\neg p \land \neg q$
    \end{tabular}
  \end{center}
  
  \paragraph{Example 1:}
    ~\\ Given the propositional variables:
    
    \begin{itemize}
      \item   $o$: The printer is online
      \item   $p$: The printer has paper
    \end{itemize}
    
    The statement $\neg(o \land p)$, ``It is NOT true that - the printer is online and has paper'',
    is logically equivalent to $\neg o \lor \neg p$, ``The printer is not online or the printer does not have paper.''
    
    ~\\ We can think of the printer as operational if $o \land p$ is true. If it's false, then $\neg (o \land p)$ is true,
    which gives us $\neg o \lor \neg p$ - basically, either the printer IS NOT online or it DOESN'T have paper, therefore it is out of order.
    
    
  \paragraph{Example 2:}
    ~\\ Given the propositional variables:
    
    \begin{itemize}
      \item   $c$: I have a cat
      \item   $d$: I have a dog
    \end{itemize}
    
    The statement $\neg(c \lor d)$, ``It is NOT true that - I have a cat or a dog'',
    is logically equivalent to $\neg c \land \neg d$, ``I don't have a cat and I don't have a dog.''
    
    ~\\ If the statement ``I have a cat or a dog'' is false, that means I have neither a cat nor a dog, so $\neg c \land \neg d$.
    

\newpage
\subsubsection{Tautologies and Contradictions}
  Sometimes we can end up building a propositional statement that is a
  \textbf{tautology} (true for all states of $p$, $q$, etc.),
  or a \textbf{contradiction} (false for all states of $p$, $q$, etc.).
  
  
  \begin{figure}[h]
      \centering
      \begin{subfigure}{.5\textwidth}
        A really basic tautology is $p \lor \neg p$ -- we're saying
        the statement is true if $p$ is true or... if $p$ is false... so
        either way, the result of the statement is true:
      \end{subfigure}%
      \begin{subfigure}{.5\textwidth}
       \centering
        \begin{tabular}{ | c | c || c | } \hline
          $p$       & $\neg p$      & $p \lor \neg q$         \\ \hline
          \true     & \false        & \true                   \\ \hline
          \true     & \false        & \true                   \\ \hline
          \false    & \true         & \true                   \\ \hline
          \false    & \true         & \true                   \\ \hline
        \end{tabular}
      \end{subfigure}
  \end{figure}
  
  
  \begin{itemize}
    \item   ``I am either \textbf{happy} or \textbf{not happy}.''
    \item   ``Either \textbf{she has a cat and a dog} or \textbf{she doesn't have a cat and doesn't have a dog}''
  \end{itemize}
  

  \begin{figure}[h]
      \centering
      \begin{subfigure}{.5\textwidth}
        Similarly, a really basic contradiction is $p \land \neg p$ --
        we're saying $p$ must be true and $\neg p$ must be true, which is
        impossible, so the statement is always false:
      \end{subfigure}%
      \begin{subfigure}{.5\textwidth}
       \centering
        \begin{tabular}{ | c | c || c | } \hline
          $p$       & $\neg p$      & $p \land \neg q$        \\ \hline
          \true     & \false        & \false                  \\ \hline
          \true     & \false        & \false                  \\ \hline
          \false    & \true         & \false                  \\ \hline
          \false    & \true         & \false                  \\ \hline
        \end{tabular}
      \end{subfigure}
  \end{figure}


  \begin{itemize}
    \item   ``The \textbf{printer is online} and the \textbf{printer is offline}.''
    \item   ``I \textbf{have a cat and a dog} and I don't \textbf{have a cat or a dog}.''
  \end{itemize}



  \newpage
  \paragraph{Example tautology:}
  
  In programming, perhaps a tautological logic error could happen from some logic like this:
  
\begin{lstlisting}[style=code]
// CHECK FOR VALID INPUT
if ( min <= x || x <= max )
{
  // Always true
}
\end{lstlisting}

  Assuming min is less than max and the user has entered a number, all numbers would be true:
  
    \begin{center}
        \begin{tikzpicture}
         
            \fill[red!25] (2,-0.2) rectangle (5,0.2);
            \fill[blue!25] (-2,-0.2) rectangle (-5,0.2);
            \fill[purple!25] (-2,-0.2) rectangle (2,0.2);
            
            
            \draw (-5,0) -- (5,0);
            \draw (-4.8,.2) -- (-5,0); \draw (-4.8,-.2) -- (-5,0);
            \draw (4.8,.2) -- (5,0); \draw (4.8,-.2) -- (5,0);
            
            \draw (0, -0.2) -- (0, 0.2);
            \draw (1, -0.2) -- (1, 0.2);
            \draw (2, -0.2) -- (2, 0.2);
            \draw (3, -0.2) -- (3, 0.2);
            \draw (4, -0.2) -- (4, 0.2);
            \draw (-1, -0.2) -- (-1, 0.2);
            \draw (-2, -0.2) -- (-2, 0.2);
            \draw (-3, -0.2) -- (-3, 0.2);
            \draw (-4, -0.2) -- (-4, 0.2);
            
            \draw[-stealth,ultra thick,blue] (2,0.3) -- (-5,0.3);
            \draw[-stealth,ultra thick,red] (-2,-0.3) -- (5,-0.3);
            
            \node[red] at (-2, -0.6) {min}; 
            \node[blue] at (2, 0.6) {max}; 
            \node at (0, 1) { $min \leq x$ OR $x \leq max$ };
            
        \end{tikzpicture}
    \end{center}
    
    The logic error here, is that if we wanted to restrict the region between min and max, we would
    need to use AND to make sure it is ``greater than the min'' AND ``less than the max'':
    
    \begin{center}
        \begin{tikzpicture}
         
            %\fill[red!25] (2,-0.2) rectangle (5,0.2);
            %\fill[blue!25] (-2,-0.2) rectangle (-5,0.2);
            \fill[purple!25] (-2,-0.2) rectangle (2,0.2);
            
            
            \draw (-5,0) -- (5,0);
            \draw (-4.8,.2) -- (-5,0); \draw (-4.8,-.2) -- (-5,0);
            \draw (4.8,.2) -- (5,0); \draw (4.8,-.2) -- (5,0);
            
            \draw (0, -0.2) -- (0, 0.2);
            \draw (1, -0.2) -- (1, 0.2);
            \draw (2, -0.2) -- (2, 0.2);
            \draw (3, -0.2) -- (3, 0.2);
            \draw (4, -0.2) -- (4, 0.2);
            \draw (-1, -0.2) -- (-1, 0.2);
            \draw (-2, -0.2) -- (-2, 0.2);
            \draw (-3, -0.2) -- (-3, 0.2);
            \draw (-4, -0.2) -- (-4, 0.2);
            
            \draw[-stealth,ultra thick,blue] (2,0.3) -- (-5,0.3);
            \draw[-stealth,ultra thick,red] (-2,-0.3) -- (5,-0.3);
            
            \node[red] at (-2, -0.6) {min}; 
            \node[blue] at (2, 0.6) {max}; 
            
            \node at (0, 1) { $min \leq x$ AND $x \leq max$ };
            
        \end{tikzpicture}
    \end{center}
  
\begin{lstlisting}[style=code]
// CHECK FOR VALID INPUT
if ( min <= x && x <= max )
{
  // Only true when x is between min and max
}
\end{lstlisting}

  \newpage
  \paragraph{Example contradiction:}

  Another programming based logical statement would be:
  
\begin{lstlisting}[style=code]
// CHECK FOR INVALID INPUT
if ( x < min && x > max )
{
  // Always false
}
\end{lstlisting}

  In this case, no number $x$ is both less than the min and greater than the max at the same time
  (assuming $min < max$).
  
    \begin{center}
        \begin{tikzpicture}
         
            \fill[blue!25] (2,-0.2) rectangle (5,0.2);
            \fill[red!25] (-2,-0.2) rectangle (-5,0.2);
                        
            \draw (-5,0) -- (5,0);
            \draw (-4.8,.2) -- (-5,0); \draw (-4.8,-.2) -- (-5,0);
            \draw (4.8,.2) -- (5,0); \draw (4.8,-.2) -- (5,0);
            
            \draw (0, -0.2) -- (0, 0.2);
            \draw (1, -0.2) -- (1, 0.2);
            \draw (2, -0.2) -- (2, 0.2);
            \draw (3, -0.2) -- (3, 0.2);
            \draw (4, -0.2) -- (4, 0.2);
            \draw (-1, -0.2) -- (-1, 0.2);
            \draw (-2, -0.2) -- (-2, 0.2);
            \draw (-3, -0.2) -- (-3, 0.2);
            \draw (-4, -0.2) -- (-4, 0.2);
            
            \draw[-stealth,ultra thick,blue] (2,0.3) -- (5,0.3);
            \draw[-stealth,ultra thick,red] (-2,-0.3) -- (-5,-0.3);
            
            \node[red] at (-2, -0.6) {min}; 
            \node[blue] at (2, 0.6) {max}; 
            \node at (0, 1) { $x < min$ AND $x > max$ };
            
        \end{tikzpicture}
    \end{center}
    
    In this case, we'd need to use an OR statement to see if our
    $x$ variable is OUTSIDE the valid range.
    
    \begin{center}
        \begin{tikzpicture}
         
            \fill[purple!25] (2,-0.2) rectangle (5,0.2);
            \fill[purple!25] (-2,-0.2) rectangle (-5,0.2);
            
            
            \draw (-5,0) -- (5,0);
            \draw (-4.8,.2) -- (-5,0); \draw (-4.8,-.2) -- (-5,0);
            \draw (4.8,.2) -- (5,0); \draw (4.8,-.2) -- (5,0);
            
            \draw (0, -0.2) -- (0, 0.2);
            \draw (1, -0.2) -- (1, 0.2);
            \draw (2, -0.2) -- (2, 0.2);
            \draw (3, -0.2) -- (3, 0.2);
            \draw (4, -0.2) -- (4, 0.2);
            \draw (-1, -0.2) -- (-1, 0.2);
            \draw (-2, -0.2) -- (-2, 0.2);
            \draw (-3, -0.2) -- (-3, 0.2);
            \draw (-4, -0.2) -- (-4, 0.2);
            
            \draw[-stealth,ultra thick,blue] (2,0.3) -- (5,0.3);
            \draw[-stealth,ultra thick,red] (-2,-0.3) -- (-5,-0.3);
            
            \node[red] at (-2, -0.6) {min}; 
            \node[blue] at (2, 0.6) {max}; 
            
            \node at (0, 1) { $x < min$ OR $x > max$ };
            
        \end{tikzpicture}
    \end{center}

\begin{lstlisting}[style=code]
// CHECK FOR INVALID INPUT
if ( x < min || x > max )
{
  // True when x is outside the [min,max] range
}
\end{lstlisting}

