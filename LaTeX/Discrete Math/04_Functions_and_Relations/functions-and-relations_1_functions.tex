

\chapter{Notes: Functions and Relations}

    %---%
    \section{Introduction to Functions}

    A \textbf{function} is something with a set of \textbf{inputs} that gives some \textbf{output}
    based on what is input.

    In programming, a function might look like this:

\begin{lstlisting}[style=code]
int Area( int w, int h )
{
    return w * h;
}
\end{lstlisting}

    And in math, it may look like this:

    $$ A( w, h ) = w \cdot h $$

    In the programming example, the inputs and outputs are integers,
    with values coming from the set of all integers, $\mathbb{Z}$.

    ~\\
    For this section, we will define a function like this:

    $$ f : A \to B $$

    where $f$ is the function name, $A$ is the \textbf{domain} (the set where
    values for inputs come from), and $B$ is the \textbf{codomain} (the set where
    values for the output comes from).

    The internals of the function
    \textbf{map} the input value(s) from $A$ to an output value from $B$,
    such as how we take two inputs in the previous function, $w$ and $h$,
    and map those to some output by computing $w \cdot h$, resulting in
    another integer.
    ~\\

    Defining a function requires a \textbf{name for the function}, specifying the \textbf{domain} and \textbf{codomain} sets,
    and the \textbf{rule} that links an input to an output.

    The \textbf{rule} can be a set of ordered pairs, explicitly stating all maps between elements, such as:

    $$ \{ (1, 2), (2, 4), (3, 6) \} $$

    or it could be defined as a function, like:

    $$ f(x) = 2x $$

    \subsection{Function Diagrams}

    For a function with a discrete (aka finite) amount of elements in the
    domain and codomain, we can diagram the mapping from its potential inputs
    to its potential outputs.~\\

    For example, let's have $A = \{ 1, 2, 3 \}$ and $B = \{2, 4, 6\}$,
    and our function $f : A \to B$ is defined as $f(x) = 2x$. We can diagram
    it as such:

    \begin{center}
        \begin{tikzpicture}
            % Define points
            \coordinate (1) at (0.5, 0.5);          \coordinate (2) at (0.5, 1.0);          \coordinate (3) at (0.5, 1.5);
            \coordinate (a) at (3.5, 0.5);          \coordinate (b) at (3.5, 1.0);          \coordinate (c) at (3.5, 1.5);

            % Rectangle
            \draw (0,0) -- (1,0) -- (1,2.0) -- (0,2.0) -- (0,0); \draw (3,0) -- (4,0) -- (4,2.0) -- (3,2.0) -- (3,0);

            % Node circles
            \filldraw (1) circle (1pt) node[left] {1};
            \filldraw (2) circle (1pt) node[left] {2};
            \filldraw (3) circle (1pt) node[left] {3};

            \filldraw (a) circle (1pt) node[right] {2};
            \filldraw (b) circle (1pt) node[right] {4};
            \filldraw (c) circle (1pt) node[right] {6};

            % Domain/Codomain labels
            \node at (0.5, -0.5) {Domain};
            \node at (3.5, -0.5) {Codomain};

            % Directional arrows
            \draw[-stealth,ultra thick,blue] (1) -- (a);
            \draw[-stealth,ultra thick,orange] (2) -- (b);
            \draw[-stealth,ultra thick,purple] (3) -- (c);
        \end{tikzpicture}
    \end{center}

    \newpage
    \subsection{Properties of Functions}

    \subsubsection{Function}
    A function is only valid if \textbf{each input is associated with exactly one output}. ~\\


    Recall previous math courses that taught the \textit{vertical line test} for functions,
    where if a vertical line intersects a graphed item \textit{more than once}, it indicates
    that the graphed item is not a function.
    ~\\

    \begin{tabular}{p{6cm} p{6cm}}
        \begin{tikzpicture}
            \draw[gray] (2, 0) -- (-2, 0);
            \draw[gray] (0, -2) -- (0, 2);
            \node at (2.5, 0) {$x$};
            \node at (0, 2.5) {$y$};

            \draw (0,0) parabola (1.5,2.25) ;
            \draw (0,0) parabola (-1.5,2.25) ;

            \draw[red, dashed] (1, 2) -- (1, -2);
            \draw[fill=red,red] (1, 1) circle (2pt);
        \end{tikzpicture}
        &
        \begin{tikzpicture}
            \draw[gray] (2, 0) -- (-2, 0);
            \draw[gray] (0, -2) -- (0, 2);
            \node at (2.5, 0) {$x$};
            \node at (0, 2.5) {$y$};
            \draw [black, thick, domain=0:2] plot (\x, {sqrt(\x)});
            \draw [black, thick, domain=0:2] plot (\x, -{sqrt(\x)});

            \draw[red, dashed] (1, 2) -- (1, -2);
            \draw[fill=red,red] (1, 1) circle (2pt);
            \draw[fill=red,red] (1, -1) circle (2pt);
        \end{tikzpicture}
        \\
        Function
        &
        Not a function
    \end{tabular}

    \subsubsection{Onto}
    A function is \textbf{onto} if \textbf{every element of the codomain is an output}.
    This means that there \textit{can't} be a number in the codomain that isn't mapped to. ~\\
    Graphically, it means that each element in the codomain \underline{is pointed to} by at least one arrow.

    \begin{center}
        \begin{tikzpicture}
            % Define points
            \coordinate (1) at (0.5, 0.5);
            \coordinate (3) at (0.5, 1.5);
            \coordinate (a) at (3.5, 0.5);
            \coordinate (b) at (3.5, 1.0);
            \coordinate (c) at (3.5, 1.5);

            % Rectangle
            \draw (0,0) -- (1,0) -- (1,2.0) -- (0,2.0) -- (0,0); \draw (3,0) -- (4,0) -- (4,2.0) -- (3,2.0) -- (3,0);

            % Node circles
            \filldraw (1) circle (1pt) node[left] {1};
            \filldraw (3) circle (1pt) node[left] {3};

            \filldraw (a) circle (1pt) node[right] {2};
            \filldraw (b) circle (1pt) node[right] {4};
            \filldraw (c) circle (1pt) node[right] {6};

            % Domain/Codomain labels
            \node at (0.5, -0.5) {Domain};
            \node at (3.5, -0.5) {Codomain};

            % Directional arrows
            \draw[-stealth,ultra thick,blue] (1) -- (a);
            \draw[-stealth,ultra thick,purple] (3) -- (c);
        \end{tikzpicture}
        ~\\
        Not onto; nothing maps to 4. ~\\

        \begin{tikzpicture}
            % Define points
            \coordinate (1) at (0.5, 0.5);          \coordinate (2) at (0.5, 1.0);          \coordinate (3) at (0.5, 1.5);
            \coordinate (a) at (3.5, 0.5);          \coordinate (b) at (3.5, 1.0);          \coordinate (c) at (3.5, 1.5);

            % Rectangle
            \draw (0,0) -- (1,0) -- (1,2.0) -- (0,2.0) -- (0,0); \draw (3,0) -- (4,0) -- (4,2.0) -- (3,2.0) -- (3,0);

            % Node circles
            \filldraw (1) circle (1pt) node[left] {1};
            \filldraw (2) circle (1pt) node[left] {2};
            \filldraw (3) circle (1pt) node[left] {3};

            \filldraw (a) circle (1pt) node[right] {a};
            \filldraw (b) circle (1pt) node[right] {b};
            \filldraw (c) circle (1pt) node[right] {c};

            % Domain/Codomain labels
            \node at (0.5, -0.5) {Domain};
            \node at (3.5, -0.5) {Codomain};

            % Directional arrows
            \draw[-stealth,ultra thick,blue] (1) -- (a);
            \draw[-stealth,ultra thick,orange] (2) -- (c);
            \draw[-stealth,ultra thick,purple] (3) -- (c);
        \end{tikzpicture}
        ~\\
        Not onto; nothing maps to $b$.
    \end{center}

    \subsubsection{One-to-one}
    A function is \textbf{one-to-one} if \textbf{no element of the codomain is the output of more than one input.}
    Graphically, this means that each output in the codomain is \underline{being pointed to by no more than one arrow.}

    \begin{center}
        \begin{tikzpicture}
            % Define points
            \coordinate (1) at (0.5, 0.5);          \coordinate (2) at (0.5, 1.0);          \coordinate (3) at (0.5, 1.5);
            \coordinate (a) at (3.5, 0.5);          \coordinate (b) at (3.5, 1.0);          \coordinate (c) at (3.5, 1.5);

            % Rectangle
            \draw (0,0) -- (1,0) -- (1,2.0) -- (0,2.0) -- (0,0); \draw (3,0) -- (4,0) -- (4,2.0) -- (3,2.0) -- (3,0);

            % Node circles
            \filldraw (1) circle (1pt) node[left] {1};
            \filldraw (2) circle (1pt) node[left] {2};
            \filldraw (3) circle (1pt) node[left] {3};

            \filldraw (a) circle (1pt) node[right] {a};
            \filldraw (b) circle (1pt) node[right] {b};
            \filldraw (c) circle (1pt) node[right] {c};

            % Domain/Codomain labels
            \node at (0.5, -0.5) {Domain};
            \node at (3.5, -0.5) {Codomain};

            % Directional arrows
            \draw[-stealth,ultra thick,blue] (1) -- (a);
            \draw[-stealth,ultra thick,orange] (2) -- (c);
            \draw[-stealth,ultra thick,purple] (3) -- (c);
        \end{tikzpicture}
        ~\\
        Not one-to-one; $c$ is pointed to by two different inputs.
    \end{center}

    \subsubsection{Invertible}
    If a function is \textbf{both onto and one-to-one}, then it is also \textbf{invertible}.
    This means that if you take the inverse of the function, the inverse will also be a function
    (each element of the codomain is mapped to by one and only one element of the domain).

    \subsubsection{Function inverse}
    The \textbf{inverse} of a function $f : A \to B$ is written as $f^-1 : B \to A$
    switches the domain and codomain of the original function, with the mappings reversed.
    ~\\

    Example: Given $A = \{ 1, 2, 3 \}$, $B = \{ a, b, c \}$, and the rule $\{ (1, c), (2, a), (3, b) \}$,
    we can diagram it like this:

    \begin{center}
        \begin{tikzpicture}
            % Define points
            \coordinate (1) at (0.5, 0.5);          \coordinate (2) at (0.5, 1.0);          \coordinate (3) at (0.5, 1.5);
            \coordinate (a) at (3.5, 0.5);          \coordinate (b) at (3.5, 1.0);          \coordinate (c) at (3.5, 1.5);

            % Rectangle
            \draw (0,0) -- (1,0) -- (1,2.0) -- (0,2.0) -- (0,0); \draw (3,0) -- (4,0) -- (4,2.0) -- (3,2.0) -- (3,0);

            % Node circles
            \filldraw (1) circle (1pt) node[left] {1};
            \filldraw (2) circle (1pt) node[left] {2};
            \filldraw (3) circle (1pt) node[left] {3};

            \filldraw (a) circle (1pt) node[right] {a};
            \filldraw (b) circle (1pt) node[right] {b};
            \filldraw (c) circle (1pt) node[right] {c};

            % Domain/Codomain labels
            \node at (0.5, -0.5) {Domain};
            \node at (3.5, -0.5) {Codomain};

            % Directional arrows
            \draw[-stealth,ultra thick,blue] (1) -- (c);
            \draw[-stealth,ultra thick,orange] (2) -- (a);
            \draw[-stealth,ultra thick,purple] (3) -- (b);
        \end{tikzpicture}
    \end{center}

    And the inverse, $f^-1 : B \to A$ will have the rule $ \{ (c, 1), (a, 2), (b, 3) \}$
    and be diagramed like this:

    \begin{center}
        \begin{tikzpicture}
            % Define points
            \coordinate (a) at (0.5, 0.5);          \coordinate (b) at (0.5, 1.0);          \coordinate (c) at (0.5, 1.5);
            \coordinate (1) at (3.5, 0.5);          \coordinate (2) at (3.5, 1.0);          \coordinate (3) at (3.5, 1.5);

            % Rectangle
            \draw (0,0) -- (1,0) -- (1,2.0) -- (0,2.0) -- (0,0); \draw (3,0) -- (4,0) -- (4,2.0) -- (3,2.0) -- (3,0);

            % Node circles
            \filldraw (1) circle (1pt) node[right] {1};
            \filldraw (2) circle (1pt) node[right] {2};
            \filldraw (3) circle (1pt) node[right] {3};

            \filldraw (a) circle (1pt) node[left] {a};
            \filldraw (b) circle (1pt) node[left] {b};
            \filldraw (c) circle (1pt) node[left] {c};

            % Domain/Codomain labels
            \node at (0.5, -0.5) {Domain};
            \node at (3.5, -0.5) {Codomain};

            % Directional arrows
            \draw[-stealth,ultra thick,blue]    (c) -- (1);
            \draw[-stealth,ultra thick,orange]  (a) -- (2);
            \draw[-stealth,ultra thick,purple]  (b) -- (3);
        \end{tikzpicture}
    \end{center}

    \newpage
    \subsection{Illustrating onto, one-to-one, and invertibility}

    \paragraph{Functions that are not onto:}
    \begin{center}
        \begin{tabular}{p{6cm} p{6cm}}
            \textbf{Original} & \textbf{Inverse} \\
            \begin{tikzpicture}
                % Define points
                \coordinate (1) at (0.5, 0.5);
                \coordinate (3) at (0.5, 1.5);
                \coordinate (a) at (3.5, 0.5);
                \coordinate (b) at (3.5, 1.0);
                \coordinate (c) at (3.5, 1.5);

                % Rectangle
                \draw (0,0) -- (1,0) -- (1,2.0) -- (0,2.0) -- (0,0); \draw (3,0) -- (4,0) -- (4,2.0) -- (3,2.0) -- (3,0);

                % Node circles
                \filldraw (1) circle (1pt) node[left] {1};
                \filldraw (3) circle (1pt) node[left] {3};

                \filldraw (a) circle (1pt) node[right] {2};
                \filldraw (b) circle (1pt) node[right] {4};
                \filldraw (c) circle (1pt) node[right] {6};

                % Domain/Codomain labels
                \node at (0.5, -0.5) {Domain};
                \node at (3.5, -0.5) {Codomain};

                % Directional arrows
                \draw[-stealth,ultra thick,blue] (1) -- (a);
                \draw[-stealth,ultra thick,purple] (3) -- (c);
            \end{tikzpicture}
            &
            \begin{tikzpicture}
                % Define points
                \coordinate (1) at (3.5, 0.5);
                \coordinate (3) at (3.5, 1.5);
                \coordinate (a) at (0.5, 0.5);
                \coordinate (b) at (0.5, 1.0);
                \coordinate (c) at (0.5, 1.5);

                % Rectangle
                \draw (0,0) -- (1,0) -- (1,2.0) -- (0,2.0) -- (0,0); \draw (3,0) -- (4,0) -- (4,2.0) -- (3,2.0) -- (3,0);

                % Node circles
                \filldraw (1) circle (1pt) node[right] {1};
                \filldraw (3) circle (1pt) node[right] {3};

                \filldraw (a) circle (1pt) node[left] {2};
                \filldraw (b) circle (1pt) node[left] {4};
                \filldraw (c) circle (1pt) node[left] {6};

                % Domain/Codomain labels
                \node at (0.5, -0.5) {Domain};
                \node at (3.5, -0.5) {Codomain};

                % Directional arrows
                \draw[-stealth,ultra thick,blue]    (a) -- (1);
                \draw[-stealth,ultra thick,purple] (c) -- (3);
            \end{tikzpicture}
            \\
            \checkmark Is a function &
            \xmark Is not a function
            \\
            \xmark Not onto &
            (Not all inputs have an output)
            \\
            \checkmark One-to-one
            \\
            \xmark Not invertible
        \end{tabular}
    \end{center}

    \paragraph{Functions that are not one-to-one:}
    \begin{center}
        \begin{tabular}{p{6cm} p{6cm}}
            \textbf{Original} & \textbf{Inverse} \\
            \begin{tikzpicture}
                % Define points
                \coordinate (1) at (0.5, 0.5);
                \coordinate (2) at (0.5, 1.0);
                \coordinate (3) at (0.5, 1.5);
                \coordinate (a) at (3.5, 0.5);
                \coordinate (c) at (3.5, 1.5);

                % Rectangle
                \draw (0,0) -- (1,0) -- (1,2.0) -- (0,2.0) -- (0,0); \draw (3,0) -- (4,0) -- (4,2.0) -- (3,2.0) -- (3,0);

                % Node circles
                \filldraw (1) circle (1pt) node[left] {1};
                \filldraw (2) circle (1pt) node[left] {2};
                \filldraw (3) circle (1pt) node[left] {3};

                \filldraw (a) circle (1pt) node[right] {2};
                \filldraw (c) circle (1pt) node[right] {6};

                % Domain/Codomain labels
                \node at (0.5, -0.5) {Domain};
                \node at (3.5, -0.5) {Codomain};

                % Directional arrows
                \draw[-stealth,ultra thick,blue] (1) -- (a);
                \draw[-stealth,ultra thick,orange] (2) -- (a);
                \draw[-stealth,ultra thick,purple] (3) -- (c);
            \end{tikzpicture}
            &
            \begin{tikzpicture}
                % Define points
                \coordinate (1) at (3.5, 0.5);
                \coordinate (2) at (3.5, 1.0);
                \coordinate (3) at (3.5, 1.5);
                \coordinate (a) at (0.5, 0.5);
                \coordinate (c) at (0.5, 1.5);

                % Rectangle
                \draw (0,0) -- (1,0) -- (1,2.0) -- (0,2.0) -- (0,0); \draw (3,0) -- (4,0) -- (4,2.0) -- (3,2.0) -- (3,0);

                % Node circles
                \filldraw (1) circle (1pt) node[right] {1};
                \filldraw (2) circle (1pt) node[right] {2};
                \filldraw (3) circle (1pt) node[right] {3};

                \filldraw (a) circle (1pt) node[left] {2};
                \filldraw (c) circle (1pt) node[left] {6};

                % Domain/Codomain labels
                \node at (0.5, -0.5) {Domain};
                \node at (3.5, -0.5) {Codomain};

                % Directional arrows
                \draw[-stealth,ultra thick,blue] (a) -- (1);
                \draw[-stealth,ultra thick,orange] (a) -- (2);
                \draw[-stealth,ultra thick,purple] (c) -- (3);
            \end{tikzpicture}
            \\
            \checkmark Is a function &
            \xmark Is not a function
            \\
            \checkmark Onto &
            (2 has two outputs;
            \\
            \xmark Not one-to-one &
            fails the vertical line test)
            \\
            \xmark Not invertible
        \end{tabular}
    \end{center}

    \paragraph{Functions that are onto and one-to-one:}
    \begin{center}
        \begin{tabular}{p{6cm} p{6cm}}
            \textbf{Original} & \textbf{Inverse} \\
        \begin{tikzpicture}
            % Define points
            \coordinate (1) at (0.5, 0.5);          \coordinate (2) at (0.5, 1.0);          \coordinate (3) at (0.5, 1.5);
            \coordinate (a) at (3.5, 0.5);          \coordinate (b) at (3.5, 1.0);          \coordinate (c) at (3.5, 1.5);

            % Rectangle
            \draw (0,0) -- (1,0) -- (1,2.0) -- (0,2.0) -- (0,0); \draw (3,0) -- (4,0) -- (4,2.0) -- (3,2.0) -- (3,0);

            % Node circles
            \filldraw (1) circle (1pt) node[left] {1};
            \filldraw (2) circle (1pt) node[left] {2};
            \filldraw (3) circle (1pt) node[left] {3};

            \filldraw (a) circle (1pt) node[right] {a};
            \filldraw (b) circle (1pt) node[right] {b};
            \filldraw (c) circle (1pt) node[right] {c};

            % Domain/Codomain labels
            \node at (0.5, -0.5) {Domain};
            \node at (3.5, -0.5) {Codomain};

            % Directional arrows
            \draw[-stealth,ultra thick,blue] (1) -- (c);
            \draw[-stealth,ultra thick,orange] (2) -- (a);
            \draw[-stealth,ultra thick,purple] (3) -- (b);
        \end{tikzpicture}
            &
            \begin{tikzpicture}
                % Define points
                \coordinate (a) at (0.5, 0.5);          \coordinate (b) at (0.5, 1.0);          \coordinate (c) at (0.5, 1.5);
                \coordinate (1) at (3.5, 0.5);          \coordinate (2) at (3.5, 1.0);          \coordinate (3) at (3.5, 1.5);

                % Rectangle
                \draw (0,0) -- (1,0) -- (1,2.0) -- (0,2.0) -- (0,0); \draw (3,0) -- (4,0) -- (4,2.0) -- (3,2.0) -- (3,0);

                % Node circles
                \filldraw (1) circle (1pt) node[right] {1};
                \filldraw (2) circle (1pt) node[right] {2};
                \filldraw (3) circle (1pt) node[right] {3};

                \filldraw (a) circle (1pt) node[left] {a};
                \filldraw (b) circle (1pt) node[left] {b};
                \filldraw (c) circle (1pt) node[left] {c};

                % Domain/Codomain labels
                \node at (0.5, -0.5) {Domain};
                \node at (3.5, -0.5) {Codomain};

                % Directional arrows
                \draw[-stealth,ultra thick,blue]    (c) -- (1);
                \draw[-stealth,ultra thick,orange]  (a) -- (2);
                \draw[-stealth,ultra thick,purple]  (b) -- (3);
            \end{tikzpicture}
            \\
            \checkmark Is a function &
            \checkmark Is a function
            \\
            \checkmark Onto &
            \checkmark Onto
            \\
            \checkmark One-to-one &
            \checkmark One-to-one
            \\
            \checkmark Invertible &
            \checkmark Invertible
        \end{tabular}
    \end{center}















