

    \newpage
    %---%
    \section{The Composition Operation}

    Let's say we are implementing a program that contains multiple functions,
    such as:
    
\begin{lstlisting}[style=code]
float GetCircleArea( float radius )
{
    return PI * radius * radius;
}

float GetRadius( float diameter )
{
    return diameter / 2;
}
\end{lstlisting}

    If we have a diameter and want to get the circle's area, we would
    call these functions like this:
    
\begin{lstlisting}[style=code]
float diameter = 10;
float area = GetCircleArea( GetRadius( diameter ) );
\end{lstlisting}

    The \texttt{GetRadius} function - the inner one - is called first.
    It returns \texttt{5}, and that is then passed into the
    \texttt{GetCircleArea} function, which then returns about
    \texttt{78.5398...}, which is $\pi r^2$.
    
    We had some information, and two functions that had to be strung together
    in order to get our final information:
    
    \begin{enumerate}
        \item   \texttt{GetRadius} mapped input 10 to output 5.
        \item   \texttt{GetCircleArea} mapped input 5 to output 78.5398
        \item   As a whole, the input 10 mapped to output 78.5398.
    \end{enumerate}
    
    This is an example of \textbf{function composition} - when 
    two functions are combined together with one being the inner function
    and one being the outer function.
    ~\\
    
    Perhaps in another math course, you've seen this before:
    
    \begin{center}
        $f(g(x))$ \tab or \tab $(f \circ g)(x)$
    \end{center}
    
    This is the notation we will use for function composition here.
    
    \newpage

    \subsection{Definition of Function Composition}
    
    \paragraph{Definition:}\footnote{Adapted from Discrete Mathmematics, Ensley \& Crawley, pg 268}
    Given two functions, $f : A \to B$ and $g : B \to C$,
    we can make a new function mapping $A \to C$ called
    $g(f(x))$ or $(g \circ f)(x)$. This is read as
    ``$g$ of $f$'', or ``the composition of $g$ with $f$.''
    ~\\
    
    Let's look at the circle area example before, but in mathematical terms:
    
    ~\\
    \begin{tabular}{l l}
        $r(d) = d/2$ & Radius function takes diameter $d$ \\ \\
        $a(r) = \pi r^2$ & Area function takes radius $r$
    \end{tabular}
    
    ~\\~\\
    Finding $a(r(10)):$
    
    \begin{enumerate}
        \item   The inner function is $r(10)$, start here:
            ~\\ $r(10) = 10/2 = 5$
        \item   Now find $a(5)$:
            ~\\ $a(5) = \pi 5^2 = \pi \cdot 25 \approx 78.5398$
    \end{enumerate}
    
    ~\\~\\
    Now let's create our new function - the function to get the area
    when we're given the diameter. This means we find $a(r(d))$, without
    having any values to plug in - the result will be a new function
    that maps diameter inputs $d$ to area outputs.
    
    ~\\~\\
    Finding $a(r(d)):$
    \begin{enumerate}
        \item   Inner function: $r(d) = d/2$.
        \item   Plug $d/2$ into the function $a(r) = \pi r^2$...
        \item   $a( d/2 ) = \pi (d/2)^2$
        \item   New function, $(a \circ r)(d) = \pi (\frac{d}{2})^2$,
                finds the area of a circle given a diameter.
    \end{enumerate}
    
    \newpage
    \subsection{Diagramming Function Composition}

    We can also visualize the new functions mappings via a function diagram.
    Let's say we have two functions $f : A \to B$, $g : B \to C$, 
    and we're going to create a third, $(g \circ f) : A \to C$.
    
    \begin{center}
        $f(x) = 2x$ \tab $g(x) = x + 3$ \tab $g(f(x)) = ?$ ~\\ ~\\
        $A = \{ 1, 2, 3\}$ \tab $B = \{2, 4, 6\}$ \tab $C = \{5, 7, 9\}$
    \end{center}
    
    Diagrams:
    
    ~\\
    \begin{tabular}{c c c}
        $f : A \to B$ & $g : B \to C$ & $(g \circ f) : A \to C$ \\ \\
        \begin{tikzpicture}
            \coordinate (rect1a) at (0, 0.5);
            \coordinate (rect1b) at (1, 0.5);
            \coordinate (rect1c) at (1, 3.5);
            \coordinate (rect1d) at (0, 3.5);
            
            \coordinate (rect2a) at (2, 0.5);
            \coordinate (rect2b) at (3, 0.5);
            \coordinate (rect2c) at (3, 3.5);
            \coordinate (rect2d) at (2, 3.5);
            
            \coordinate (node1a) at (0.5, 1);
            \coordinate (node1b) at (0.5, 2);
            \coordinate (node1c) at (0.5, 3);
            
            \coordinate (node2a) at (2.5, 1);
            \coordinate (node2b) at (2.5, 2);
            \coordinate (node2c) at (2.5, 3);
            
            \draw (rect1a) -- (rect1b) -- (rect1c) -- (rect1d) -- (rect1a);
            \draw (rect2a) -- (rect2b) -- (rect2c) -- (rect2d) -- (rect2a);
            
            \filldraw (node1a) circle (1pt) node[left] {1};
            \filldraw (node1b) circle (1pt) node[left] {2};
            \filldraw (node1c) circle (1pt) node[left] {3};
            
            \filldraw (node2a) circle (1pt) node[right] {2};
            \filldraw (node2b) circle (1pt) node[right] {4};
            \filldraw (node2c) circle (1pt) node[right] {6};
            
            \node at (0.5, 0) {Domain};
            \node at (2.5, 0) {Codomain};
            
            \draw[-stealth,ultra thick,colorblind_medium_blue] (node1a) -- (node2a);
            \draw[-stealth,ultra thick,colorblind_medium_blue] (node1b) -- (node2b);
            \draw[-stealth,ultra thick,colorblind_medium_blue] (node1c) -- (node2c);
        \end{tikzpicture}
        &
        
        \begin{tikzpicture}
            \coordinate (rect1a) at (0, 0.5);
            \coordinate (rect1b) at (1, 0.5);
            \coordinate (rect1c) at (1, 3.5);
            \coordinate (rect1d) at (0, 3.5);
            
            \coordinate (rect2a) at (2, 0.5);
            \coordinate (rect2b) at (3, 0.5);
            \coordinate (rect2c) at (3, 3.5);
            \coordinate (rect2d) at (2, 3.5);
            
            \coordinate (node1a) at (0.5, 1);
            \coordinate (node1b) at (0.5, 2);
            \coordinate (node1c) at (0.5, 3);
            
            \coordinate (node2a) at (2.5, 1);
            \coordinate (node2b) at (2.5, 2);
            \coordinate (node2c) at (2.5, 3);
            
            \draw (rect1a) -- (rect1b) -- (rect1c) -- (rect1d) -- (rect1a);
            \draw (rect2a) -- (rect2b) -- (rect2c) -- (rect2d) -- (rect2a);
            
            \filldraw (node1a) circle (1pt) node[left] {2};
            \filldraw (node1b) circle (1pt) node[left] {4};
            \filldraw (node1c) circle (1pt) node[left] {6};
        
            \filldraw (node2a) circle (1pt) node[right] {5};
            \filldraw (node2b) circle (1pt) node[right] {7};
            \filldraw (node2c) circle (1pt) node[right] {9};
            
            \node at (0.5, 0) {Domain};
            \node at (2.5, 0) {Codomain};
            
            \draw[-stealth,ultra thick,colorblind_medium_orange] (node1a) -- (node2a);
            \draw[-stealth,ultra thick,colorblind_medium_orange] (node1b) -- (node2b);
            \draw[-stealth,ultra thick,colorblind_medium_orange] (node1c) -- (node2c);
        \end{tikzpicture}
        &
        
        \begin{tikzpicture}
            \coordinate (rect1a) at (0, 0.5);
            \coordinate (rect1b) at (1, 0.5);
            \coordinate (rect1c) at (1, 3.5);
            \coordinate (rect1d) at (0, 3.5);
            
            \coordinate (rect2a) at (2, 0.5);
            \coordinate (rect2b) at (3, 0.5);
            \coordinate (rect2c) at (3, 3.5);
            \coordinate (rect2d) at (2, 3.5);
            
            \coordinate (node1a) at (0.5, 1);
            \coordinate (node1b) at (0.5, 2);
            \coordinate (node1c) at (0.5, 3);
            
            \coordinate (node2a) at (2.5, 1);
            \coordinate (node2b) at (2.5, 2);
            \coordinate (node2c) at (2.5, 3);
            
            \draw (rect1a) -- (rect1b) -- (rect1c) -- (rect1d) -- (rect1a);
            \draw (rect2a) -- (rect2b) -- (rect2c) -- (rect2d) -- (rect2a);
            
            \filldraw (node1a) circle (1pt) node[left] {1};
            \filldraw (node1b) circle (1pt) node[left] {2};
            \filldraw (node1c) circle (1pt) node[left] {3};
        
            \filldraw (node2a) circle (1pt) node[right] {5};
            \filldraw (node2b) circle (1pt) node[right] {7};
            \filldraw (node2c) circle (1pt) node[right] {9};
            
            \node at (0.5, 0) {Domain};
            \node at (2.5, 0) {Codomain};
            
            \node at (1.5, 2) {?};
            
            %\draw[-stealth,ultra thick,colorblind_medium_orange] (node1a) -- (node2a);
            %\draw[-stealth,ultra thick,colorblind_medium_orange] (node1b) -- (node2b);
            %\draw[-stealth,ultra thick,colorblind_medium_orange] (node1c) -- (node2c);
        \end{tikzpicture}
    \end{tabular}
    ~\\
    
    To build out the diagram for $g \circ f$, we look at $f$'s mapping
    from $A$ to $B$, and $g$'s mapping from $B$ to $C$, in order to
    figure out the mapping from $A$ to $C$ for $g \circ f$.
    The $\mapsto$ symbol means ``maps to'', showing the link between
    an element from the \textbf{domain} input to the \textbf{codomain} output.
    
    \begin{center}
        \begin{tabular}{c | c | c}
            \textbf{$f$ mappings}   & \textbf{$g$ mappings}     & \textbf{$g \circ f$ mappings} \\ \hline
            $1 \mapsto 2$           & $2 \mapsto 5$             & $1 \mapsto 5$ \\ \hline
            $2 \mapsto 4$           & $4 \mapsto 7$             & $2 \mapsto 7$ \\  \hline
            $3 \mapsto 6$           & $6 \mapsto 9$             & $3 \mapsto 9$
        \end{tabular}
    \end{center}
    
    So the diagram for $g \circ f$ will be: 
    \begin{center}
        \begin{tikzpicture}
            \coordinate (rect1a) at (0, 0.5);
            \coordinate (rect1b) at (1, 0.5);
            \coordinate (rect1c) at (1, 3.5);
            \coordinate (rect1d) at (0, 3.5);
            
            \coordinate (rect2a) at (2, 0.5);
            \coordinate (rect2b) at (3, 0.5);
            \coordinate (rect2c) at (3, 3.5);
            \coordinate (rect2d) at (2, 3.5);
            
            \coordinate (node1a) at (0.5, 1);
            \coordinate (node1b) at (0.5, 2);
            \coordinate (node1c) at (0.5, 3);
            
            \coordinate (node2a) at (2.5, 1);
            \coordinate (node2b) at (2.5, 2);
            \coordinate (node2c) at (2.5, 3);
            
            \draw (rect1a) -- (rect1b) -- (rect1c) -- (rect1d) -- (rect1a);
            \draw (rect2a) -- (rect2b) -- (rect2c) -- (rect2d) -- (rect2a);
            
            \filldraw (node1a) circle (1pt) node[left] {1};
            \filldraw (node1b) circle (1pt) node[left] {2};
            \filldraw (node1c) circle (1pt) node[left] {3};
        
            \filldraw (node2a) circle (1pt) node[right] {5};
            \filldraw (node2b) circle (1pt) node[right] {7};
            \filldraw (node2c) circle (1pt) node[right] {9};
            
            \node at (0.5, 0) {Domain};
            \node at (2.5, 0) {Codomain};
            
            \draw[-stealth,ultra thick,colorblind_dark] (node1a) -- (node2a);
            \draw[-stealth,ultra thick,colorblind_dark] (node1b) -- (node2b);
            \draw[-stealth,ultra thick,colorblind_dark] (node1c) -- (node2c);
        \end{tikzpicture}
    \end{center}

    \newpage
    \subsection{Finding the missing \underline{inner function}}
    
        Just as we can find some function composition $g(f(x))$ given
        $f(x)$ and $g(x)$, we can also find a missing $f(x)$ (the inner function)
        given $g(f(x))$ and $g(x)$.
        
        ~\\
        To keep things easier to read, we're going to use functions $o(x)$ as the outer function,
        and $i(x)$ as the inner function, with $o(i(x))$ (or $o \circ i$) as the composite.
        
        \subsubsection{With diagrams}
    
        \begin{center}
            \begin{tabular}{c c c}
                $i : ? \to ?$ & $o : B \to C$ & $(o \circ i) : A \to C$ \\ \\
                \begin{tikzpicture}
                    \coordinate (rect1a) at (0, 0.5);
                    \coordinate (rect1b) at (1, 0.5);
                    \coordinate (rect1c) at (1, 3.5);
                    \coordinate (rect1d) at (0, 3.5);
                    
                    \coordinate (rect2a) at (2, 0.5);
                    \coordinate (rect2b) at (3, 0.5);
                    \coordinate (rect2c) at (3, 3.5);
                    \coordinate (rect2d) at (2, 3.5);
                    
                    \coordinate (node1a) at (0.5, 1);
                    \coordinate (node1b) at (0.5, 2);
                    \coordinate (node1c) at (0.5, 3);
                    
                    \coordinate (node2a) at (2.5, 1);
                    \coordinate (node2b) at (2.5, 2);
                    \coordinate (node2c) at (2.5, 3);
                    
                    \draw (rect1a) -- (rect1b) -- (rect1c) -- (rect1d) -- (rect1a);
                    \draw (rect2a) -- (rect2b) -- (rect2c) -- (rect2d) -- (rect2a);
                    
                    \filldraw (node1a) circle (1pt) node[left] {?};
                    \filldraw (node1b) circle (1pt) node[left] {?};
                    \filldraw (node1c) circle (1pt) node[left] {?};
                    
                    \filldraw (node2a) circle (1pt) node[right] {?};
                    \filldraw (node2b) circle (1pt) node[right] {?};
                    \filldraw (node2c) circle (1pt) node[right] {?};
                    
                    \node at (0.5, 0) {Domain};
                    \node at (2.5, 0) {Codomain};
                    
                    \node at (1.5, 2) {?};
                    
                    %\draw[-stealth,ultra thick,colorblind_medium_blue] (node1a) -- (node2a);
                    %\draw[-stealth,ultra thick,colorblind_medium_blue] (node1b) -- (node2b);
                    %\draw[-stealth,ultra thick,colorblind_medium_blue] (node1c) -- (node2c);
                \end{tikzpicture}
                &
                
                \begin{tikzpicture}
                    \coordinate (rect1a) at (0, 0.5);
                    \coordinate (rect1b) at (1, 0.5);
                    \coordinate (rect1c) at (1, 3.5);
                    \coordinate (rect1d) at (0, 3.5);
                    
                    \coordinate (rect2a) at (2, 0.5);
                    \coordinate (rect2b) at (3, 0.5);
                    \coordinate (rect2c) at (3, 3.5);
                    \coordinate (rect2d) at (2, 3.5);
                    
                    \coordinate (node1a) at (0.5, 1);
                    \coordinate (node1b) at (0.5, 2);
                    \coordinate (node1c) at (0.5, 3);
                    
                    \coordinate (node2a) at (2.5, 1);
                    \coordinate (node2b) at (2.5, 2);
                    \coordinate (node2c) at (2.5, 3);
                    
                    \draw (rect1a) -- (rect1b) -- (rect1c) -- (rect1d) -- (rect1a);
                    \draw (rect2a) -- (rect2b) -- (rect2c) -- (rect2d) -- (rect2a);
                    
                    \filldraw (node1a) circle (1pt) node[left] {2};
                    \filldraw (node1b) circle (1pt) node[left] {4};
                    \filldraw (node1c) circle (1pt) node[left] {6};
                
                    \filldraw (node2a) circle (1pt) node[right] {5};
                    \filldraw (node2b) circle (1pt) node[right] {7};
                    \filldraw (node2c) circle (1pt) node[right] {9};
                    
                    \node at (0.5, 0) {Domain};
                    \node at (2.5, 0) {Codomain};
                    
                    \draw[-stealth,ultra thick,colorblind_medium_orange] (node1a) -- (node2a);
                    \draw[-stealth,ultra thick,colorblind_medium_orange] (node1b) -- (node2b);
                    \draw[-stealth,ultra thick,colorblind_medium_orange] (node1c) -- (node2c);
                \end{tikzpicture}
                &
                
                \begin{tikzpicture}
                    \coordinate (rect1a) at (0, 0.5);
                    \coordinate (rect1b) at (1, 0.5);
                    \coordinate (rect1c) at (1, 3.5);
                    \coordinate (rect1d) at (0, 3.5);
                    
                    \coordinate (rect2a) at (2, 0.5);
                    \coordinate (rect2b) at (3, 0.5);
                    \coordinate (rect2c) at (3, 3.5);
                    \coordinate (rect2d) at (2, 3.5);
                    
                    \coordinate (node1a) at (0.5, 1);
                    \coordinate (node1b) at (0.5, 2);
                    \coordinate (node1c) at (0.5, 3);
                    
                    \coordinate (node2a) at (2.5, 1);
                    \coordinate (node2b) at (2.5, 2);
                    \coordinate (node2c) at (2.5, 3);
                    
                    \draw (rect1a) -- (rect1b) -- (rect1c) -- (rect1d) -- (rect1a);
                    \draw (rect2a) -- (rect2b) -- (rect2c) -- (rect2d) -- (rect2a);
                    
                    \filldraw (node1a) circle (1pt) node[left] {1};
                    \filldraw (node1b) circle (1pt) node[left] {2};
                    \filldraw (node1c) circle (1pt) node[left] {3};
                
                    \filldraw (node2a) circle (1pt) node[right] {5};
                    \filldraw (node2b) circle (1pt) node[right] {7};
                    \filldraw (node2c) circle (1pt) node[right] {9};
                    
                    \node at (0.5, 0) {Domain};
                    \node at (2.5, 0) {Codomain};
                    
                    \draw[-stealth,ultra thick,colorblind_dark] (node1a) -- (node2a);
                    \draw[-stealth,ultra thick,colorblind_dark] (node1b) -- (node2b);
                    \draw[-stealth,ultra thick,colorblind_dark] (node1c) -- (node2c);
                \end{tikzpicture}
            \end{tabular}
        \end{center}

        \begin{center}
            \begin{tabular}{c | c | c}
                \textbf{$i$ mappings}   & \textbf{$o$ mappings}     & \textbf{$o \circ i$ mappings} \\ \hline
                $? \mapsto ?$           & $2 \mapsto 5$             & $1 \mapsto 5$ \\ \hline
                $? \mapsto ?$           & $4 \mapsto 7$             & $2 \mapsto 7$ \\  \hline
                $? \mapsto ?$           & $6 \mapsto 9$             & $3 \mapsto 9$
            \end{tabular}
        \end{center}
        
        \paragraph{Finding the domain and codomain:}
        From a diagram point-of-view, we know that the \textbf{domain}
        of $o(i(x))$ ($o \circ i$) and $i(x)$ must match,
        as well as the \textbf{codomain} of $i(x)$ with the \textbf{domain}
        of $o(x)$.
        
        \paragraph{Finding the mappings:}
        We also know that mappings from $A_i \mapsto C_k$ must be the same result
        as $A_i \mapsto B_j$ and from $B_j \mapsto C_k$. So for example,
        $2 \mapsto 5$ in the outer function $o$, 
        and $1 \mapsto 5$ in the composite functions $o \circ i$, 
        so $1 \mapsto 2$ must be true in the inner function $i$.
        We can work through it this way to find our mappings.
        
        \begin{center}
            \begin{tabular}{c | c | c}
                \textbf{$i$ mappings}   & \textbf{$o$ mappings}     & \textbf{$o \circ i$ mappings} \\ \hline
                $1 \mapsto 2$           & $2 \mapsto 5$             & $1 \mapsto 5$ \\ \hline
                $2 \mapsto 4$           & $4 \mapsto 7$             & $2 \mapsto 7$ \\  \hline
                $3 \mapsto 6$           & $6 \mapsto 9$             & $3 \mapsto 9$
            \end{tabular}
        \end{center}
        
        \newpage
        \subsubsection{With function definitions}
        
        Let's say we have functions $o(x) = 3x$ and $o(i(x)) = 3x + 3$. How do we find
        the equation for the inner function, $i(x)$?
        
        ~\\
        \begin{tabular}{p{10cm} | l}
            1. First, we use a placeholder for $i(x)$, like $a$.    & $i(x) = a$ \\ \\
            2. Rewrite $o(i(x))$, swapping out $i(x)$.              & $o(a)$ \\ \\
            3. Simplify $o(a)$.                                     & $o(a) = 3a$ \\ \\
            4. Now set $3a$ equal to the function of $o(i(x))$.     & $3a = 3x + 3$ \\ \\
            5. Solve for $a$:                                       & $3a = 3x + 3$ \\
                                                                    & $\frac{3a}{3} = \frac{3x + 3}{3}$ \\
                                                                    & $a = x + 1$ \\ \\
            6. Re-sub $a$ with $i(x)$.                              & \color{red} $i(x) = x + 1$ \color{black}
        \end{tabular}
        ~\\




    \newpage
    \subsection{Finding the missing \underline{outer function}}

        Given $g(f(x))$ and $f(x)$, we can find a missing $g(x)$ (the outer function)
        as well.
        
        ~\\
        To keep things easier to read, we're going to use functions $o(x)$ as the outer function,
        and $i(x)$ as the inner function, with $o(i(x))$ (or $o \circ i$) as the composite.
        
        \subsubsection{With diagrams}
    
        \begin{center}
            \begin{tabular}{c c c}
                $i : A \to B$ & $o : ? \to ?$ & $(o \circ i) : A \to C$ \\ \\
                \begin{tikzpicture}
                    \coordinate (rect1a) at (0, 0.5);
                    \coordinate (rect1b) at (1, 0.5);
                    \coordinate (rect1c) at (1, 3.5);
                    \coordinate (rect1d) at (0, 3.5);
                    
                    \coordinate (rect2a) at (2, 0.5);
                    \coordinate (rect2b) at (3, 0.5);
                    \coordinate (rect2c) at (3, 3.5);
                    \coordinate (rect2d) at (2, 3.5);
                    
                    \coordinate (node1a) at (0.5, 1);
                    \coordinate (node1b) at (0.5, 2);
                    \coordinate (node1c) at (0.5, 3);
                    
                    \coordinate (node2a) at (2.5, 1);
                    \coordinate (node2b) at (2.5, 2);
                    \coordinate (node2c) at (2.5, 3);
                    
                    \draw (rect1a) -- (rect1b) -- (rect1c) -- (rect1d) -- (rect1a);
                    \draw (rect2a) -- (rect2b) -- (rect2c) -- (rect2d) -- (rect2a);
                    
                    \filldraw (node1a) circle (1pt) node[left] {1};
                    \filldraw (node1b) circle (1pt) node[left] {2};
                    \filldraw (node1c) circle (1pt) node[left] {3};
                    
                    \filldraw (node2a) circle (1pt) node[right] {2};
                    \filldraw (node2b) circle (1pt) node[right] {4};
                    \filldraw (node2c) circle (1pt) node[right] {6};
                    
                    \node at (0.5, 0) {Domain};
                    \node at (2.5, 0) {Codomain};
                    
                    \draw[-stealth,ultra thick,colorblind_medium_blue] (node1a) -- (node2a);
                    \draw[-stealth,ultra thick,colorblind_medium_blue] (node1b) -- (node2b);
                    \draw[-stealth,ultra thick,colorblind_medium_blue] (node1c) -- (node2c);
                \end{tikzpicture}
                &
                
                \begin{tikzpicture}
                    \coordinate (rect1a) at (0, 0.5);
                    \coordinate (rect1b) at (1, 0.5);
                    \coordinate (rect1c) at (1, 3.5);
                    \coordinate (rect1d) at (0, 3.5);
                    
                    \coordinate (rect2a) at (2, 0.5);
                    \coordinate (rect2b) at (3, 0.5);
                    \coordinate (rect2c) at (3, 3.5);
                    \coordinate (rect2d) at (2, 3.5);
                    
                    \coordinate (node1a) at (0.5, 1);
                    \coordinate (node1b) at (0.5, 2);
                    \coordinate (node1c) at (0.5, 3);
                    
                    \coordinate (node2a) at (2.5, 1);
                    \coordinate (node2b) at (2.5, 2);
                    \coordinate (node2c) at (2.5, 3);
                    
                    \draw (rect1a) -- (rect1b) -- (rect1c) -- (rect1d) -- (rect1a);
                    \draw (rect2a) -- (rect2b) -- (rect2c) -- (rect2d) -- (rect2a);
                    
                    \filldraw (node1a) circle (1pt) node[left] {?};
                    \filldraw (node1b) circle (1pt) node[left] {?};
                    \filldraw (node1c) circle (1pt) node[left] {?};
                
                    \filldraw (node2a) circle (1pt) node[right] {?};
                    \filldraw (node2b) circle (1pt) node[right] {?};
                    \filldraw (node2c) circle (1pt) node[right] {?};
                    
                    \node at (0.5, 0) {Domain};
                    \node at (2.5, 0) {Codomain};
                    
                    \node at (1.5, 2) {?};
                    
                    %\draw[-stealth,ultra thick,colorblind_medium_orange] (node1a) -- (node2a);
                    %\draw[-stealth,ultra thick,colorblind_medium_orange] (node1b) -- (node2b);
                    %\draw[-stealth,ultra thick,colorblind_medium_orange] (node1c) -- (node2c);
                \end{tikzpicture}
                &
                
                \begin{tikzpicture}
                    \coordinate (rect1a) at (0, 0.5);
                    \coordinate (rect1b) at (1, 0.5);
                    \coordinate (rect1c) at (1, 3.5);
                    \coordinate (rect1d) at (0, 3.5);
                    
                    \coordinate (rect2a) at (2, 0.5);
                    \coordinate (rect2b) at (3, 0.5);
                    \coordinate (rect2c) at (3, 3.5);
                    \coordinate (rect2d) at (2, 3.5);
                    
                    \coordinate (node1a) at (0.5, 1);
                    \coordinate (node1b) at (0.5, 2);
                    \coordinate (node1c) at (0.5, 3);
                    
                    \coordinate (node2a) at (2.5, 1);
                    \coordinate (node2b) at (2.5, 2);
                    \coordinate (node2c) at (2.5, 3);
                    
                    \draw (rect1a) -- (rect1b) -- (rect1c) -- (rect1d) -- (rect1a);
                    \draw (rect2a) -- (rect2b) -- (rect2c) -- (rect2d) -- (rect2a);
                    
                    \filldraw (node1a) circle (1pt) node[left] {1};
                    \filldraw (node1b) circle (1pt) node[left] {2};
                    \filldraw (node1c) circle (1pt) node[left] {3};
                
                    \filldraw (node2a) circle (1pt) node[right] {5};
                    \filldraw (node2b) circle (1pt) node[right] {7};
                    \filldraw (node2c) circle (1pt) node[right] {9};
                    
                    \node at (0.5, 0) {Domain};
                    \node at (2.5, 0) {Codomain};
                    
                    \draw[-stealth,ultra thick,colorblind_dark] (node1a) -- (node2a);
                    \draw[-stealth,ultra thick,colorblind_dark] (node1b) -- (node2b);
                    \draw[-stealth,ultra thick,colorblind_dark] (node1c) -- (node2c);
                \end{tikzpicture}
            \end{tabular}
        \end{center}

        \begin{center}
            \begin{tabular}{c | c | c}
                \textbf{$i$ mappings}   & \textbf{$o$ mappings}     & \textbf{$o \circ i$ mappings} \\ \hline
                $1 \mapsto 2$           & $? \mapsto ?$             & $1 \mapsto 5$ \\ \hline
                $2 \mapsto 4$           & $? \mapsto ?$             & $2 \mapsto 7$ \\  \hline
                $3 \mapsto 6$           & $? \mapsto ?$             & $3 \mapsto 9$
            \end{tabular}
        \end{center}

        \paragraph{Finding the domain and codomain:}
        We know that the \textbf{codomain} of $i$ will be the \textbf{domain} of $o$,
        and that the \textbf{domain} of $o \circ i$ will be the \textbf{codomain} of $i$.

        \paragraph{Finding the mappings:}
        We have the mappings from the inner function, $A_i \mapsto B_j$,
        and we have the mappings from the composite function, $A_i \mapsto C_k$,
        so we can fill in the information we need to find the mappings
        from $B_j$ to $C_k$.

        \begin{center}
            \begin{tabular}{c | c | c}
                \textbf{$i$ mappings}   & \textbf{$o$ mappings}     & \textbf{$o \circ i$ mappings} \\ \hline
                $1 \mapsto 2$           & $2 \mapsto 5$             & $1 \mapsto 5$ \\ \hline
                $2 \mapsto 4$           & $4 \mapsto 7$             & $2 \mapsto 7$ \\  \hline
                $3 \mapsto 6$           & $6 \mapsto 9$             & $3 \mapsto 9$
            \end{tabular}
        \end{center}

        \newpage
        \subsubsection{With function definitions}
        
        Now let's look at how to find $o(x)$, given $i(x) = 2x$ and $o(i(x)) = 6x$.

        ~\\
        \begin{tabular}{p{10cm} | l}
            1. First, we use a placeholder for $i(x)$, like $a$. The difference this time is we're setting $a$ equal to the function body of $i(x)$.
                                                                        & $a = 2x$ \\ \\
            2. Solve this equation for $x$.                             & $a = 2x$ \\
                                                                        & $\frac{a}{2} = x$ \\ \\
            3. Use the equation $o(i(x)) = 6x$,                         & $o(i(x)) = 6x$ \\
                and replace $i(x)$ with $a$, and $x$ with $\frac{a}{2}$. & $o(a) = 6( \frac{a}{2} )$ \\
                                                                        & $o(a) = 3a$ \\ \\
            4. We've found the equation for $o$, so we can just
                change the variable from $a$ to $x$ now.                & \color{red} $o(x) = 3x$ \color{black}
        \end{tabular}
        ~\\









