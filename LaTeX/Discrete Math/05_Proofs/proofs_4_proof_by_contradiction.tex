
Another way we can prove a statement is to find a counter-example,
try to plug it in and work out, until we eventually get a contradiction,
showing that the counter-example is not possible.

Remember the truth tables for an implication and its negation:
\begin{center}
\begin{tabular}{c c c}
  Implication & Negation \\

  \begin{tabular}{ | c | c || c | } \hline
      $p$     & $q$     & $p \to q$ \\ \hline
      \true   & \true   & \true       \\ \hline
      \true   & \false  & \false      \\ \hline
      \false  & \true   & \true       \\ \hline
      \false  & \false  & \true       \\ \hline
  \end{tabular}
  &
  \begin{tabular}{ | c | c || c | } \hline
      $p$     & $q$     & $p \land \neg q$  \\ \hline
      \true   & \true   & \false            \\ \hline
      \true   & \false  & \true             \\ \hline
      \false  & \true   & \false            \\ \hline
      \false  & \false  & \false            \\ \hline
  \end{tabular}
\end{tabular}
\end{center}

And remember that to ``disprove'' an implication, we need a 
\textbf{hypothesis} $p$ that is \textbf{true},
and a \textbf{conclusion} $q$ that is \textbf{false} ($p \land \neg q$).

~\\

Another way we can come up with a proof is by proving that the
\textit{counterexample} does not exist.


\newpage \small
\paragraph{Example:} For any integer $n$, if $n^2$ is even, then $n$ is even.
~\\~\\ We'll work with this proposition again, but prove it a different way!

~\\
Let's look at this as an \textbf{implication}: ~\\
``if $n^2$ is even, then $n$ is even.''

~\\
And to get a \textbf{counter-example}, the \textbf{negation} would have to be true: ~\\
``$n^2$ is even, and $n$ is \textit{not} even.''

~\\ So for our theoretical counter-example, we will assume we cound some
case where $n^2$ is even and $n$ is not even (or, odd). In this case,
we will need \textbf{two aliases}:

\begin{center}
    $n^2$ is even:  $n^2 = 2k$
    \tab
    $n$ is not even: $n = 2j+1$
\end{center}

\begin{center}
    \color{red}
    A common mistake is to re-use the same alias variable in different contexts
    (e.g., using $k$ in both cases) - never re-use the same alias variable!
    \color{black}
\end{center}

Next, we know that if we take $n \cdot n$, we get $n^2$, so we can turn
our equation with $k$ and $j$ into this:

$$ n \cdot n = n^2 $$        
$$ (2j+1)(2j+1) = 2k $$

Then we simplify as much as we can:

\begin{enumerate}
    \item   $(2j+1)(2j+1) = 2k$
    \item   $4j^2 + 4j + 1 = 2k$
    \item   $2(2j^2 + 2j) + 1 = 2k$ \tab \xmark This is a contradiction!
\end{enumerate}

At this step, we've came out with the definition of an odd number equalling the definition of an even number,
which is a contradiction as it is.      
We could continue simplifying it out further, however:

\begin{enumerate}
    \item[4.]   $4j^2 + 4j - 2k = 1$            \tab[1.1cm] Moving the constant to one side
    \item[5.]   $2(2j^2 + 2j - k) = 1$          \tab Factoring out the 2
    \item[6.]   $2j^2 + 2j - k = \frac{1}{2}$   \tab[1.4cm] Dividing both sides by 2
    \item[7.]   \xmark Broken the \textbf{closure property of integers!}
\end{enumerate}

At this point, we have math on the LHS (left-hand side) between integers (multiplication, addition, subtraction)
which, given the closure property of integers, should always result in another integer.
On the RHS (right-hand side), we have a fraction. At this point, the proposition
is broken because we \textbf{cannot find a counter-example} that deals with integers.

\normalsize


