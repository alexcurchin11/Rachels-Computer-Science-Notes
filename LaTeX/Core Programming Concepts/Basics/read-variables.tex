
    \begin{center}
        \includegraphics[width=\textwidth]{Basics/images/variables}
    \end{center}
    
    We use variables in programming as places to temporarily store data
    so that we can manipulate that data. We can have the user write
    to it via the keyboard, we can use files to write data to variables,
    we can do math operations or modify these variables, and we can
    print them back out to the screen or a text file.\footnote{Printing here meaning ``display on the screen''; not to a printer. Holdover from ancient times when computers' outputs came via the printers.}
    
    When we're writing programs in C++, we need to tell our program
    what the \textbf{data type} of each variable is, and give each variable
    a \textbf{variable name} (aka identifier). When a variable is \textbf{declared},
    a space in RAM is allocated to that variable and it can store its data there.
    That data can be manipulated or overwritten later as needed in the program.
    
    \section{Data types}
    
        In C++, when we want to create a variable we need to tell the \textbf{compiler}
        what the \textbf{data type} of the variable is. Data types specify
        \textit{what} is stored in the variable (whole numbers? numbers with decimal values?
        text? true/false values?). 
        Each data type take up a different amount of space in memory.
        
        \begin{center}
         \begin{tabular}{l p{4cm} l l}
          \textbf{Data type}    & \textbf{Values}                           & \textbf{Size} & \textbf{Example code} \\ \hline
            integer             & whole numbers                             & 4 bytes       & \texttt{int age = 100; } 
            \\ \\                                                             
            char                & single symbols -                                  
                
                                  letters, numbers, anything                & 1 byte        & \texttt{char currency = '\$';}
            \\ \\
            float               & numbers w/ decimals                       & 4 bytes       & \texttt{float price = 9.95;}
            \\ \\                                                             
            double              & numbers w/ decimals                       & 8 bytes       & \texttt{double price = 1.95;}
            \\                      
            \\
            string              & any text, numbers, symbols, any length    & variable      & \texttt{string password = "123secure";}
            \\ \\                                                             
            boolean             & \texttt{true} or \texttt{false}           & 1 byte        & \texttt{bool saved = false;}
         \end{tabular}
        \end{center}
        
        Of these, notice that the \textbf{string} data type doesn't have a fixed size. This
        is because technically, behind-the-scenes, a string is really just an 
        array (or list) of \texttt{char} data types. The C++ standard library contains a
        string library that handles a lot of text-related functionality like \texttt{find}.  ~\\
        
        \textbf{Floats} and \textbf{doubles} both store numbers with fractional (decimal) parts,
        but a double takes up \textit{double the memory in RAM}, allowing to store
        more accurate fractional amounts. A \texttt{float} has 6 to 9 digits of precision
         and a \texttt{double} has 15 to 18 digits of precision.
        \footnote{From https://www.learncpp.com/cpp-tutorial/floating-point-numbers/} ~\\
        
        \textbf{Boolean} data types store \texttt{true} and \texttt{false} values,
        but will also accept integer values when assigned. It will convert
        a value of \texttt{0} to \texttt{false} and any other number to \texttt{true}.
    
    \section{Declaring variables and assigning values to variables}
    
        \subsection{Variable declaration}
        
            When we're \textbf{declaring} a variable it needs to follow one of these formats:
            
            \begin{intro}{Variable declaration formats}
                \begin{enumerate}
                    \item   \texttt{DATATYPE VARIABLENAME;}
                    \item   \texttt{\texttt{DATATYPE VARIABLENAME = VALUE;} }
                    \item   \texttt{\texttt{DATATYPE VARIABLENAME1, VARIABLENAME2, VARIABLENAME3;} }
                    \item   \texttt{\texttt{DATATYPE VARIABLENAME1 = VALUE1, VARIABLENAME2 = VALUE2;} }
                \end{enumerate}
            \end{intro}
            
            The \textbf{data type} goes first, then the \textbf{variable name/identifier},
            and if you'd like, you can also \textbf{assign a value} during the same step
            (though this is not required).
            ~\\
            
            Once a variable has been \textbf{declared}, you don't need to declare it again.
            This means you don't need to re-specify its data type when you're using it.
            Just address the variable by its name and that's all.
        
            ~\\ Code that uses integers to figure out how many candies each kid should get:
\begin{lstlisting}[style=code]
// Declaring variables and assigning values
int totalCandies = 10;
int totalKids = 5;
int candiesPerKid = totalCandies / totalKids;

// Reusing the same variables later on
totalCandies = 100;
totalKids = 10;
candiesPerKid = totalCandies / totalKids;
\end{lstlisting}

        ~\\ Code to display the price plus tax to the user:
\begin{lstlisting}[style=code]
float price = 9.99;
float tax = 0.11;
// Text output to the screen. Can do math within!
cout << "Please pay " << (price + price * tax);
\end{lstlisting}

        \subsection{Variable assignment}
    
            \begin{intro}{Variable declaration formats}
                \begin{enumerate}
                    \item   \texttt{VARIABLENAME = LITERAL;} ~\\
                            Stores the LITERAL value in VARIABLENAME.
                    \item   \texttt{VARIABLENAME1 = VARIABLENAME2;} ~\\
                            Copies the value from VARIABLENAME2 to VARIABLENAME1.
                \end{enumerate}
            \end{intro}
            
            When assigning a \textbf{value} to a \textbf{variable},
            the variable being assigned to always goes on the left-hand side (``LHS'')
            of the equal sign =.
            The = sign is known here as the \texttt{assignment operator}.
            
            The item on the right-hand side (``RHS'') will be the value
            stored in the variable specified on the LHS. This can be a
            \textbf{literal} (a hard-coded value), or it can be a different
            variable of the same data type, whose value you want to copy over.
            
            ~\\ Assigning literal values to variables:
\begin{lstlisting}[style=code]
price = 9.99;       // 9.99 is a float literal
state = "Kansas";   // "Kansas" is a string literal
operation = '+';    // '+' is a char literal
\end{lstlisting}
        
            ~\\ Copying \texttt{student13}'s value to the \texttt{bestStudent} variable:
\begin{lstlisting}[style=code]
bestStudent = student13;
\end{lstlisting}

            \begin{center}
             \includegraphics[width=11cm]{Basics/images/assignment1}
            \end{center}

        \subsection{Naming conventions}
            You can name a variable anything you'd like - but it can only
            contain numbers, underscore (\_), and upper- and lower-case letters
            in the name. Variable names can begin with the underscore
            or a letter, but it cannot start with a number. And definitely
            NO spaces allowed in a variable name! ~\\
            
            Additionally, a variable name cannot be a \textbf{keyword}
            in C++ - a name reserved for something else in the language,
            such as \texttt{void}, \texttt{if}, \texttt{int}, etc.~\\
            
            Everything in C++ is \textbf{case sensitive} as well, which means
            if you name a variable \texttt{username}, it will be a different
            variable from one named \texttt{userName} (or if you type
            ``userName'' when the variable is called ``username'', the compiler
            will complain at you because it doesn't know what you mean).
            ~\\
            
            In C++, it's standard to name your variables using \textbf{camel casing},
            with a lower-case first letter. This means that if your variable
            name ``should'' contain spaces, you just capitalize the next word instead:
        
\begin{lstlisting}[style=code]
int howManyCookies;
string mySecurePassword;
float howMuchStudentLoansIHave;
\end{lstlisting}

        \subsection{Unsigned data types}
        
            Sometimes it doesn't make sense to store negative values in
            a variable - such as speed (which isn't directional, like velocity)
            or the size of a list (can't have negative items) or
            measurement (can't have negative width).
            You can mark a variable as \texttt{unsigned} to essentially
            double it's range (by getting rid of the negative side of values).
            ~\\
            
            For instance, a normal \texttt{int} can store values from
            -2,147,483,648 to ~\\ 2,147,483,647 -  but if you mark it as
            \texttt{unsigned}, then your range is 0 to 4,294,967,295.
        
        \newpage
        \subsection{Modern C++: \texttt{auto}}
            In C++11\footnote{This means the 2011 update.} and later
            you can use the keyword \texttt{auto} as the ``data type''
            in your variable declaration that includes an assignment statement.
            When you use \texttt{auto}, it uses the assigned value to automatically
            figure out the variable's data type, so you don't have to explicitly
            define it:
        
\begin{lstlisting}[style=code]
auto price = 9.99;      // it's a double!
auto price2 = 2.95f;    // it's a float!
auto player = '@';      // it's a char!
auto amount = 20;       // it's an int!
\end{lstlisting}

        \begin{figure}[h]
            \centering
            \begin{subfigure}{.3\textwidth}
                \includegraphics[width=0.9\textwidth]{Basics/images/rachu}
            \end{subfigure}%
            \begin{subfigure}{.7\textwidth}
                
                Using C\#'s equivalent of \texttt{auto} (\texttt{var})
                is super common from what I've seen on the job.
                However, it still feels weird to me to use \texttt{auto}
                in C++. I tend to prefer to always be as explicit as
                possible in my programming to reduce any guesswork,
                even if it's something that takes half a second to figure out.
                
            \end{subfigure}
        \end{figure}
        
    \section{Basic operations on variables}
    
        Now that you have variables available in your program to play with,
        what can you even do with them?
        
        \subsection{Outputting variable values}
        
            Using the \texttt{cout} (console-out) statement, you can
            display text to the screen with string literals:
            
\begin{lstlisting}[style=code]
cout << "Hello, world!" << endl;
\end{lstlisting}

            But you can also display the values stored within variables,
            simply by using the variable's name:
        
\begin{lstlisting}[style=code]
cout << myUsername << endl;
\end{lstlisting}

            (We will cover more about input and output next part)

        \subsection{Inputting variable values}
        
            We can use the \texttt{cin} (console-in) statement to
            get the user to enter somethign on the keyboard and
            store that data into a variable:
        
\begin{lstlisting}[style=code]
cin >> favoriteColor;
\end{lstlisting}

        \subsection{Math operations}
        
            With variables with numeric data types (ints, floats, doubles)
            we can do arithmetic with the \texttt{+}, \texttt{-}, \texttt{*} and \texttt{/} operators.
            
\begin{lstlisting}[style=code]
cout << "Sum: " << num1 + num2 + num3 << endl;
\end{lstlisting}

            \paragraph{Operations:}
            \begin{center}
             \begin{tabular}{l l}
              \textbf{Symbol}   & \textbf{Description}  \\ \hline
              \texttt{+}        & Addition              \\
              \texttt{-}        & Subtraction           \\
              \texttt{*}        & Multiplication        \\
              \texttt{/}        & Division              \\
             \end{tabular}   
            \end{center}

            \paragraph{Make sure to put the result somewhere!}
            When you do a math operation and you want to use the result
            elseware in the program, make sure you're storing the result
            in a variable via an assignment statement!             
            If you just do this, nothing will happen:
\begin{lstlisting}[style=code]
totalCats + 1;
\end{lstlisting}

            You can use an assignment statement to store the result
            in a new variable...
\begin{lstlisting}[style=code]
newCatTotal = totalCats + 1;
\end{lstlisting}
            
            Or overwrite the variable you're working with...
\begin{lstlisting}[style=code]
totalCats = totalCats + 1;
\end{lstlisting}

            \begin{center}
             \includegraphics[height=3cm]{Basics/images/cats}   
            \end{center}
            
            \newpage
            \paragraph{Compound operations:}
            There are also shortcut operations you can use to quickly
            do some math and overwrite the original variable.
            This works with each of the arithmetic operations:
            
\begin{lstlisting}[style=code]
// Long way:
totalCats = totalCats + 5;

// Compound operation:
totalCats += 5;
\end{lstlisting}
        
        \subsection{String operations}
        
            Strings have some special operations you can do on them.
            You can also see a list of functions supported by
            strings here: ~\\
            \texttt{https://www.cplusplus.com/reference/string/string/}
            (We will cover more with strings in a later part).
            
            \paragraph{Concatenating strings:}
            You can use the \texttt{+} symbol to combine strings together.
            When used in this context, the + sign is called the \textbf{concatenation operator}.
        
\begin{lstlisting}[style=code]
string type = "pepperoni";
string food = "pizza";

// Creates the string "pepperoni pizza"
string order = type + " " + food;
\end{lstlisting}

            \paragraph{Letter-of-the-string:}
            You can also use the subscript operator \texttt{[ ]} (more on
            this when we cover arrays) to access a letter at some
            \textit{position} in the string. Note that in C++
            position starts at 0, not 1.

\begin{lstlisting}[style=code]
string food = "pizza";

char letter1 = food[0];  // Stores 'p'
char letter2 = food[1];  // Stores 'i'
char letter3 = food[2];  // Stores 'z'
char letter4 = food[3];  // Stores 'z'
char letter5 = food[4];  // Stores 'a'
\end{lstlisting}
    
    
    \newpage
    \section{Named constants}
    
        Whenever you find yourself using a \textbf{literal} value
        in your assignment statements, you may want to think about
        whether you should replace it with a \textbf{named constant}
        instead.
        
        A named constant looks like a variable when you declare it,
        but it also has the keyword \texttt{const} - meaning that
        the value can't change after its declaration.
    
            \begin{intro}{Named constant declaration format}
                \begin{enumerate}
                    \item   \texttt{const CONSTNAME = LITERAL;} ~\\
                            Stores the LITERAL value in CONSTNAME.
                \end{enumerate}
            \end{intro}
            
        Let's say you wrote a program and hard-coded the tax rate:
                
\begin{lstlisting}[style=code]
// Somewhere in the code...
checkoutPrice = cartTotal + ( cartTotal * 0.0948 );

// Somewhere else in the code...
cout << 0.0948 << " sales tax" << endl;
\end{lstlisting}

        Then the tax rate changes later on. You would have to go into
        your program and search for ``0.0948'' and update \textit{all those places!}
        
        Instead, it would have been easier to assign the tax rate ONCE
        to a named constant, and referred to that instead:
        
\begin{lstlisting}[style=code]
// Beginning of program somewhere...
const SALES_TAX = 0.0948;
    
// Somewhere in the code...
checkoutPrice = cartTotal + ( cartTotal * SALES_TAX );

// Somewhere else in the code...
cout << SALES_TAX << " sales tax" << endl;
\end{lstlisting}

        If you ever find yourself using the same literal multiple times
        in your program, then perhaps consider replacing it with a
        named constant.
        
        \paragraph{Named constant naming convention:}
        In C++, it is customary to give your named constants names in
        ALL CAPS, using underscores (\texttt{\_}) to separate words.
    
    %math operations
    %concatenating strings
    
    
    %compound operators *=
    %no commas or $ signs
    
    %why are floats
    % subscript with strings
    
    
    
