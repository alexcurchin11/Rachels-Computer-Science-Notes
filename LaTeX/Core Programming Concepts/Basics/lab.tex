%---%
\newpage
\section{Lab: Variables and Data Types}

\subsection*{Opening the files}

When you download the starter files, it will need to be \textbf{unzipped/extracted}.
If you're on a lab computer, extract the files to the desktop, where it will be most easily accessible.

\paragraph{Files included:}

\begin{itemize}
	\item	CS200-Lab01-Variables/
	\begin{itemize}
		\item	functions.hpp		(Source code file)
		\item	functions.cpp		(Source code file)
		\item	main.cpp			(Source code file)

		\item	Project-CodeBlocks/
		\begin{itemize}
			\item	CS200-Lab01-Variables.cbp	(Code::Blocks project file)
		\end{itemize}

		\item	Project-VisualStudio/
		\begin{itemize}
			\item	CS200-Lab01-Variables.sln	(Visual Studio solution file)
		\end{itemize}
	\end{itemize}
\end{itemize}

~\\ If you're using Code::Blocks, you can double-click on the
\textbf{CS200-Lab01-Variables.cbp} file to open up the project in Code::Blocks.

~\\ If you're using Visual Studio, double click on
\textbf{CS200-Lab01-Variables.sln} to open it up in Visual Studio. ~\\

Either way, the project has been set up with the files added. In the future,
you can create projects/solutions yourself, and add source code files.



\subsection*{Running the project}

After you've opened up the project in Visual Studio or Code::Blocks,
run it to make sure there are no errors in your IDE's setup.
There is already code added in the files, so the program will begin even
if the functionality doesn't work yet.

\begin{intro}{Running a program}
Each time changes are made to a program,
it must be \textbf{compiled} (aka built).
This turns the source code into an executable file (.exe).
After you compile it, you can run the program.

\paragraph{Building and running with Code::Blocks:} asdf

\paragraph{Building and running with Visual Studio:} asdf
\end{intro}
~\\

Some code has already been written to get the program up and running
so it's easier to test out your work. When you run it, you will see
a menu like this:

\begin{lstlisting}[style=output]
******************************
* CS 200 Lab 1     Main Menu *
******************************
1. Run tests
2. Run program
3. Quit

SELECTION:
\end{lstlisting}

Later on you will be filling out code for some basic math. You can use
the \textbf{tests} to validate your work. If you run the tests
right now, without having changed anything in the code, you'll see this
output:

\begin{lstlisting}[style=output]
Test_AddTwoNumbers
TEST 1: 2 + 2 should be 4.                        FAIL!
TEST 2: 4 + 5 should be 9.                        FAIL!
TEST 3: 0 + 0 should be 0.                        FAIL!

Test_MultiplyTwoNumbers
TEST 1: 2 x 2 should be 4.                        FAIL!
TEST 2: 4 x 5 should be 20.                       FAIL!
TEST 3: 0 x 5 should be 0.                        FAIL!

Test_GetSquared
TEST 1: 2 squared should be 4.                    FAIL!
TEST 2: 4 squared should be 16.                   FAIL!
TEST 3: 10 squared should be 100.                 FAIL!

Test_GetOneMoreThan
TEST 1: One more than 2 is 3.                     FAIL!
TEST 2: One more than 5 is 6.                     FAIL!
TEST 3: One more than -5 is -4.                   FAIL!
\end{lstlisting}

As you implement each function in this lab, you should run the tester to
make sure you have the right logic. Once you're done with these functions,
then we'll implement the main program code, which will ask the user for
information and calculate the results.

\begin{intro}{Programming tip!}
	\begin{center}
		\includegraphics[height=3cm]{images/Cuties/floppy-confused.png}
	\end{center}

	When you're first starting with a programming language,
	you're learning the words and grammar;
	it is easy to make mistakes!
	~\\ ~\\
	This is why it is important to:

	\begin{itemize}
		\item	Build and test \underline{often}!
		\item	Don't implement \underline{everything} all at once!
	\end{itemize}
\end{intro}



\subsection*{Introduction to Functions}

\begin{intro}{Introducing functions?}
Before we get into practicing \textbf{variables}, \textbf{input}, and \textbf{output},
we need to have a quick introduction to \textbf{Functions}.
~\\~\\
Don't worry if it doesn't 100\% make sense right now - we will cover functions
more in-depth later on, but it's good to at least have some idea of what
is going on with these right now!
\end{intro}

In programming, we use functions as a way to calculate data (and other things).
Functions can take \textbf{input} parameters, and return some \textbf{output} data in return.

In algebra, with $f(x) = x^2$, $x$ would be our \textbf{input} variable,
and the result (\textbf{output}) is computed by the calulation $x^2$.
So, if we plugged in $x = 2$ as the input, the output $f(2)$ would be $2^2$, or $4$.

\begin{center}
	\includegraphics[width=5cm]{images/labimages/lab1/lab1-function.png}
\end{center}

We could plug in all sorts of numbers as the input $x$,
and the function would calculate and return different outputs...

\begin{center}
	\begin{tabular}{c | c}
		\textbf{Input $x$} 	& \textbf{Output $f(x)$} \\ \hline
		$x = 0$				& $f(0) = 0$
		\\
		$x = 1$				& $f(1) = 1$
		\\
		$x = 2$				& $f(2) = 4$
		\\
		$x = 3$				& $f(3) = 9$
		\\
		$x = 4$				& $f(4) = 16$
	\end{tabular}
\end{center}

In programming we also use functions.
In C++, we can have virtually any amount of \textbf{input} parameters,
but we can only return (\textbf{output}) one piece of information for each function (for now).

One of the plus sides of programming is that our function names can be any length (instead of just "f"),
and our variable names can be any length (instead of just "x"). ~\\

Implementing a function to calculate the square of a number would look like this in C++:

\begin{lstlisting}[style=code]
int Square( int x )
{
    return x * x;
}
\end{lstlisting}

There's more information here than the basic $f(x) = x^2$, so what is all this extra information?

\begin{center}
	\includegraphics[width=9cm]{images/labimages/lab1/lab1-function2.png}
\end{center}

With our functions in C++, we have need three parts to \textbf{declare} it...

\begin{itemize}
    \item	\textbf{Return type:} What kind of information will be returned from this function.
    \item	\textbf{Function name:} We use the name to call the function later on, and help describe what it does.
    \item	\textbf{Input parameters:} We can have 0, 1, or more input variables (parameters) that we will work with to compute the data.
\end{itemize}

The first line of code for a function is known as a \textbf{function header}.
It specifies what's the input, the return type, and gives the function a name.
Below the function header, there's more information: We use opening and closing
\textbf{curly braces} to create a \textbf{code block} - code that belongs
\textbf{inside the function}.

\begin{center}
	\includegraphics[width=9cm]{images/labimages/lab1/lab1-function3.png}
\end{center}

And, within the \textbf{code block}, we add in our computations, logic,
or anything else we need within this function. We use the
\texttt{return} keyword to specify what data gets returned from this function.

\begin{center}
	\includegraphics[width=9cm]{images/labimages/lab1/lab1-function4.png}
\end{center}

\hrulefill

\subsection*{Coding the math functions}

Open up the \textbf{functions.cpp} file in your IDE.
This is where the functions you will be modifing are. ~\\

All of these functions have \texttt{int} (integer) return types,
which means they return whole numbers only.
Their inputs are also integers, which means our input data will only be whole numbers.

\subsubsection*{int AddTwoNumbers( int num1, int num2 )}

\begin{lstlisting}[style=code]
int AddTwoNumbers( int num1, int num2 )
{
    return -1; // temporary - delete me
}
\end{lstlisting}

In this function, there are two input variables: \texttt{int num1} and \texttt{int num2}.
This function, \texttt{v}, is responsible for finding out the sum
of \texttt{num1} and \texttt{num2}, and returning this data as the output.  ~\\

Within this function, add \texttt{num1} and \texttt{num2} together.
Pass this out of the function by using the return keyword.

\begin{hint}{Hint: What is this ``return -1''?}
There is currently a \textbf{placeholder} return statement in this function:
\texttt{return -1;}
This is only here because C++ requires that all functions with an \texttt{int} return type
\textbf{must} return something; we cannot just leave the function blank!
However, once you add in your math, you can delete this line of code.
\end{hint}

\begin{hint}{Hint: How do I calculate the sum and return it?}
Once completed, this function will look like this:

\begin{lstlisting}[style=code]
int AddTwoNumbers( int num1, int num2 )
{
    return num1 + num2;
}
\end{lstlisting}
\end{hint}

Once you've implemented this function, \textbf{Build and Run} the program,
and select \textbf{Run tests} to verify your work. The tests for this function should pass:

\begin{lstlisting}[style=output]
Test_AddTwoNumbers
TEST 1: 2 + 2 should be 4.                        PASS!
TEST 2: 4 + 5 should be 9.                        PASS!
TEST 3: 0 + 0 should be 0.                        PASS!
\end{lstlisting}

\begin{intro}{Reminder!}
	Build, run, and test your program before continuing! If something doesn't
	work, now is the time to fix it before moving on!
	~\\~\\
	After implementing each function, build and run before moving to the next one!
\end{intro}

\hrulefill

\subsubsection*{int MultiplyTwoNumbers( int num1, int num2 )}

\begin{tabular}{l l}
	\textbf{Inputs:} & \texttt{num1} and \texttt{num2}, two integers. \\
	\textbf{Outputs:} & The product of \texttt{num1} and \texttt{num2}, also an integer.
\end{tabular}

~\\ Multiply \texttt{num1} by \texttt{num2} and return the result.

\begin{intro}{Reminder!}
	Build, run, and test. :)
\end{intro}

\hrulefill

\subsubsection*{int GetSquared( int num )}

\begin{tabular}{l l}
	\textbf{Inputs:} & \texttt{num}, the integer we want to square. \\
	\textbf{Outputs:} & The value of \texttt{num}$^2$.
\end{tabular}

~\\ In C++, you \textbf{cannot} calculate a square like this: \texttt{num\^{}2}; it won't work.
The easiest way to compute a square is simply to multiply the variable \texttt{num} by itself.

\begin{hint}{Hint: How do I calculate \texttt{num\^{}2}?}
You can use \texttt{return num * num;}
\end{hint}

\begin{intro}{Reminder!}
	Build, run, and test. :)
\end{intro}

\hrulefill

\subsubsection*{int GetOneMoreThan( int num )}

\begin{tabular}{l l}
	\textbf{Inputs:} & \texttt{num}, an integer that we want the next value above. \\
	\textbf{Outputs:} & The number that is one more than \texttt{num}.
\end{tabular}

\begin{intro}{Reminder!}
	Build, run, and test. :)
\end{intro}

\subsubsection*{Test results}

Once all the functions are implemented, the tests should all pass:

\begin{lstlisting}[style=output]
Test_AddTwoNumbers
TEST 1: 2 + 2 should be 4.                        PASS!
TEST 2: 4 + 5 should be 9.                        PASS!
TEST 3: 0 + 0 should be 0.                        PASS!

Test_MultiplyTwoNumbers
TEST 1: 2 x 2 should be 4.                        PASS!
TEST 2: 4 x 5 should be 20.                       PASS!
TEST 3: 0 x 5 should be 0.                        PASS!

Test_GetSquared
TEST 1: 2 squared should be 4.                    PASS!
TEST 2: 4 squared should be 16.                   PASS!
TEST 3: 10 squared should be 100.                 PASS!

Test_GetOneMoreThan
TEST 1: One more than 2 is 3.                     PASS!
TEST 2: One more than 5 is 6.                     PASS!
TEST 3: One more than -5 is -4.                   PASS!
\end{lstlisting}

Next, we're going to begin working on the program itself -
the sort of program a user would use to enter some values,
and ask the computer to do calculations. But first, some more introductions...

\hrulefill


\subsection*{Introduction to Variables}

A variable is a named ``container'' where you can store data in your program for use later.
Examples might be a user's age, the user name, an account balance, an address, etc.
A variable must have a \textbf{data type}, which specifies what \textit{kind} of data it can store.

\subsubsection*{Data types}

Some of the common simple data types in C++ are:

\begin{center}
	\begin{tabular}{p{3cm} p{5cm} p{5cm}}
		\textbf{Data type} & \textbf{Stores...} & \textbf{Example code} \\ \hline
		Integer & Whole numbers, including negative whole numbers and 0. &

		\texttt{int catAmount = 4;}

		\texttt{int pixelWidth = 32;}

		\\ \hline
		Float and Double & Numbers that can store a decimal or fractional component. &

		\texttt{float price = 9.95;}

		\texttt{double ratio = 1.235;}

		\\ \hline

		Character & A single letter, number, or symbol. Character literals must be stored in single-quotes.
		&

		\texttt{char currency = '\$'; }

		\texttt{char choice = 'a'; }

		\\ \hline

		String & Text, letters, sentences, etc. String literals must be stored in double-quotes.
		&

		\texttt{string town = "KC";}

		\texttt{string name = "Bob";}

		\\ \hline

		Boolean & \texttt{true} or \texttt{false} &

		\texttt{bool saved = false;}

		\texttt{bool quit = true;}

	\end{tabular}
\end{center}

\subsubsection*{Variable declarations}

A variable \textbf{declaration} is a line of code where we create our variable.
At minimum, we must specify the variable's \textbf{data type} and its \textbf{name}.

\begin{lstlisting}[style=code]
int score;
\end{lstlisting}

We can also \textbf{initialize} it with some value during its declaration.

\begin{lstlisting}[style=code]
int score = 0;
\end{lstlisting}

And, we can also declare multiple variables at the same time in the
same line of code, as long as they have the same data type.

\begin{lstlisting}[style=code]
float priceA, priceB, priceC;
\end{lstlisting}

\subsubsection*{Variable assignment}

In C++, the \textbf{assignment operator} is a single equal sign: \texttt{=}.
Use this to assign a value to a variable.

Assignments should be in this order:
\begin{itemize}
	\item	Left side: The variable name (where we're storing the data)
	\item	Middle: The assignment operator \texttt{=}
	\item	Right side: The data we're going to store in the variable;
			this can be a hard-coded value, a computation, or even
			copying data from a \textit{different} variable.
\end{itemize}

\begin{lstlisting}[style=code]
// Assign the variable a to the value 4.
a = 4;

// Assign the variable a to
// whatever is currently stored in b.
a = b;

// Assign the variable a to the result of 2 * 3.
a = 2 * 3;
\end{lstlisting}

\hrulefill

\subsection*{Introduction to I/O}

In C++, some of the most basic parts of a program are the \textbf{input and output}.
We can display text to the screen with the \texttt{cout} (console-out, \textit{``c-out''}) command,
and we can get text from the keyboard with the \texttt{cin} (console-in, \textit{``c-in''}) command.

\subsubsection*{\texttt{cout} to output text}

To display text to the screen, we use the \texttt{cout} command,
followed by the \textbf{output stream operator}: \texttt{<<}. After the stream operator,
you can display text to the screen, as long as it is written within double-quotes.

\begin{lstlisting}[style=code]
// Display Hello, world! to the screen.
cout << "Hello, world!";

// Add a new line before the next message.
cout << endl;

// Display Goodbye, world!
cout << "Goodbye, world!";
\end{lstlisting}

You can chain together as many strings and end-lines together as you want, so long as the stream operator is between each item...

\begin{lstlisting}[style=code]
// One cout statement, multiple things to output!
cout << "Hello, world!" << endl 
	<< "Goodbye, world!" << endl;
\end{lstlisting}

And if you have a variable, you can display the variable's value by writing the variable's name (NOT in double-quotes!)

\begin{lstlisting}[style=code]
// Display the text "Your username",
// followed by the value stored in the variable username
cout << "Your username: " << username << endl;
\end{lstlisting}

\paragraph{Special characters:} You can use some special escape characters to display special items in your output strings.

\begin{center}
	\begin{tabular}{c l}
		\textbf{Escape character} 	& \textbf{What it does} 	\\ 
		\texttt{ \textbackslash t } & Adds a tab in your text \\ 
		\texttt{ \textbackslash n } & Adds a new line, same as \texttt{endl} \\ 
		\texttt{ \textbackslash \textbackslash } & Allows you to output a backslash \\ 
		\texttt{ \textbackslash " } & Outputs double-quotes \\ 
		\texttt{ \textbackslash a } & Plays a beep sound (if supported) \\ 
	\end{tabular}
\end{center}


\subsubsection*{\texttt{cin} to input data}

You can use the \texttt{cin} statement to read text in from the keyboard. 
After the \texttt{cin} command, you need to use the \textbf{input stream operator} \texttt{>>} 
and then place a variable afterwards - this is the variable where the user's input will be stored.

\begin{lstlisting}[style=code]
// Display a message to the user
cout << "Enter your favorite color: ";

// Create a string variable to store the user's input in
string color;

// Receive only one word
cin >> color; 
\end{lstlisting}

Note that using \texttt{cin >>} only allows the user to enter one word
at a time; it doesn't not allow spaces in the input. We will learn more
about getting longer input later on.

\hrulefill

\subsection*{Introduction to function calls}



