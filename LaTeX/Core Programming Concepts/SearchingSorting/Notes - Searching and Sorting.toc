\contentsline {chapter}{\numberline {1}Intro to algorithm complexity}{2}
\contentsline {section}{\numberline {1.1}Thinking about algorithm efficiency}{2}
\contentsline {section}{\numberline {1.2}Counting operations}{3}
\contentsline {paragraph}{Single operations:}{3}
\contentsline {paragraph}{Looping:}{3}
\contentsline {subparagraph}{Single loops:}{3}
\contentsline {subparagraph}{Neighbor loops:}{3}
\contentsline {subparagraph}{Nested loops:}{4}
\contentsline {section}{\numberline {1.3}Thinking about operations generically}{5}
\contentsline {paragraph}{$O(1)$:}{5}
\contentsline {paragraph}{$O(n)$:}{5}
\contentsline {paragraph}{$O(n^2)$:}{6}
\contentsline {paragraph}{$O(log(n))$:}{7}
\contentsline {subsection}{\numberline {1.3.1}Best, Average, and Worst case times}{7}
\contentsline {chapter}{\numberline {2}Intro to Sorting}{8}
\contentsline {section}{\numberline {2.1}Insertion sort}{8}
\contentsline {section}{\numberline {2.2}Selection sort}{10}
\contentsline {section}{\numberline {2.3}More sorting algorithms}{13}
\contentsline {chapter}{\numberline {3}Searching}{14}
\contentsline {section}{\numberline {3.1}Linear search}{14}
\contentsline {section}{\numberline {3.2}Binary search}{15}
