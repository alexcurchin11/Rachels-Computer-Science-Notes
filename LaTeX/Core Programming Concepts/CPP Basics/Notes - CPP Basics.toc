\contentsline {chapter}{\numberline {1}Basic C++ Programs}{2}
\contentsline {paragraph}{int main():}{2}
\contentsline {paragraph}{\{ \}}{2}
\contentsline {paragraph}{return 0;:}{2}
\contentsline {section}{\numberline {1.1}Comments}{3}
\contentsline {paragraph}{Single-line comments:}{3}
\contentsline {paragraph}{Multi-line comments:}{3}
\contentsline {section}{\numberline {1.2}Variables}{3}
\contentsline {paragraph}{Declaring a variable:}{3}
\contentsline {paragraph}{Variable names:}{4}
\contentsline {paragraph}{Initializing a variable:}{4}
\contentsline {paragraph}{Variables and memory:}{4}
\contentsline {paragraph}{Garbage values:}{4}
\contentsline {subsection}{\numberline {1.2.1}Named constants}{4}
\contentsline {paragraph}{Example:}{5}
\contentsline {subsection}{\numberline {1.2.2}Data types}{6}
\contentsline {section}{\numberline {1.3}Input and Output}{7}
\contentsline {subsection}{\numberline {1.3.1}Outputting data}{7}
\contentsline {paragraph}{Outputting a string literal:}{7}
\contentsline {paragraph}{Outputting a variable value:}{8}
\contentsline {paragraph}{New lines:}{8}
\contentsline {paragraph}{Special characters:}{9}
\contentsline {subsection}{\numberline {1.3.2}Getting input}{10}
\contentsline {paragraph}{Getting a line of text}{10}
\contentsline {paragraph}{Program errors:}{10}
