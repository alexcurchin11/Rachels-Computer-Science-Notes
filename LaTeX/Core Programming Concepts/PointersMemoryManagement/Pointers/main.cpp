#include <iostream>
using namespace std;

int main()
{
    int * ptr;
    int var = 10;

    ptr = &var;

    cout << "ptr value:        " << ptr << endl;
    cout << "ptr dereferenced: " << *ptr << endl;

    *ptr = 20;

    cout << "var value:        " << var << endl;
    cout << "*ptr value:       " << *ptr << endl;

    return 0;
}
