

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=10cm]{images/tests.png}
    \end{center}
\end{figure}



    \section{Introduction: Validating your work}
    
    \begin{center}
        How do you know that your program actually works?
    \end{center}
    
\begin{figure}[h]
    \centering
    \begin{subfigure}{.5\textwidth}
        
        Sure, you may have manually tested your code, running the program
        and typing in test data and checking the output. ~\\
        
        After a while, manual testing becomes a chore. Maybe you start entering
        ``asdjhfklq'' as the test data and just make sure nothing
        breaks while running the program. ~\\
        
        But are you sure your program runs, doesn't crash, and gives the correct
        output for \textit{all reasonable cases}?
        
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
           \includegraphics[width=8cm]{images/hopeitworks.png}
    \end{subfigure}
\end{figure}

        \vspace{1cm}
        
        A skill that is good to develop as a software developer is how
        to test and validate your work. You can break down parts of your code
        into little transactions of ``inputs'' and ``outputs'', and then
        develop \textbf{test cases}.
        
        \newpage
        
        \paragraph{Test case:} A test case is a single test.
        You specify some \textbf{input(s)} that you will give your program,
        and the \textbf{expected output(s)} it should return.
        
        ~\\
        When you run the actual program with your inputs, it will return
        \textbf{actual output(s)}. Compare the actual output with the
        expected output to validate whether the program worked as expected.
        
        ~\\
        Test cases can be built without any of the program built so far.
        In fact, it can be handy to write your tests \textit{ahead of time}
        so you have a better understanding of how things are supposed to work.
        
        \begin{intro}{Example: Bank withdrawals}
            In this example, the program keeps track of a bank balance and the
            amount the user wants to withdraw, but it shouldn't let the balance fall
            below 0. 
            
            ~\\
            \begin{tabular}{c | p{3cm} | p{3cm} | p{3cm}}
                \footnotesize \textbf{Test case} & \footnotesize \textbf{Inputs} & \footnotesize \textbf{Expected output} & \footnotesize \textbf{Actual output}
                \\ \hline
                1 
                & balance = 100
                
                withdraw = 10
                &
                balance is now 90
                \\ \hline
                2
                & balance = 0
                
                withdraw = 100
                &
                can't withdraw!
                \\ \hline
                3
                & balance = 100
                
                withdraw = 100
                &
                balance is now 0
            \end{tabular}
            
            ~\\~\\
            You would make a list of inputs and what \textit{should} be the result
            for each case, and then run your program and check each scenario.
            If something doesn't match, it could mean that there's an error in a
            calculation somewhere, or other logic in the program.
            (And yes, sometimes tests can be wrong, too.)
        \end{intro}
        

        \subsection{The role of testing in software development}
        
            Companies that write big software products will often have
            multiple types of testing that they do to ensure the validity
            of their software. ~\\
                
            \textbf{QA} (Quality Assurance) people generally will be responsible
            for writing \textbf{test cases} and running through \textbf{manual tests}
            to validate the software works as intended. They might also
            write \textbf{test scripts} that automate going through the UI of
            the software, or automate other parts of testing.
            
            \textbf{Software Engineers} will regularly need to be able to write
            \textbf{unit tests} along with whatever features they're working on
            in order to validate that their new feature works as intended \textit{and}
            they will run older unit tests to ensure that their new feature
            \textbf{doesn't break anything already there}. ~\\
            
            Testing is an important part of software development and,
            from a student perspective, it is also a handy tool in ensuring that
            your programming projects work as intended before turning them in.


    \section{Designing tests}
    
        \subsection{Test cases}
        
            When writing out test cases, it's important to try to think
            of valid inputs and invalid inputs that could be entered in,
            and account for how the program will respond to that input.
            
            ~\\ There are three main pieces to defining a test case:
            
            \begin{enumerate}
                \item   Inputs entered in.
                \item   What the expected output of the program is.
                \item   What the program \textit{actually did}.
            \end{enumerate}
            
            By comparing the \textbf{expected result} with the \textbf{actual result},
            you can validate whether your program is working as intended.
            If they match - the test passes. If they don't match - the test failed.
            
            If a test fails, you can investigate the expected output and the actual
            output to help you get an idea of perhaps where it went wrong - perhaps
            a formula is wrong somewhere.
                
            \begin{center}
            \includegraphics[width=8cm]{Testing/images/validation.png}
            \end{center}
            
            \newpage
            \begin{intro}{Example: Calculate price plus tax}
                When ringing up a product at a store, there will be a price for that product
                and a sales tax that gets added onto it. Using a test case can
                help us make sure that the program's calculations are correct.
                
                ~\\
                Let's say that the programmer implemented the formula like this:
                
                \begin{center}
                \texttt{total = price + tax}
                \end{center}
                
                We could validate the program's actual output vs. the expected output,
                and see that there's an error.  
                
                ~\\
                \begin{tabular}{c | p{3cm} | p{3cm} | p{3cm}}
                    \footnotesize \textbf{Test case} & 
                    \footnotesize \textbf{Inputs} & 
                    \footnotesize \textbf{Expected output} & 
                    \footnotesize \textbf{Actual output}
                    \\ \hline
                    1 
                    & price = \$10.00
                    
                    tax = 0.09
                    &
                    \color{black} total = \$10.90
                    &
                    \color{red}total = \$10.09 \color{black}
                    
                    \\ \hline
                    2 
                    & price = \$5.45
                    
                    tax = 0.06
                    &
                    \color{black} total = \$5.78
                    &
                    \color{red}total = \$5.51 \color{black}
                    \\ \hline
                    3 
                    & price = \$2.25
                    
                    tax = 0.095
                    &
                    \color{black} total = \$2.46
                    &
                    \color{red}total = \$2.35 \color{black}
                \end{tabular}
                
                \vspace{1cm}
                
                What is the problem with the formula? Well, we don't just \textit{add tax}
                as a flat number; ``0.09'' is supposed to represent 9\%, not 9 cents!
                The correct formula would be:
                \begin{center}
                \texttt{total = price + (price * tax)}
                \end{center}
            \end{intro}
        
        \section{Manual testing}
        
\begin{figure}[h]
    \centering
    \begin{subfigure}{.5\textwidth}
        
            You can manually test your program effectively by documenting
            your test cases and running through each of them each time you
            decide to test your program. Of course it can take a while,
            but by using your test cases instead of entering in gibberish
            or only testing one scenario, you make sure your program works,
            even as you keep adding onto it.
        
    \end{subfigure}%
    \begin{subfigure}{.4\textwidth}
        \centering
            \includegraphics[width=4cm]{Testing/images/dontexplode.png}
    \end{subfigure}
\end{figure}

        
        \section{Automated unit tests}
        
            \begin{center}
                \includegraphics[width=12cm]{Testing/images/inputoutput.png}
            \end{center}
            
            Our test cases can be a handy roadmap to follow when running through your program's
            various features and validating it all works manually, but you can
            speed up the process by letting the computer do these checks for you.
            
            After all, when you manually validate outputs yourself, you're basically asking
            yourself, 
            
            \begin{center}
            ``If the actual output DOES NOT match the expected output, ~\\
            then the test failed.''
            \end{center}
                    
            A test that only validates \textbf{one function} is known as a 
            \textbf{unit test}, testing the smallest possible unit of a program.
            You could also write automated tests that checks several functions,
            but unit tests are good to validate each function on its own, independently of the rest.
            
            \subsection{Unit tests in the real world}
            
            At companies that maintain tests,\footnote{``Tests just mean more code to maintain. We don't keep tests.'' -- I was told this at a job interview once. I didn't take the job.}
            the software engineers will usually write unit tests to validate their work
            as they're adding in new functionality or making modifications to the existing code.
                       
            This means that \textit{ideally} each feature of the program has one or more
            test to validate that it works.
            
            This also means that, when adding or modifying features, you can run the
            entire test suite to make sure that \textit{all features still work}. ~\\
            
            Many software companies have something called \textbf{Continuous Integration (CI)},
            which is an automated system that kicks off a build of the software and
            runs all the tests each time a developer commits code to the repository.
            
            
            \newpage
            \subsubsection{Fixing errors with failing tests}
            
            \begin{intro}{Example: Area function}
                Let's say we have a function with the header 
                \begin{center}
                \texttt{int Area( int width, int length )}
                \end{center}
                
                and we don't necessarily know what's in it (we don't need that to test).
                We can write a series of test cases with inputs we specify and outputs
                we know to be correct in order to check the logic of the \texttt{Area} function...
                
                \begin{center}
                    \begin{tabular}{c | p{3cm} | p{3cm} | p{3cm}}
                        \footnotesize \textbf{Test case} & 
                        \footnotesize \textbf{Inputs} & 
                        \footnotesize \textbf{Expected output} & 
                        \footnotesize \textbf{Actual output} \\ \hline
                        1 & width = 10
                        
                            length = 20     & 120           \\ \hline
                        2 & width = 2
                        
                            length = 5      & 10            \\ \hline
                    \end{tabular}
                \end{center}
                
                ...and so on.
                
                We could use this to manually test by calling \texttt{Area} and checking the output it gives you,
                but you could also \textbf{write a function to validate it for us}...
                
                \begin{center}
                \includegraphics[width=12cm]{Testing/images/tedious.png}                
                \end{center}
            \end{intro}
            
            \newpage
            \begin{intro}{Example: Area function (Continued)}
                Here's some example code of a function that tests \texttt{Area()} for us.
\begin{lstlisting}[style=code]
void Test_Area()
{
  // Variables used for each test
  int width, length;
  int expectedOutput;
  int actualOutput;

  // Test 1
  width = 10;
  length = 20;
  expectedOutput = 200;
  actualOutput = Area( width, length );

  if ( actualOutput != expectedOutput ) {
    cout << "Test failed!"
      << "\n width: " << width
      << "\n length: " << length
      << "\n expected output: " << expectedOutput
      << "\n actual output: " << actualOutput 
      << endl;
  }
  else {
    cout << "Test 1 passed" << endl;
  }
}
\end{lstlisting}

                You can add as many tests as you'd like inside one function, testing
                different permutations of inputs against outputs to make sure that
                any reasonable scenarios are covered.
                To test, just call the function in your program, and check the output:
    
\begin{lstlisting}[style=output]
Test failed!
 width: 10
 length: 20
 expected output: 200
 actual output: 30
\end{lstlisting}

                If the test fails, you can \texttt{cout} your variables' values
                to help you figure out what might be wrong with the logic.
                
            \end{intro}
            
        
        \subsection{Other automated tests}
        
            \textbf{Unit tests} test the smallest unit of code - a single function.\footnote{It \textit{should} be the smallest unit, if your function is really long you should probably split it up!}
            There are other types of automated tests as well, and software tools to help
            developers create and run those tests. ~\\
            
            An \textbf{integration test} is when you test multiple units together.
            A \textit{unit} might work fine on its own, but when multiple units are
            put together, the resulting system may not work as intended.\footnote{Look up ``integration test unit test'' online for some good gif examples. :)}

            \begin{center}
            \includegraphics[width=10cm]{Testing/images/integrationtest.jpg}                
            \end{center}
            
            Tests can also be written to test \textbf{user interfaces} of a program,
            running a script to drive the cursor around the screen, or defining
            text that should be typed into fields, running through features of 
            the program from the user's perspective, and validating that
            data updates and is displayed when some action is performed from the UI.
