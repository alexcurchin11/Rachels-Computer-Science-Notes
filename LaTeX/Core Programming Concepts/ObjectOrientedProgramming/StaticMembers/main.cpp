#include <iostream>
#include <string>
using namespace std;

class Cat
{
    public:
    Cat()
    {
        catCount++;
    }

    static int GetCount()
    {
        return catCount;
    }

    private:
    string m_name;
    static int catCount;
};

int Cat::catCount = 0;

int main()
{
    Cat catA, catB, catC;

    cout << catA.GetCount() << endl;
    cout << catB.GetCount() << endl;
    cout << catC.GetCount() << endl;
    cout << Cat::GetCount() << endl;

    return 0;
}
