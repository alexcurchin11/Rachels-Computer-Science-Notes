\documentclass[a4paper,12pt,oneside]{book}
\usepackage[utf8]{inputenc}

\newcommand{\laTopic}       {Polymorphism}
\newcommand{\laTitle}       {Rachel's Computer Science Notes}
\newcounter{question}

\renewcommand{\chaptername}{Topic}

\usepackage{../../rachwidgets}
\usepackage{../../rachdiagrams}

\title{Introduction to C++}
\author{Rachel Singh}
\date{\today}

\pagestyle{fancy}
\fancyhf{}

\lhead{\laTopic \ / \laTitle}

\chead{}

\rhead{\thepage}

\rfoot{\tiny \thepage\ of \pageref{LastPage}}

\lfoot{\tiny Rachel Singh, last updated \today}

\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}

\begin{document}

    \begin{titlepage}
        \centering{}

        \sffamily{
            \textbf{
                {\fontsize{2cm}{3cm}\selectfont Core Computer Science Notes:}
                {\fontsize{2cm}{3cm}\selectfont \laTopic}
            }
        }

        \begin{figure}[h]
            \begin{center}
                \includegraphics[width=12cm]{../images/classes.png}
            \end{center}
        \end{figure}

        \sffamily{
            \textbf{
                An overview compiled by Rachel Singh
            }
        }
        
        \vspace{1cm} \small
        This work is licensed under a \\ Creative Commons Attribution 4.0 International License. ~\\~\\
        \includegraphics{../images/cc-by-88x31.png}
        ~\\~\\
        
        Last updated \today

    \end{titlepage}
    
	\tableofcontents
	
\newpage
\section{Polymorphism}
\subsection{Design and polymorphism}
So much of the design tricks and features we utilize in C++ and other
object oriented programming languages all stem from the concept of
\textbf{``do not repeat yourself''}. If you're writing the same
set of code in multiple places, there is a chance that we could
design the program so that we only need to write that code \textbf{once}.

\begin{figure}[h]
    \centering
    \begin{subfigure}{.3\textwidth}
        \centering
        \includegraphics[width=4cm]{Polymorphism/family.png}
    \end{subfigure}%
    \begin{subfigure}{.1\textwidth}  \tab[0.1cm]  
    \end{subfigure}%
    \begin{subfigure}{.7\textwidth}
        Polymorphism is a way that we can utilize pointers and something called
        \textbf{vtables} to have a family of classes (related by inheritance)
        and be able to write one set of code to handle interfacing with
        \textit{all of those family members}.
        We have a family tree of classes, and we can write our program to
        treat all the objects as the \textbf{parent class}, but the program will
        decide which set of functions to call at run time.
    \end{subfigure}
\end{figure}
 
 
\begin{lstlisting}[style=code]
Parent* myPtr = nullptr;
if      ( type == 1 ) { myPtr = new ChildA; }
else if ( type == 2 ) { myPtr = new ChildB; }

myPtr->Display();
delete myPtr;
\end{lstlisting}
 
\newpage
\subsubsection{Example: Quizzer and multiple question types} 
Let's say we are writing a quiz program and
there are different types of questions: True/false questions,
multiple choice, and fill-in-the-blank. They all have a common
\texttt{question} string, but how they store their answers is different...

\begin{center}
    \begin{tabular}{l l}
    
        \begin{tabular}{| l |} \hline
        \textbf{Question} 
        \\ \hline
        \# m\_question : string
        \\ \hline
        + bool AskQuestion() \\
        + void DisplayQuestion()
        \\ \hline
        \end{tabular}&
        
        \begin{tabular}{| l |} \hline
        \textbf{TrueFalseQuestion} 
        \\ \hline
        \# m\_answer : bool
        \\ \hline
        + bool AskQuestion() \\ \hline
        \end{tabular}
        
        \\ \\
    
        \begin{tabular}{| l |} \hline
        \textbf{MultipleChoiceQuestion} 
        \\ \hline
        \# m\_options : string[4] \\
        \# m\_correct : int
        \\ \hline
        + bool AskQuestion() \\
        + void ListAllAnswers()
        \\ \hline
        \end{tabular}&
    
        \begin{tabular}{| l |} \hline
        \textbf{FillInQuestion} 
        \\ \hline
        \# m\_answer : string
        \\ \hline
        + bool AskQuestion() \\ \hline
        \end{tabular}    
    \end{tabular}
\end{center}

How would you store a series of inter-mixed quiz questions in a program?
Without polymorphism, you might think to just have separate vectors or arrays
for all the questions:

\begin{lstlisting}[style=code]
vector<TrueFalseQuestion>       tfQuestions;
vector<MultipleChoiceQuestion>  mcQuestions;
vector<FillInQuestion>          fiQuestions;
\end{lstlisting}

Utilizing polymorphism in C++, we could simply store an array of
\textbf{pointers of the parent-type}:

\begin{lstlisting}[style=code]
vector<Question*> questions;
\end{lstlisting}

And then initialize the question as the type we want during creation:

\begin{lstlisting}[style=code]
questions.push_back( new TrueFalseQuestion );
questions.push_back( new MultipleChoiceQuestion );
questions.push_back( new FillInQuestion );
\end{lstlisting}

Since we are using the \textbf{new} keyword here, we would
also need to make sure to \textbf{delete} these items at the end
of the program:

\begin{lstlisting}[style=code]
for ( auto& question : questions )
{
    delete question;
}
\end{lstlisting}

\newpage
\subsubsection{Other design considerations}
When we're working with polymorphism in this way, we need to be able
to \textbf{treat each child as its parent}, from a ``calling functions''
perspective. Each child can have its own unique member functions and variables,
but when we're making calls to functions \textit{via a pointer to the parent type},
the parent only knows about functions that it, itself, has.
~\\

Let's say that the \textbf{Question} class a \textbf{DisplayQuestion()}
function. Since all its children use \texttt{m\_question} in the same way
and inherit this function, it will be fine to call it via the pointer.

\begin{lstlisting}[style=code]
ptrQuestion->DisplayQuestion(); // ok
\end{lstlisting}

But with a function that belongs to a child - not the parent's \textit{interface} -
we wouldn't be able to call that function via the pointer without \textbf{casting}.

\begin{lstlisting}[style=code]
ptrQuestion->ListAllAnswers();  // not ok

static_cast<MultipleChoiceQuestion*>(ptrQuestion)->ListAllAnswers(); // ok
\end{lstlisting}

You could, however, still call that \textbf{ListAllAnswers} function
from within \textbf{MultipleChoiceQuestion}'s \textbf{DisplayQuestion} function,
and that would still work fine...

\begin{lstlisting}[style=code]
bool MultipleChoiceQuestion::AskQuestion()
{
    DisplayQuestion();
    ListAllAnswers();
    //etc.
}
\end{lstlisting}

~\\~\\

Still fuzzy? That's OK, this is just an overview, we're going to step
into how all this works more in-depth next.

\newpage
\subsection{Review: Class inheritance and function overriding}


\begin{figure}[h]
    \centering
    \begin{subfigure}{.3\textwidth}
        \centering
        \includegraphics[width=5cm]{Polymorphism/questioninherit.png}
    \end{subfigure}%
    \begin{subfigure}{.1\textwidth}  \tab[0.1cm]  
    \end{subfigure}%
    \begin{subfigure}{.7\textwidth}
        
        Some things to remember about inheritance with classes:

        \begin{itemize}
            \item   Any \textbf{public} or \textbf{protected} members 
                    (functions and variables) are inherited by the child class.~\\
                    (e.g., \texttt{m\_question}, \texttt{DisplayQuestion()}, and \texttt{AskQuestion()}.)
            \item   A child class can \textbf{override} the a parent's function
                    by declaring and defining a function with the same signature. ~\\
                    (e.g., \texttt{AskQuestion()}.)
            \item   If the child class doesn't override a parent's function,
                    then when that function is called via the child object
                    it will call the parent's version of that function. ~\\
                    (e.g., \texttt{DisplayQuestion()}.)
        \end{itemize}
        
    \end{subfigure}
\end{figure}

\subsection{Review: Pointers to class objects}

You can declare a pointer to point to the address of an existing object,
or use the pointer to allocate memory for one or more new instances
of that class...

\begin{figure}[h]
    \centering
    \begin{subfigure}{.5\textwidth}
        \centering
        Pointer to existing address: ~\\
        \texttt{ myPtr = \&existingQuestion; }
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
        Pointer to allocate memory: ~\\  
        \texttt{ myPtr = new Question;}
    \end{subfigure}
\end{figure} ~\\

Then, to access a member of that object via the pointer, we use the \texttt{->} operator,
which is equivalent to dereferencing the pointer and then accessing a member:

\begin{figure}[h]
    \centering
    \begin{subfigure}{.5\textwidth}
        \centering
        Arrow operator: ~\\
        \texttt{ myPtr->DisplayQuestion(); }
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
        Dereference and access: ~\\  
        \texttt{ (*myPtr).DisplayQuestion(); }
    \end{subfigure}
\end{figure}

\newpage
\subsection{Which version of the method is called?}

\begin{figure}[h]
    \centering
    \begin{subfigure}{.7\textwidth}
        
        Let's say we have several objects already declared:
    
\begin{lstlisting}[style=code]
Question q1, q2;
MultipleChoiceQuestion mc1;
\end{lstlisting} ~\\

        We could create a \texttt{Question*} ptr that points to
        \texttt{q1} or \texttt{q2} or even \texttt{mc1}...

\begin{lstlisting}[style=code]
Question* ptr;
ptr = &q1;  // ok
ptr = &q2;  // ok
ptr = &mc1; // ok?
\end{lstlisting}


    \end{subfigure}%
    \begin{subfigure}{.1\textwidth}  \tab[0.1cm]  
    \end{subfigure}%
    \begin{subfigure}{.3\textwidth}
        \centering
        \includegraphics[width=5cm]{Polymorphism/questioninherit.png}
    \end{subfigure}
\end{figure}

And, any functions that the \texttt{Question} class and
the \texttt{MultipleChoiceQuestion} class could be called
from this pointer...

\begin{lstlisting}[style=code]
ptr->DisplayQuestion();
\end{lstlisting} 

This is fine for any member methods \textbf{not overridden} by the child class.
But, which version of the function is called if we used an \textbf{overridden} method?

\begin{lstlisting}[style=code]
ptr->AskQuestion();
\end{lstlisting}

\begin{center}
\includegraphics[width=\textwidth]{Polymorphism/whichone.png}
\end{center}

\paragraph{No virtual methods - Which \texttt{AskQuestion()} is called?}
~\\
Let's say our class declarations look like this:

~\\ \textbf{Question:}
\begin{lstlisting}[style=code]
class Question
{
    public:
    bool AskQuestion();
    // etc.
};
\end{lstlisting}

~\\ \textbf{MultipleChoiceQuestion:}
\begin{lstlisting}[style=code]
class MultipleChoiceQuestion : public Question
{
    public:
    bool AskQuestion();
    // etc.
};
\end{lstlisting}

Here are the outputs we could have from using pointers in different ways:

\begin{mdframed}[backgroundcolor=colorblind_light_blue]
\texttt{Question* ptr = new Question;} ~\\
\texttt{bool result = ptr->AskQuestion();} ~\\~\\
\textbf{Question}'s AskQuestion() is called.
\end{mdframed}

\begin{mdframed}[backgroundcolor=colorblind_light_yellow]
\texttt{MultipleChoiceQuestion* ptr = new MultipleChoiceQuestion;} ~\\
\texttt{bool result = ptr->AskQuestion();} ~\\~\\
\textbf{MultipleChoiceQuestion}'s AskQuestion() is called.
\end{mdframed}

\begin{mdframed}[backgroundcolor=colorblind_light_blue]
\texttt{Question* ptr = new MultipleChoiceQuestion;} ~\\
\texttt{bool result = ptr->AskQuestion();} ~\\~\\
\textbf{Question}'s AskQuestion() is called.
\end{mdframed}

``Well, how is that useful at all? The function called matches the
pointer data type!'' - true, but we're missing one piece that allows us to
call \textbf{any child's version of the method} from a pointer of the parent type...


\newpage
\paragraph{Virtual methods - Which \texttt{AskQuestion()} is called?}
~\\
Instead, let's mark our method with the \textbf{virtual} keyword:

~\\ \textbf{Question:}
\begin{lstlisting}[style=code]
class Question
{
    public:
    virtual bool AskQuestion();
    // etc.
};
\end{lstlisting}

~\\ \textbf{MultipleChoiceQuestion:}
\begin{lstlisting}[style=code]
class MultipleChoiceQuestion : public Question
{
    public:
    virtual bool AskQuestion();
    // etc.
};
\end{lstlisting}

Here are the outputs we could have from using pointers in different ways:

\begin{mdframed}[backgroundcolor=colorblind_light_blue]
\texttt{Question* ptr = new Question;} ~\\
\texttt{bool result = ptr->AskQuestion();} ~\\~\\
\textbf{Question}'s AskQuestion() is called.
\end{mdframed}

\begin{mdframed}[backgroundcolor=colorblind_light_yellow]
\texttt{MultipleChoiceQuestion* ptr = new MultipleChoiceQuestion;} ~\\
\texttt{bool result = ptr->AskQuestion();} ~\\~\\
\textbf{MultipleChoiceQuestion}'s AskQuestion() is called.
\end{mdframed}

\begin{mdframed}[backgroundcolor=colorblind_light_yellow]
\texttt{Question* ptr = new MultipleChoiceQuestion;} ~\\
\texttt{bool result = ptr->AskQuestion();} ~\\~\\
\textbf{MultipleChoiceQuestion}'s AskQuestion() is called.
\end{mdframed}

With this, we can now store a list of \textbf{Question*} objects,
and each question can be a different child class, but we can
write one set of code to interact with each one of them.

\newpage
\subsection{Virtual methods, late binding, and the Virtual Table}

By using the \textbf{virtual} keyword, something happens with our functions -
it allows the pointer-to-the-parent class to figure out \textit{which version}
of the method to actually call, instead of just defaulting to the parent class' version.
But how does this work?

\paragraph{The \texttt{virtual} keyword} tells the compiler that the function
called will be figured out later. By marking a function as \textbf{virtual},
it then is added to something called a \textbf{virtual table} - or \textbf{vtable}.

The \textbf{vtable} stores special \textit{pointers to functions}. If a class
contains \textit{at least one virtual function}, then it will have its own vtable.

\begin{center}
\includegraphics[width=12cm]{Polymorphism/vtables1.png}
\end{center}

With the \textbf{Question} class, it isn't inheriting any methods from anywhere
else so the vtable reflects the same methods it has. But, we also have
the child class that inherits \textbf{DisplayQuestion()} and overrides \textbf{AskQuestion()}.

\begin{center}
\includegraphics[width=12cm]{Polymorphism/vtables2.png}
\end{center}

Because of these \textbf{vtables}, we can then have our pointers reference
this vtable when figuring out which version of a method to call.
Doing this is called \textbf{late binding} or \textbf{dynamic binding}.

\newpage
\subsection{When should we use \texttt{virtual}?}

\paragraph{Destructors should always be virtual.} If you're working with inheritance.
By making your destructor \textbf{virtual} for each class in the family,
you are ensuring that the \textbf{correct destructor} will be called when
the object is destroyed or goes out of scope. If you don't make it virtual
and utilize polymorphism, the correct destructor may not be called
(i.e., \texttt{Question}'s instead of \texttt{MultipleChoiceQuestion}'s).

\paragraph{Constructors cannot be marked \texttt{virtual}.}
When the object is instantiated (e.g., \texttt{ptr = new MultipleChoiceQuestion;})
that class' constructor will be called already.

\paragraph{Not every function needs to be virtual.}
It's all about design. Though generally, if you always want the parent's
version of a method to be called, you wouldn't override that method in
the child class anyway.

\newpage
\subsection{Designing interfaces with pure virtual functions and abstract classes}

Polymorphism works best if you're designing a family of classes around
some sort of \textbf{interface} that they will all share. In the C\# language,
there is an interface type that is available to you, but that's not here
in C++, so we implement it via classes.

\paragraph{What is an Interface?} ~\\

When we're designing a class to be an interface, the idea is that the
user (or other programmers) will just see a set of functions it will
interface with - none of the behind-the-scenes, how-it-works stuff.

Most of the devices we use have some sort of \textbf{interface},
hiding the more complicated specifics of how it actually works within a case.
For example, a calculator has a simple interface of buttons, but if you opened
it up you would be able to see its hardware and how everything is hooked up.

We use the same idea with writing software, where we expose some interface
(in the form of the class' \textbf{public methods}) as how the ``user''
interacts with our class.

\begin{center}
\includegraphics[width=8cm]{Polymorphism/questionmanager.png}
\end{center}

A common design practice is to write the first \textbf{base (parent) class}
to be a specification of this sort of \textbf{interface} that all its children
will adhere to, and to ensure that each child class \textbf{must follow the interface}
by using something that the compiler will enforce itself: pure virtual functions.

\newpage

When working with our Quiz program idea, our \textbf{base class} is \texttt{Question},
which would define the interface for all other types of Questions.
Generally, our base interface class \textbf{would never be instantiated} -
it is not complete in and of itself (i.e., a Question with no types of Answers) -
but is merely used to outline a common interface for its family members.

~\\
Here is a blank diagram with just the member variables defined, but not yet
any functionality, so that we can begin to step through thinking about an interface:

\begin{center}
\includegraphics[width=\textwidth]{Polymorphism/questionfamily.png}
\end{center}

Thinking in terms of implementing a program that could \textbf{edit questions}
(such as the teacher's view of the quiz), as well as that could
\textbf{ask questions} (such as the student's view), we can try to think of
what kind of functionality we would need from a question...

\begin{itemize}
    \item   Setup the question, answer(s)
    \item   Display the question to the user
    \item   Get the user's answer
    \item   Check if the user's answer was correct
\end{itemize}

But, the specifics of how each of these question types stores the correct
answer (and what data type it is) and validates it differ between each of them...

\begin{center}
    \begin{tabular}{p{3cm} | p{1.5cm} p{3cm} p{4cm}  }
                                & \textbf{User}     & \textbf{Stored answer}    & \textbf{Validate} \\ \hline
        \textbf{True/false}     & bool              & bool answer               & \footnotesize User input == answer? \\ \hline
        \textbf{MultiChoice}    & int               & string options[4]
        
                                                      int answer                & \footnotesize User input == answer? \\ \hline
        \textbf{FillIn}         & string            & string answer             & \footnotesize User input == answer?
    \end{tabular}
\end{center}

We could design our Questions so that they have functionality that interacts
with the user directly (e.g., a bool function that asks the user to enter
their response and returns true if they got it right and false if not)
rather than writing functions around returning the actual answer (which would
be more difficult because they have different data types).

\begin{itemize}
    \item   Set up question
    \item   Run question
\end{itemize}

\paragraph{Declarations:}~\\
We can set up a simple interface for our Questions with these functions.
They've been marked as \texttt{virtual}, which allows us to use polymorphism,
and they've also been marked with \texttt{= 0} at the end, marking them as
\textbf{pure virtual} - this tells the compiler that child classes \textbf{must}
implement their own version of these methods. A function that contains
pure virtual methods is called an \textbf{abstract class}.

\begin{lstlisting}[style=code]
class Question
{
  public:
  virtual void Setup() = 0;
  virtual bool Run() = 0;

  protected:
  string m_question;
};
\end{lstlisting}
\vspace{0.5cm}

Now our child classes can inherit from \texttt{Question}. They will be
required to override \texttt{Setup()} and \texttt{Run()}, and we can also
have additional functions as needed for that implementation:

\begin{lstlisting}[style=code]
class MultipleChoiceQuestion : public Question
{
  public:
  virtual void Setup();
  virtual bool Run();
  void ListAllAnswers();

  protected:
  string m_options[4];
  int m_answer;
};
\end{lstlisting}

\newpage
\paragraph{Definitions:}~\\
Each class will have its own implementation of these interface functions,
but since they're part of an interface, when we build a program around
these classes later we can call all of them the same way.

~\\ \textbf{Question:}
\begin{lstlisting}[style=code]
void Question::Setup() {
    cout << "Enter question: ";
    getline( cin, m_question );
}
\end{lstlisting}

~\\ \textbf{TrueFalseQuestion:}
\begin{lstlisting}[style=code]
void TrueFalseQuestion::Setup() {
    Question::Setup();    
    cout << "Enter answer (0 = false, 1 = true): ";
    cin >> m_answer;
}
\end{lstlisting}

~\\ \textbf{MultipleChoiceQuestion:}
\begin{lstlisting}[style=code]
void MultipleChoiceQuestion::Setup() {
    Question::Setup();
    
    for ( int i = 0; i < 4; i++ )
    {
        cout << "Enter option " << i << ": ";
        getline( cin, m_options[i] );
    }
    
    cout << "Which index is correct? ";
    cin >> m_answer;
}
\end{lstlisting}

~\\ \textbf{FillInQuestion:}
\begin{lstlisting}[style=code]
void FillInQuestion::Setup() {
    Question::Setup();
    cout << "Enter answer text: ";
    getline( cin, m_answer );
}
\end{lstlisting}

\newpage
\paragraph{Function calls:}~\\
Now, no matter what \textit{kind} of question subclass we're using,
we can utilize the same interface - and the same code.

\begin{lstlisting}[style=code]
// Create the pointer
Question* ptr = nullptr;

// Allocate memory
if ( choice == "true-false" )
{
    ptr = new TrueFalseQuestion();
}
else if ( choice == "multiple-choice" )
{
    ptr = new MultipleChoiceQuestion();
}
else if ( choice == "fill-in" )
{
    ptr = new FillInQuestion();
}

// Set up the question
ptr->Setup();

// Run the question
ptr->Run();

// Free the memory
delete ptr;
\end{lstlisting}

And, utilizing this interface, we could then store a \texttt{vector<Question*>}
and set up each question as any question subclass without any duplicate code.



\end{document}

