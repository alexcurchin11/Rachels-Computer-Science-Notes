#ifndef _MOVIE_HPP
#define _MOVIE_HPP

#include <string>
using namespace std;

class Movie
{
    public:
    void Setup();
    void Update();
    void Display();

    private:
    string title;
    string rating;
    string genre;
    int year;
};

#endif
