#include "Movie.hpp"

#include <iostream>
using namespace std;


void Movie::Setup()
{
    cout << "Enter title: ";
    cin.ignore();
    getline( cin, title );

    cout << "Enter genre: ";
    getline( cin, genre );

    cout << "Enter rating: ";
    getline( cin, rating );

    cout << "Enter year: ";
    cin >> year;
}

void Movie::Update()
{
    Setup();
}

void Movie::Display()
{
    cout << "Title:  " << title << endl
         << "Genre:  " << genre << endl
         << "Rating: " << rating << endl
         << "Year:   " << year << endl;
}
