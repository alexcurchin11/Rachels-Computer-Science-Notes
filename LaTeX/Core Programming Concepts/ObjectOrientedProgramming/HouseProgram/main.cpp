#include <iostream>
using namespace std;

#include "House.hpp"

int main()
{
    House myHouse;

    cout << "HOUSE BUILDER" << endl;
    bool done = false;
    while ( !done )
    {
        cout << "(A)dd new room or (Q)uit: ";
        char choice;
        cin >> choice;

        if ( toupper(choice) == 'A' )
        {
            myHouse.AddRoom();
        }
        else if ( toupper(choice) == 'Q' )
        {
            done = true;
        }
        cout << endl;
    }

    return 0;
}
