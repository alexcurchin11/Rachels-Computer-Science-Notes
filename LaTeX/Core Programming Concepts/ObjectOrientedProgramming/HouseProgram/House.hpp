#ifndef _HOUSE_HPP
#define _HOUSE_HPP

#include "Room.hpp"

const int MAX_ROOMS = 10;

class House
{
    public:
    House();
    ~House();

    void AddRoom();

    private:
    Room rooms[MAX_ROOMS];
    int roomCount;
};

#endif
