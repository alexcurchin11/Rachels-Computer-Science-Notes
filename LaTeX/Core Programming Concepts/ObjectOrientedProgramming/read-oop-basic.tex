%------------------------------------%

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=12cm]{images/classes.png}
    \end{center}
\end{figure}

    \section{Introduction to Object Oriented Programming}

    \subsection{Programming paradigms}
    
        Programming paradigms (pronounced ``pair-uh-dimes'') are ways we can
        classify different programming languages based on features they have
        available or the style programs are written. Different paradigms
        have popped up over the life of computers as programming languages
        grow and evolve.
        
        \paragraph{Machine code:}
        At the beginning, computers were programmed with \textbf{machine code},
        where you work directly with instructions supported by the hardware -
        for example, \texttt{add}, \texttt{sub}, storing data in registers,
        and other relatively simple commands.
        
        \paragraph{Procedural Languages:}
        Eventually, those commands were abstracted into higher-level languages,
        where one command in, say, C, could ``translate'' to several machine-code
        instructions. Languages like C, Fortran, Algol, BASIC, and C are
        known as \textbf{Procedural Languages}, where these programs would
        describe a procedure to follow and instructions are executed one-at-a-time
        in a specific order (top-to-bottom, or calling a function and returning).
        
        \paragraph{Object Oriented Programming:}
        Languages like C supported our basic control flow and using functions,
        but did not include \textbf{classes} - a way to make more sophisticated
        data types. A \textbf{class} is a structure that can store its own
        \textbf{member variables and member functions}. A variable whose
        data type is from some defined class is known as an \textbf{object}.
    
        There are other programming paradigms, but we are going to focus on
        Object Oriented Programming (OOP) now since it's a large part of
        using C++ and other languages like Java, C\#, and Python.
    
    \subsection{Design ideals}
    In programming, an \textbf{object} is some sort of structure that
    stores its own data (variables) and functionality (functions).
    We try to design our programs as a collection of objects that
    interact with each other to get some job done. ~\\
    
    Designing programs in an OOP style helps us pursue certain design goals:
    
    \begin{itemize}
        %\item	\textbf{Abstraction:} Modeling real-world problems digitally
        %        by representing them as a series of objects that work together.
        
        \item	\textbf{Encapsulation:} Giving the user / other programmers
                an ``interface'' to interact with the objects, hiding the
                inner-workings within the class.  
        \begin{itemize}
            \item	Certain functions are made \textbf{public} that other programmers can use to interface with the object.
            \item	The other programmers don't need to worry about the inner-workings of the object in order to use it.
            \item	The developer of the class can modify \textit{how} the internals work without breaking the public interface.
            \item	Helps protect the data within the class from being accessed or modified by things it shouldn't.
        \end{itemize}
                
        \item	\textbf{Loose coupling:} Ideally, different objects in a program
                shouldn't have their functionality tied to other objects too closely;
                we want to reduce inter-dependence between objects. When objects
                are more independent from each other, they are \textit{loosely coupled}.
                
        \item	\textbf{High cohesion:} When we design our objects, we shouldn't just
                throw everything and the kitchen sink into one object. To design an object
                with \textit{high cohesion} means that everything inside the object
                \textit{belongs} in that object. Reduce the clutter.
    \end{itemize}
    
    
    \newpage
    \subsection{Example: Moving to an OOP design}
    Let's think about a simple program we can make with just functions
    and arrays for contrast: A library of the movies a user owns.
    
    For this program, we could start with an array of strings to store
    movie titles, but we'd have to create new arrays for each additional
    field to keep track of - year, genre, rating, etc.

\begin{lstlisting}[style=code]
string movieTitles[MAX_MOVIES];
string movieRatings[MAX_MOVIES];
string movieGenre[MAX_MOVIES];
int movieYearReleased[MAX_MOVIES];
\end{lstlisting}
    
    In addition to our arrays, we need functions to deal with different
    operations we would want to do on our movie collection...
    
\begin{lstlisting}[style=code]
void AddMovie( 
string movieTitles[], string movieRatings[], 
string movieGenre[], int movieYears[], 
int& savedMovies, const int MAX_MOVIES );
void UpdateMovie( 
string movieTitles[], string movieRatings[], 
string movieGenre[], int movieYears[], 
int& savedMovies );
\end{lstlisting}

    \subparagraph{Cons of this style:}
    \begin{itemize}
        \item	We have to add logic in our program to make sure that
                item $i$ in \texttt{movieTitles} and item $i$ in \texttt{movieRatings}
                will refer to the same movie. There's nothing in C++
                that will directly link these things so we have to add the logic ourselves.
        \item	Have to keep track of four different arrays and have to pass four different arrays between functions.
    \end{itemize}

    Instead, we can take this general structure and turn it into an \textbf{object},
    like this:
    
    \begin{center}
        \begin{tabular}{| l |}
            \hline
            \multicolumn{1}{|c|}{\textbf{Movie}}
            \\ \hline
            - title : string \\
            - rating : string \\
            - genre : string \\
            - year : int \\ \hline
            + Setup() \\
            + Update() \\
            + Display() \\ \hline
        \end{tabular}
    \end{center}
    
    The \textbf{Movie} object would store information about the movie
    and a set of functionality we would do on a movie. During the ``Add Movie''
    feature of the program, we would be calling the \textbf{Setup()} function
    of the Movie object to get its information all set up.
    
    In addition to our Movie object, we could also build an object that
    is the \textbf{library itself} - what data and functionality does our
    library of movies have?

    \begin{center}
        \begin{tabular}{| l |}
            \hline
            \multicolumn{1}{|c|}{\textbf{MovieLibrary}}
            \\ \hline
            - movieList[MAX\_MOVIES] : Movie \\ 
            - totalMovies : int \\ 
            - MAX\_MOVIES : const int 
            \\ \hline
            + AddMovie() \\
            + UpdateMovie() \\
            + ClearMovies() \\
            + DisplayAllMovies() \\ 
            + SaveList( path:string ) \\ 
            + LoadList( path:string ) \\ \hline
        \end{tabular}
    \end{center}
    
    Our movie library would contain one array - an array of \textbf{Movie}
    objects - instead of the four separate arrays for each of the movie fields.
    The library itself would have the Add, Update, Clear, etc. functions
    and know how to handle that functionality. ~\\
    
    Finally, in our main() function we would declare a \textbf{MovieLibrary} variable
    for our single library - or, we could make an array of \textbf{MovieLibrary} objects
    if the program accomodated multiple users!
    
    After initialization, we would have our main menu as usual...

\begin{lstlisting}[style=output]
1. Add movie
2. Update movie
(etc)
\end{lstlisting}

    and when a choice is made, we call the appropriate function \textit{via}
    our MovieLibrary object.
            
    ~\\ Creating the MovieLibrary object:
\begin{lstlisting}[style=code]
int main()
{
// Creating a MovieLibrary object
MovieLibrary movieLibrary;
\end{lstlisting}

~\\ Calling the AddMovie function of MovieLibrary:
\begin{lstlisting}[style=code]
if ( choice == 1 )
{
    // Calling the AddMovie function
    movieLibrary.AddMovie();
}
\end{lstlisting} ~\\

When defining the \textbf{AddMovie()} function, it could call
the \textbf{Setup()} function for the next \textbf{Movie} object
in the array...

\begin{lstlisting}[style=code]
void MovieLibrary::AddMovie()
{
movieList[ totalMovies ].Setup();
totalMovies++;
}
\end{lstlisting}

And the \textbf{Movie} object's \textbf{Setup()} function could
handle dealing with the inputs and outputs...

\begin{lstlisting}[style=code]
void Movie::Setup() {
cout << "Enter a title: ":
getline( cin, title );

cout << "Enter a rating: ";
getline( cin, rating );

cout << "Enter a genre: ";
getline( cin, genre );

cout << "Enter a year: ";
cin >> year;
}
\end{lstlisting}

\newpage

~\\ Here's a diagram of the main program and objects:
\begin{center}
    \includegraphics[width=14cm]{ObjectOrientedProgramming/library.png}
\end{center}

\newpage
Here are a few more examples of using object oriented design in different
types of programs:

\begin{center}
    \includegraphics[width=8cm]{ObjectOrientedProgramming/game.png}
\end{center}

    \paragraph{Video game} ~\\
    
    \begin{tabular}{l l l}
        \begin{tabular}{| l |}
            \hline
            \multicolumn{1}{|c|}{\textbf{Player}}
            \\ \hline
            - x : int \\ 
            - y : int \\ 
            - width : int \\
            - height : int \\
            - image : Bitmap
            \\ \hline
            + Setup() \\
            + Move() \\
            + Jump() \\
            + Attack() \\
            + Draw() \\ \hline
        \end{tabular}
        &
        \begin{tabular}{| l |}
            \hline
            \multicolumn{1}{|c|}{\textbf{Tile}}
            \\ \hline
            - x : int \\ 
            - y : int \\ 
            - width : int \\
            - height : int \\
            - image : Bitmap 
            \\ \hline
            + Setup() \\
            + Draw() \\ \hline
        \end{tabular}
        &
        \begin{tabular}{| l |}
            \hline
            \multicolumn{1}{|c|}{\textbf{Level}}
            \\ \hline
            - tiles[ ][ ] : Tile
            \\ \hline
            + Load() \\
            + Draw()
            \\ \hline
        \end{tabular}
    \end{tabular}
    
    ~\\
    A video game usually has characters that can move around,
    a lot of 2D game levels are built out of a grid of tiles.
    
    Anything that gets displayed to the screen will need $(x,y)$ coordinates
    as well as dimensions like width and height. C++ doesn't have a built-in
    Bitmap object, but there are graphical libraries that provide graphics functionality.

\newpage
\paragraph{College} ~\\
    A college could be separated into multiple different objects,
    where a \textbf{College} might have different \textbf{Campuses}
    (such as KU-Lawrence, KU-Edwards or the different MCCKC campuses),
    and each campus has an array of different \textbf{Departments} (CSIS, MATH,
    ENG, etc.), each department has an array of \textbf{Teachers}
    and an array of \textbf{Courses}. A course would generally have
    one \textbf{Teacher} and an array of \textbf{Students}.

    \begin{center}
        \includegraphics[width=14cm]{ObjectOrientedProgramming/campus.png}
    \end{center}

    As far as functionality goes, this diagram has only some basic
    functions, like \textbf{SetTeacher} for a course, or \textbf{AddStudent}.
    The idea is that, anything related to a course would go in the
    Course object, with each object handling its own little domain.

\section{Structs}

    \subsection{Structs vs. Classes}

    Structs in C++ have all the same functionality of a Class, but
    are generally used design-wise to group a few variables together,
    maybe some functions, into a simple structure. A class, on the other hand,
    is usually used for much bigger and more complex objects.
    
    In C++, the only difference between Structs and Classes are default accessibility -
    if you don't specify the accessibility of your variables/functions within
    a struct, they are \textbf{public} by default. This means that anything
    in the program can access those variables/functions. For Classes,
    everything is \textbf{private} by default - only that Class itself
    can use the variables/functions.
    
    \subsection{Example structs}
    
    Starting off small, some examples of what kind of \textbf{structs}
    one might define are...
    
    ~\\ Grouping $x$ and $y$ coordinates together in a Coordinate Pair...
\begin{lstlisting}[style=code]
struct CoordinatePair
{
    float x, y;
};
\end{lstlisting}

    \newpage
    ~\\ Grouping a numerator and denominator together for a Fraction...
\begin{lstlisting}[style=code]
struct Fraction
{
    // Member variables
    float numerator, denominator;

    // Member functions (method)
    void Display()
    {
        cout << numerator << "/" << denominator;
    }

    void Multiply( Fraction otherFraction )
    {
        numerator *= otherFraction.numerator;
        denominator *= otherFraction.denominator;
    }

    void Divide( Fraction otherFraction )
    {
        numerator *= otherFraction.denominator;
        denominator *= otherFraction.numerator;
    }
};
\end{lstlisting}

Member functions can be defined \textit{within} its struct/class,
but the more traditional design requires putting the struct/class
declaration in a header file (.h or .hpp) and the member functions
into their own source file (.cpp).

We will go over this later on.

\newpage
\subsection{Declaring object variables}
Once a struct has been declared in a program, you can then create
a variable with that data type. To access the internal variables
of the struct, you use the variable's name followed by the dot operator .
and then the name of the member variable or function.

\begin{lstlisting}[style=code]
// Declare two Fraction variables
Fraction frac1, frac2;

// Set values
frac1.numerator = 1;
frac1.denominator = 2;

frac2.numerator = 2;
frac2.denominator = 3;

// Multiply frac1 by frac2
frac1.Multiply( frac2 );

// Display the fraction
frac1.Display();
\end{lstlisting}

We will have more examples during the section on Classes,
since that's mostly what we will be using. Again, structs are useful
for combining a small amount of member variables together under one name,
usually used for structures for math (coordinates, rectangles, etc.)
but more sophisticated objects ought to be created with a \textbf{class}.

\newpage
\section{Classes}

    Traditionally, a \textbf{struct} is used to create small objects
    that join a few variables together. Classes are much more heavily
    used in C++ and is the back-bone of Object Oriented Programming.
    There is a lot we can do with classes, but for now we are just
    going to look at the basics.
    
        A class declaration looks just like a struct declaration
        except that we use the keyword \textbf{class}. Take note that
        with a struct and class declaration, we must end the closing
        curly brace with a semi-colon.
        
\begin{lstlisting}[style=code]
class Player
{
    public:
    void SetPosition( int newX, int newY );
    void Move();

    private:
    int x, y;
};
\end{lstlisting}

        \subsection{Accessibility}
        
        We can define our member variables and functions with
        three different levels of accessibility, dictating where
        these members can be accessed throughout the program:
        
        \begin{itemize}
            \item	\textbf{public:} Any part of the program can access these members.
                    This is usually used for the member functions of a class.
                    
            \item	\textbf{private:} These members can \underline{only} be accessed
                    by the class itself, from within its functions.
                    
            \item	\textbf{protected:} Similar to private except that classes that
                    \textit{inherit} from the class we're defining will also have
                    access to protected members. More on this when we cover \textbf{inheritance}.
        \end{itemize}
        
        \subsection{Header and Source files}
        
        When we are creating a class, we generally will put the class
        \textbf{declaration} within its own header file. Header files
        end with .h or .hpp - I tend to use .hpp since we're using C++
        and I like to explicitly state this is a "C++ header"; the .h
        extension was also used in C.
        
        ~\\ Class declaration goes in Rectangle.hpp:
        
\begin{lstlisting}[style=code]
#ifndef _RECTANGLE_HPP
#define _RECTANGLE_HPP

class Rectangle
{
    public:
    void SetPosition( int newX, int newY );
    void SetDimensions( int newWidth, int newHeight );
    private:
    int x, int y, int width, int height;
};

#endif
\end{lstlisting}

        \begin{hint}{What is \#ifndef?}
        \footnotesize
        In C++, when we \texttt{\#include "Rectangle.hpp"} in a different source file,
        the compiler essentially \textit{copy-pastes} the code from the included
        file into the includer file.
        
        Because of this, if multiple files are \texttt{\#include "Rectangle.hpp"}-ing
        (or any other header), the same code in that header gets \textit{copied
        multiple times,} making the compiler think you've declared the same
        class over and over.
        
        \texttt{\#ifndef}, \texttt{\#define}, and \texttt{\#endif} are
        preprocessor commands used by the compiler. We are essentially saying ~\\
        ``if-not-defined \_RECTANGLE\_HPP, define \_RECTANGLE\_HPP, ... ... end-if.'',
        preventing the compiler from copying the same file twice.
        \end{hint}
        
        \newpage
        ~\\ Class function definitions go in Rectangle.cpp:
    
\begin{lstlisting}[style=code]
#include "Rectangle.hpp"

void Rectangle::SetPosition( int newX, int newY )
{
    x = newX;
    y = newY;
}

void Rectangle::SetDimensions( int newWidth, 
                            int newHeight )
{
    width = newWidth;
    height = newHeight;
}
\end{lstlisting}

        In our source file (.cpp), we need to make sure to include
        the header that goes with our class so it knows about
        the class declaration. Then, we can \textbf{define} the
        member functions here.
        
        Note that when we're defining the functions outside of the
        class declaration, we \underline{must} prefix the function
        name with the class name, followed by the \textbf{scope resolution operator ::}...
        
        \begin{center}
        \texttt{void Rectangle::SetPosition( int newX, int newY ) \{}
        \end{center}

        This is the standard way C++ files are organized - for each
        class we create, we create a \textbf{header} and a \textbf{source}
        file for it. 
        
        \subsubsection{Defining member functions \textit{inside} the class declaration}
        
        It \textit{is} completely possible to define our member functions
        \textit{inside} the class declaration, getting rid of the need 
        for the .cpp file. However, this does end up being treated differently
        by the compiler - writing our class this way makes the functions
        \textbf{inlined}... Basically, instead of having a separate
        function that the compiler will mark as ``call this'', it will
        copy the contents of the inlined function \textit{to the function call},
        basically replacing the call with the contents of the function.
        
        I haven't written much about how the compiler works since that's
        a more specialized topic and we don't need to worry about it at
        this stage of learning C++. This is just here for your own info.
        
        \newpage
        ~\\ The entire class in Rectangle.hpp:
\begin{lstlisting}[style=code]
#ifndef _RECTANGLE_HPP
#define _RECTANGLE_HPP

class Rectangle
{
    public:
    void SetPosition( int newX, int newY )
    {
        x = newX;
        y = newY;
    }

    void SetDimensions( int newWidth, int newHeight )
    {
        width = newWidth;
        height = newHeight;
    }

    private:
        int x, int y, int width, int height;
};

#endif
\end{lstlisting}

        This is also closer to how Java and C\# files look, defining
        all their methods \textit{within} the class declaration.
        It also does simplify having to go back and edit functions,
        not having to keep track of two different places where we have
        the function \textit{declaration} and the function \textit{definition}.
        
        \subsubsection{Should you define functions in the .hpp or the .cpp?}
        In general, for my courses, you can choose either approach.
        Starter code I provide will have the .hpp and .cpp files for classes,
        but when writing your own class you can throw it all in the .hpp
        file if you'd like.
        
        The main thing is that it's important to be aware of the standard way
        so that when you're working with other people you understand
        how things are structured.

        \subsection{Function vs. Method}
        
        A \textbf{method} is a word that means ``member function''.
        In Java and C\#, the term method is used instead of function,
        since you physically cannot have functions defined \textit{outside}
        of a class in those languages.
        
        I tend to stick with the term ``member function'', but if I
        write ``method'', it means that, whereas a ``function'' would
        be a standalone function elseware in the program.
    
        
        \subsection{Getters and Setters}
        
        In Object Oriented Programming, we generally want to hide the
        inner-workings of a class from the outside world (other functions
        and other classes). If everything were exposed, then other parts
        of the program could make changes to our class and it could be
        difficult to track down all those modifying locations (this,
        in particular, is a nightmare at a job with a large codebase).
        
        We generally write all our member variables as \textbf{private} -
        accessible \textit{only} to the class' member functions themseves -
        and use \textbf{public} member functions to interface with those
        variables as needed. ~\\
        
        For example, in our Rectangle class, we might have a function
        to set up the entire Rectangle all at once...
        
\begin{lstlisting}[style=code]
void Rectangle::Setup( int newX, int newY, int newWidth, int newHeight )
{
    x = newX;
    y = newY;
    width = newWidth;
    height = newHeight;
}
\end{lstlisting}

        ... or just a couple things at a time ...

\begin{lstlisting}[style=code]
void Rectangle::SetPosition( int newX, int newY )
{
    x = newX;
    y = newY;
}
\end{lstlisting}

        \newpage
        \paragraph{Setters:} Or, we might want to just set one member variable's value.
        In this case, we write a member function called a \textbf{setter}
        (aka \textbf{mutator}) that is responsible for setting a member
        variable's value:
        
\begin{lstlisting}[style=code]
void Rectangle::SetX( int newX )
{
    x = newX;
}
\end{lstlisting}

        The good thing about having setters instead of directly
        updating the \texttt{x} variable is that we can add error
        checks in our setter. Say, perhaps, that we don't want \texttt{x}
        to be negative, so we validate the input before changing it...

\begin{lstlisting}[style=code]
void Rectangle::SetX( int newX )
{
    if ( newX >= 0 )
    {
        x = newX;
    }
}
\end{lstlisting}

        If somewhere else in the program tries to set a negative value
        for the \texttt{x} variable, it will just ignore that command.
        (We could also throw an exception or display an error message
        or set a default value - it's up to your design.) ~\\
        
        \paragraph{Getters:} Likewise, sometimes we want to access
        those member variables to see what values they store. In this
        case, we write \textbf{Getter} member functions that are 
        responsible for returning a copy of the data (or, in some cases,
        a reference to it - but that's a design decision).

\begin{lstlisting}[style=code]
int Rectangle::GetX()
{
    return x;
}
\end{lstlisting}

        Getters are also handy for \textbf{formatting output}
        prior to returning it, or doing some other operation
        before returning something. Let's say we have a member function
        for a \texttt{Student} class, and a \texttt{GetName} function
        is going to combine their names and return it together:

\begin{lstlisting}[style=code]
int Student::GetFullName()
{
    return lastName + ", " + firstName;
}
\end{lstlisting}

        \newpage
        To summarize...
        
        \begin{itemize}
            \item	\textbf{Setters:} Sets the value of a member variable.
                    Generally, return type is \texttt{void} and there is
                    one parameter for the new value. ~\\
                    \texttt{void SetThing( int newValue );}
            
            \item	\textbf{Getters:} Returns the value of a member variable.
                    Generally, return type matches that variable,
                    and there are no parameters. ~\\
                    \texttt{int GetThing(); }
        \end{itemize}

        
        \subsection{Constructors and Destructors}
        
        Constructors and Destructors are special types of member functions
        in a class.
        
        \begin{itemize}
            \item	\textbf{Constructor:}
            \begin{itemize}
                \item	Runs automatically as soon as a new object is declared.
                \item	Can declare more than one constructor.
                \item	No return type
                \item	Constructor name \underline{must} match name of the class.
            \end{itemize}
            
            \item 	\textbf{Destructor:}
            \begin{itemize}
                \item	Runs automatically as soon as a new object is destroyed.
                \item	Can only have one destructor.
                \item	No return type
                \item	Destructor name \underline{must} match name of the class, prefixed with a tilde \texttt{\~}.
            \end{itemize}
        \end{itemize}
        
        \paragraph{Example: Text file wrapper}
        Let's say we're writing a class that works with a text file.
        
        When the object is \textbf{created}, the constructor will open the file
        and get it ready to write to.			
        With various member functions we can write to that text file
        as the program is running.			
        Then, when the program ends and the object is \textbf{destroyed}, it
        will automatically close the text file for us.
        
        \newpage
        
        ~\\ Class declaration:
\begin{lstlisting}[style=code]
class Logger
{
    public:
    // constructors
    Logger( string filename );
    Logger();

    // destructor
    ~Logger();

    // other method
    void Write( string text );

    private:
    ofstream m_output;
};
\end{lstlisting}

    We have \textbf{two constructors:} one where we pass in a filename
    for a file to open, and one where we don't. In the second case,
    we can use a default file name.
    
    \begin{hint}{Why ``m\_'' with the variable?}
    In some places, it is standard to prefix private member variables
    with an underscore (\texttt{\_output}) or m\_ (\texttt{m\_output}).
    I tend to use the latter when writing private member variables of a class.
    The ``m'' stands for ``member''.
    \end{hint}
    
    ~\\ The constructor definitions could look like this:
\begin{lstlisting}[style=code]
Logger::Logger( string filename )
{
    m_output.open( filename );
}

Logger::Logger()
{
    m_output.open( "log.txt" );
}
\end{lstlisting}

    \newpage
    The \texttt{Write} function could be used to write information
    to that text file, and could be implemented like this:
\begin{lstlisting}[style=code]
void Logger::Write( string text )
{
    m_output << text << endl;
}
\end{lstlisting}

    And then the destructor would be used to close up the file at the end:
\begin{lstlisting}[style=code]
Logger::~Logger()
{
    m_output.close();
}
\end{lstlisting}

    Now, we don't have to manually deal with file operations since
    this class \textbf{wraps} that functionality and deals with it for us.
    ~\\
    
    To use the program, we just declare the object variable:
\begin{lstlisting}[style=code]
#include "Logger.hpp"

int main()
{
    // At this point, 
    // a constructor is called automatically
    Logger log( "logfile.txt" );

    // Writing to the text file
    log.Write( "Hello!" );
    log.Write( "How are you?" );

    // Program ends here. The destructor
    // is called automatically when the log variable
    // goes out of scope and is destroyed.
    return 0;
}
\end{lstlisting}


    
    It can often be handy to overload our constructors so that
    we can initialize our objects in several different ways.
    
    
    \newpage
    \subsection{Default Constructors}
    A default constructor is a constructor with no parameters in it.
    This will be called when a variable of this class type is declared
    with no constructor explicitly called.
    
    Usually, a default constructor would be used to initialize variables
    to default values, and if your class contains pointers, this constructor
    should initialize those pointers to point to \texttt{nullptr}.
            
\begin{lstlisting}[style=code]
class MyClass
{
public:
MyClass()				// Default constructor
{
    m_value = 0;
}

private:
    int m_value;
};
\end{lstlisting}
    ~\\

    When we create a new object of this type like this, the default constructor will be called:
    
\begin{lstlisting}[style=code]
MyClass classVar;
\end{lstlisting}

    \newpage
    \subsection{Parameterized Constructors}
    Parameterized Constructors take in one or more parameters to help
    initialize the member variables of the object.
    We can have 0, one, or multiple parameterized constructors for our class.
            
\begin{lstlisting}[style=code]
class MyClass
{
    public:
    MyClass() {				// Default constructor
        m_value = 0;
    }

    MyClass( int value ) {	// Parameterized constructor
        m_value = value;
    }

    // etc
};
\end{lstlisting}

    ~\\
    
    When we instantiate our object, we can pass in argument(s)
    in order to call the parameterized version of the constructor:

\begin{lstlisting}[style=code]
MyClass classVar( 100 );
\end{lstlisting}

    ~\\
    
    If you declare a parameterized constructor but \textit{not} a 
    default constructor, then the C++ compiler will assume that you
    don't want an object to be declared with the default constructor.
    This means, you would only be able to declare the variable \textit{with}
    an explicit call to the constructor and with arguments.
    
    If this isn't the desired design, make sure to include a default
    constructor, even if you just leave it blank.
    
    
    \newpage
    \subsection{Copy Constructors}
    A copy constructor takes in another object of the same type as its
    parameter and uses this to \textit{copy over} members from the parameter
    to the new object.
            
\begin{lstlisting}[style=code]
class MyClass
{
    public:
    MyClass()				// Default constructor
    {
        m_value = 0;
    }

    MyClass( int value )	// Parameterized constructor
    {
        m_value = value;
    }

    MyClass( const MyClass& other )	// Copy constructor
    {
        m_value = other.m_value;
    }

    private:
    int m_value;
};
\end{lstlisting}
    ~\\
    
    In this case, we might already have an object of this type available,
    and when we are creating a new object, we want to make a \textit{copy}
    of the old object:

\begin{lstlisting}[style=code]
MyClass classVar( 100 );
MyClass anotherOne( classVar );		// Copy
\end{lstlisting}

    \paragraph{Design:}
    When a class has multiple member variables, it is up to us to
    decide which variables get copied over during this operation.
    There could be some design cases where all information is copied
    over except certain fields (maybe a unique customerID or a name).
    
    \paragraph{Default copy constructor:}
    If you don't explicitly declare a copy constructor, the C++ compiler
    will provide one for the class behind-the-scenes. This implicit
    copy constructor will only do shallow copies.

    \paragraph{Ways to copy:}
    
    \begin{itemize}
        \item	A \textbf{Shallow Copy} is where \textbf{values} of
                variables are copied over. This is generally fine for
                any sort of non-pointer-based variables. If the class
                contains a pointer that is pointing to some address,
                the shallow-copy of the pointer will point to the same address.
                
        \item	A \textbf{Deep Copy} is where values are copied over like
                with a shallow copy, but also will allocate new memory for
                a dynamic array (if the class has one) in order to copy over
                values of the element of the array to the new class copy.
    \end{itemize}
    
\begin{figure}[h]
\centering
\begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics{ObjectOrientedProgramming/images/shallow-copy.png}
    
    
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
    \textbf{Example of a shallow copy:} ~\\
    With the implicit copy constructor, any pointers in the copied version
    will be pointing to the same address as in the original. If the class
    contains a dynamic array, both the copy and the original will end
    up pointing to the same address of the array in memory.
\end{subfigure}
\end{figure}
    
    
\begin{figure}[h]
\centering
\begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics{ObjectOrientedProgramming/images/deep-copy.png}
    
    
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
    \textbf{Example of a deep copy:} ~\\
    A new block of memory has been allocated for the \texttt{int * numArr}.
    The values from InstanceA's \texttt{numArr} would be copied
    to InstanceB's \texttt{numArr} via a for loop during construction.
\end{subfigure}
\end{figure}
    







    \newpage
    \subsection{Example program: House Builder}
    
    This program allows a user to enter information about
    multiple rooms in a house and at the end it will
    output a text file with all the room information.
    
    ~\\ Example output:
    
\begin{lstlisting}[style=output]
HOUSE BUILDER
(A)dd new room or (Q)uit: a
Enter room name: livingroom
Enter width: 10
Enter length: 20

(A)dd new room or (Q)uit: a
Enter room name: kitchen
Enter width: 10
Enter length: 15

(A)dd new room or (Q)uit: a
Enter room name: bathroom
Enter width: 6
Enter length: 10

(A)dd new room or (Q)uit: q

Outputted to house.txt
\end{lstlisting}

In my design, I have two objects: A \textbf{Room}, which contains
a room name, width, and height, and a \textbf{House}, which contains
an array of rooms. In the main() function, we work directly with the
House, and the house takes care of dealing with the Rooms.

\newpage
\paragraph{Room.hpp:} ~\\

\begin{lstlisting}[style=code]
#ifndef _ROOM_HPP
#define _ROOM_HPP

#include <string>
using namespace std;

class Room
{
    public:
    void Setup();

    string GetName();
    int GetWidth();
    int GetLength();
    int GetArea();

    private:
    string name;
    int width;
    int length;
};

#endif
\end{lstlisting}

We have getter functions for the name, width, and lenght of the room,
as well as to get the area. We don't need to store the area because
we can calculate it any time we need it.

There is also a function Setup() that deals with the cins and couts
to set up the room's information.
        
\newpage
\paragraph{Room.cpp:} ~\\

\begin{lstlisting}[style=code]
#include "Room.hpp"

#include <iostream>
using namespace std;

void Room::Setup()
{
    cout << "Enter room name: ";
    cin >> name;

    cout << "Enter width: ";
    cin >> width;

    cout << "Enter length: ";
    cin >> length;
}

string Room::GetName()
{
    return name;
}

int Room::GetWidth()
{
    return width;
}

int Room::GetLength()
{
    return length;
}

int Room::GetArea()
{
    return width * length;
}
\end{lstlisting}

\newpage
\paragraph{House.hpp:} ~\\

\begin{lstlisting}[style=code]
#ifndef _HOUSE_HPP
#define _HOUSE_HPP

#include "Room.hpp"

const int MAX_ROOMS = 10;

class House
{
    public:
    House();
    ~House();

    void AddRoom();

    private:
    Room rooms[MAX_ROOMS];
    int roomCount;
};

#endif
\end{lstlisting}

The House class contains a \textbf{constructor}, which will be used to
initialize data automatically. In particular, we want \texttt{roomCount}
to be set to 0.

The \textbf{destructor} will automatically open a text file,
output all the rooms' information, and then close the text file
when the House object is destroyed at the end of the program.

\newpage
\paragraph{House.cpp:} ~\\

\begin{lstlisting}[style=code]
#include "House.hpp"

#include <iostream>
#include <fstream>
using namespace std;

House::House()
{
    roomCount = 0;
}

House::~House()
{
    ofstream output( "house.txt" );
    for ( int i = 0; i < roomCount; i++ )
    {
        output << rooms[i].GetName() << "\t"
                << rooms[i].GetWidth() << "x"
                << rooms[i].GetLength() << "\t"
                << "(" << rooms[i].GetArea() << " sqft)" 
                << endl;
    }
    output.close();

    cout << "Outputted to house.txt" << endl;
}

void House::AddRoom()
{
    if ( roomCount == MAX_ROOMS )
    {
        cout << "House is full!" << endl
             << "Cannot fit any more rooms!" << endl;
        return; // exit this function
    }

    rooms[ roomCount ].Setup();
    roomCount++;
}
\end{lstlisting}

\newpage
\paragraph{main.cpp:} ~\\

\begin{lstlisting}[style=code]
#include <iostream>
using namespace std;

#include "House.hpp"

int main()
{
    House myHouse;

    cout << "HOUSE BUILDER" << endl;
    bool done = false;
    while ( !done )
    {
        cout << "(A)dd new room or (Q)uit: ";
        char choice;
        cin >> choice;

        if ( toupper(choice) == 'A' )
        {
            myHouse.AddRoom();
        }
        else if ( toupper(choice) == 'Q' )
        {
            done = true;
        }
        cout << endl;
    }

    return 0;
}
\end{lstlisting}

This program only deals with one house, but we \textit{could}
have an array of Houses, each with their own separate array of Rooms.


