

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=10cm]{images/arrays.png}
    \end{center}
\end{figure}


    \section{Array basics} %--------------------------------------------% 
    \subsection{What are arrays?}
    In C++, arrays are a built-in way to store a series of similar data
    all under one name.~\\
    
    Before now, if we wanted to store a list of students
    in a class (or similar kind of data), we would have to declare a bunch of
    separate variables and write the same code over and over to manage it:
    
    \newpage
\begin{lstlisting}[style=code]
string student1, student2, student3;

cout << "Enter student 1 name: ";
cin >> student1;

cout << "Enter student 2 name: ";
cin >> student2;

cout << "Enter student 3 name: ";
cin >> student3;
\end{lstlisting}

	This would quickly become unmanagable if you were writing a program
	with tens, hundreds, or thousands of students stored in it.
	Instead, we can make use of \textbf{arrays} to store a series of
	related data together.

\begin{lstlisting}[style=code]
string students[100];
for ( int i = 0; i < 100; i++ )
{
	cout << "Enter student " << (i+1) << " name: ";
	cin >> student[i];
}
\end{lstlisting}

	Arrays allow us to operate on the same name (e.g., \texttt{student}),
	but addressing different \textbf{elements} of the array with an \textbf{index} number.
	This way, we can write code to act on the data \textit{once}, just modifying
	that index to work with different pieces of data.
	
	\begin{center}
		Example: student array of size 4: ~\\~\\
		\begin{tabular}{| l | c | c | c | c |} \hline
			\textbf{Element} 	
			& \texttt{"Rai"}
			& \texttt{"Anuj"}
			& \texttt{"Rebekah"}
			& \texttt{"Rose"}
			\\ \hline
			\textbf{Index} 		& 0 & 1 & 2 & 3
			\\ \hline
		\end{tabular}
	\end{center}
	
	Each item in the array has a corresponding \textbf{index} marking its
	position in the list, with 0 being the first value. If an array is
	of size $n$, then the valid indices are 0 through $n-1$.
	
	An \textbf{element} is the information stored at that index position,
	which is essentially a single variable in an array of variables.
	~\\
	
	\paragraph{Declaring arrays:}
	In C++, we declare an array similarly to how we declare a variable,
	except that we need to specify an array \textbf{size} during declaration:
	
\begin{lstlisting}[style=code]
// An array of size 100
string students[100];
\end{lstlisting}

	Or, if we already have data to put into it, we can initialize it with an
	\textbf{initializer list}. Then the array will be sized at however many
	items you give it.
	
\begin{lstlisting}[style=code]
// An array of size 4
string students[] = {"Rai", "Anuj", "Rebekah", "Rose"};
\end{lstlisting}

	\paragraph{\underline{Array size} named constant:} Array sizes must be defined
	during its declaration and the array \textbf{cannot} be resized
	during the program's execution. Because the array size can't change,
	and because we may need to know the size of the array throughout the program,
	we will generally use a \textbf{named constant} to store the size of the array.
	
\begin{lstlisting}[style=code]
const int MAX_STUDENTS = 100;
string students[MAX_STUDENTS];
\end{lstlisting}


	\paragraph{\underline{Element count} variable:}
	Because an array's size cannot be changed after its declaration,
	it often becomes necessary to \textit{overshoot} the amount of
	spaces we need in the array so that we always have enough room for our data.
	Perhaps a school's classrooms range from 20 to 70 seats, so we would
	want to declare the array of the biggest size so that we don't run out
	of space. Because of this, not all spaces in the array may be taken
	up at any given time.
	
	C++ doesn't have a function to directly get the amount of elements in
	an array, so generally when declaring an array we need to also have
	an associated variable to track how many items we've stored in the array.

\begin{lstlisting}[style=code]
const int MAX_STUDENTS = 100;	// total array size
int studentCount = 0;			// how many elements
string students[MAX_STUDENTS];	// array declaration

students[0] = "Rai";	// setting up first student
studentCount++;			// adding 1 to student count
\end{lstlisting}
	
	\paragraph{Array data types:}
	Arrays can be declared with any data type. To the computer, we are just
	declaring $n$ amount of some variable, and it takes care of putting
	all the $n$ variables back-to-back in memory.
	
	\paragraph{Valid indices:}
	As stated earlier, if an array is of size $n$, then its valid indices
	are 0 to $n-1$. In most computer languages, arrays and lists start at
	index 0 and go up from there, meaning to count for items we have ``0, 1, 2, 3''.
	
	\begin{hint}{Common issue}
	A common source of program crashes is \textbf{going outside the bounds of an array}!
	This happens if you try to access an invalid index, such as \texttt{student[-1]}
	or \texttt{student[100]} (for an array of size 100; valid indices are 0-99).
	\end{hint}
	
    \newpage
    \section{Accessing indices with variables} %------------------------%
    Since the array \textbf{index} is always an integer, we could use a
    variable to determine which item in an array to modify - such as 
    asking the user which item they want to edit.
    
\begin{lstlisting}[style=code]
const int TOTAL_ITEMS = 10;
float prices[TOTAL_ITEMS];

cout << "Edit which item? (0-9): ";
int itemIndex;
cin >> itemIndex;

cout << "Enter price for item: ";
cin >> prices[ itemIndex ];
\end{lstlisting}
    
    \section{Using for loops with arrays} %-----------------------------%  
    
    Using for loops is the most common way to iterate over all the data in 
    an array, setting data or accessing data. Have an array of size $n$?
    We want to iterate from \texttt{i} = 0 to $n-1$, going up by 1 each time.
    
    \paragraph{Initializing an array:} ~\\
    When we declare a variable without initializing it, it will be full of
    garbage data. This is the same for arrays, and sometimes you want to
    clean up an array prior to using it.
   
\begin{lstlisting}[style=code]
const int TOTAL_ITEMS = 10;
float prices[TOTAL_ITEMS];

// Initialize all prices to 0.
for ( int i = 0; i < TOTAL_ITEMS; i++ )
{
	prices[i] = 0;
}
\end{lstlisting}
	
	\newpage
	\paragraph{Asking the user to enter all elements:} ~\\
	We can iterate over all the items in an array and have the
	user enter information for each element by using a loop.
	Since the first index is 0, we sometimes just add +1 to the index
	for the user's benefit, since people generally aren't used to lists
	starting at 0.
	
\begin{lstlisting}[style=code]
const int TOTAL_ITEMS = 10;
float prices[TOTAL_ITEMS];

// Initialize all prices to 0.
for ( int i = 0; i < TOTAL_ITEMS; i++ )
{
	cout << "Enter price for item " << (i+1) << ": ";
	cin >> prices[i];
}
\end{lstlisting}

	\paragraph{Displaying all items in an array:} ~\\
	We can display all the elements of an array by using a loop as well,
	though we usually \textit{don't} want to show \textit{all elements}
	of the array - usually just the items we know we're storing data in.
	Recall that we usually will have an extra integer variable to count
	how many items have actually been stored in the array, which is different
	from the total array size.
	
\begin{lstlisting}[style=code]
const int MAX_STUDENTS = 100;
int studentCount = 0;
string students[MAX_STUDENTS];

// ...Let's say 20 students were added here...

// Iterate from index 0 to 19, since we have stored
// 20 students so far.
for ( int i = 0; i < studentCount; i++ )
{
	cout << "Student " << (i+1) 
		<< " is " << students[i] << endl;
}
\end{lstlisting}

	You can also use a \textbf{range-based for loop} in versions of C++
	from 2011 or later:
	
\begin{lstlisting}[style=code]
for ( auto & student : students )
{
	cout << student << endl;
}
\end{lstlisting}
	
	\newpage
	\section{Using multiple arrays}
	Let's say we're writing a simple restaurant program where we need
	a list of dishes \textit{and} their prices together. Later on,
	we will write our own data type using \textbf{structs and classes}
	to keep these items together. But for now, we would implement
	this relationship by keeping track of two separate arrays with the data we need.

\begin{lstlisting}[style=code]
const int MAX_DISHES = 20;
int dishCount = 0;

// Information about a dish
string  dishNames[MAX_DISHES];
float   dishPrices[MAX_DISHES];
bool	dishVegetarian[MAX_DISHES];

// ... Let's say we created some dishes here...

// Display the menu
cout << "What would you like to order?" << endl;
for ( int i = 0; i < dishCount; i++ )
{
	cout << i << ". " 
		<< dishNames[i]  << " (" 
		<< dishPrices[i] << " dollars) ";
	
	if ( dishVegetarian[i] )	cout << "Veg";
	
	cout << endl;
}

// Get the index of dish they want
int whichDish;
cout << "Selection: ";
cin >> whichDish;
\end{lstlisting}
    
    \section{Arrays as arguments} %-------------------------------------%  
    Because arrays let us declare a bunch of variables at once
    they could potentially take up a lot of memory. Because of this,
    passing an array as a pass-by-value parameter would be inefficient -
    remember that pass-by-value means that the parameter is \textit{copied}
    from the caller argument.
    
    C++ automatically passes arrays around as \textbf{pass-by-reference} instead.
    You don't have to inclue the \& symbol in your parameter list for an array,
    it just happens! But - keep in mind that any problems with pass-by-reference
    also apply to arrays. If you want to pass an array to a function but
    you \textbf{don't want the data changed}, then you would need to
    mark that array parameter as \textbf{const}.
    
\begin{lstlisting}[style=code]
void DisplayAllItems( const string arr[], int size )
{
	for ( int i = 0; i < size; i++ )
	{
		cout << arr[i] << endl;
	}
}
\end{lstlisting}

	When using an array as a parameter, you don't have to hard-code
	a size to it. You can leave the square brackets empty (but you DO
	need square brackets to show that it's an array!) and then pass any
	array (with matching data type) to the function. However, you will
	also want to have an int parameter to pass the size of the array
	as well.

	\newpage
	\section{Array management}

	 Since arrays require quite a bit of management to work with, you
	 could implement some basic functions to do this management, instead
	 of having to re-write the same code over and over. For example...
	 
	 \paragraph{Clear array:} ~\\
	 Sets all elements of this string array to an empty string and
	 resets the \texttt{elementCount} to 0 afterwards.

\begin{lstlisting}[style=code]
void Clear( string arr[], int & elementCount )
{
	for ( int i = 0; i < elementCount; i++ )
	{
		arr[i] = "";
	}
	elementCount = 0;
}
\end{lstlisting}

	 \paragraph{Display all elements:} ~\\
	 Shows all the elements of an array.

\begin{lstlisting}[style=code]
void Display( const string arr[], int elementCount )
{
	for ( int i = 0; i < elementCount; i++ )
	{
		cout << i << "\t" << arr[i] << endl;
	}
}
\end{lstlisting}

	\paragraph{Add new element to array:} ~\\
	Often in our programs we will only set up one item at a time
	(perhaps when selected from a menu). This means we need to
	get the data for the array and figure out where in the array it will go.
	
\begin{lstlisting}[style=code]
void AddItem( string arr[], int & elementCount )
{
	cout << "Enter new element: ";
	cin >> arr[ elementCount ];
	elementCount++;
}
\end{lstlisting}

	For an array, \texttt{elementCount} starts at 0 when the array is empty.
	This also happens to be the \textbf{index} where we will insert our first element - at 0.
	
	\begin{center}
		\begin{tabular}{| l | c | c | c | c |} \hline
			\textbf{Element}	& \cellcolor{blue!25} & & &
			\\ \hline
			\textbf{Index}	& \cellcolor{blue!25}0 & 1 & 2 & 3 
			\\ \hline
		\end{tabular}
	\end{center}
	
	Once we add our first element, our \texttt{elementCount} will be 1,
	and the next index to insert data at will also be 1.
	
	\begin{center}
		\begin{tabular}{| l | c | c | c | c |} \hline
			\textbf{Element}	& \texttt{"Cats"}  & \cellcolor{blue!25} & &
			\\ \hline
			\textbf{Index}	& 0 & \cellcolor{blue!25}1 & 2 & 3 
			\\ \hline
		\end{tabular}
	\end{center}
	
	Then, \texttt{elementCount} will be 2, and the next index to insert at will be 2.
	
	\begin{center}
		\begin{tabular}{| l | c | c | c | c |} \hline
			\textbf{Element}	& \texttt{"Cats"}  & \texttt{"Dogs"} & \cellcolor{blue!25} &
			\\ \hline
			\textbf{Index}	& 0 & 1 & \cellcolor{blue!25}2 & 3 
			\\ \hline
		\end{tabular}
	\end{center}
	
	Because of this, the \texttt{elementCount} variable both tells us
	how many items have been stored in the array \textit{and}
	what is the next index to store new information at.
	
	\newpage
	\paragraph{Deleting an element from an array} ~\\
	Generally, when using an array to store data, we want to avoid gaps
	in the array, like this:
	
	\begin{center}
		\begin{tabular}{| l | c | c | c | c | c | c |} \hline
			\textbf{Element}	& \texttt{"A"}  & \texttt{"B"} & \cellcolor{gray!25}\texttt{""} & \texttt{"C"} & \cellcolor{gray!25}\texttt{""} & \texttt{"D"}
			\\ \hline
			\textbf{Index}	& 0 & 1 & \cellcolor{gray!25}2 & 3 & \cellcolor{gray!25}4 & 5
			\\ \hline
		\end{tabular}
	\end{center}
	
	Because of these gaps, it makes it more difficult to add items to the list
	(have to search the array for an empty spot before adding a new item - that's
	time consuming, process-wise). We usually design our data storage in arrays
	so that everything is contiguous, starting from 0, leaving all the empty space at
	the \textit{end} of the array:

	\begin{center}
		\begin{tabular}{| l | c | c | c | c | c | c |} \hline
			\textbf{Element}	& \texttt{"A"}  & \texttt{"B"} & \texttt{"C"} & \texttt{"D"} & \cellcolor{gray!25}\texttt{""} & \cellcolor{gray!25}\texttt{""}
			\\ \hline
			\textbf{Index}	& 0 & 1 & 2 & 3 & \cellcolor{gray!25}4 & \cellcolor{gray!25}5
			\\ \hline
		\end{tabular}
	\end{center}
		
	Let's say we have data stored in the array above, but want to delete \texttt{"B"}.
	We could set it to an empty string, but that would leave us with a gap:
		
	\begin{center}
		\begin{tabular}{| l | c | c | c | c | c | c |} \hline
			\textbf{Element}	& \texttt{"A"}  & \cellcolor{red!25}\texttt{""} & \texttt{"C"} & \texttt{"D"} & \cellcolor{gray!25}\texttt{""} & \cellcolor{gray!25}\texttt{""}
			\\ \hline
			\textbf{Index}	& 0 & \cellcolor{red!25}1 & 2 & 3 & \cellcolor{gray!25}4 & \cellcolor{gray!25}5
			\\ \hline
		\end{tabular}
	\end{center}
	
	We would want to do a \textbf{shift-left} operation
	to move \texttt{"C"} to 1 and \texttt{"D"} to 2, giving us:
		
	\begin{center}
		\begin{tabular}{| l | c | c | c | c | c | c |} \hline
			\textbf{Element}	& \texttt{"A"}  & \texttt{"C"} & \texttt{"D"} & \cellcolor{gray!25}\texttt{""} & \cellcolor{gray!25}\texttt{""} & \cellcolor{gray!25}\texttt{""}
			\\ \hline
			\textbf{Index}	& 0 & 1 & 2 & \cellcolor{gray!25}3 & \cellcolor{gray!25}4 & \cellcolor{gray!25}5
			\\ \hline
		\end{tabular}
	\end{center}
	
	In code, it would look like:
	
\begin{lstlisting}[style=code]
void DeleteItem( int deleteIndex, 
	string arr[], int & elementCount )
{
  // Shift left
  for ( int i = deleteIndex; i < elementCount-1; i++ )
  {
	arr[i] = arr[i+1];
  }
  
  elementCount--;
}
\end{lstlisting}
		
		
	\newpage
	\paragraph{Inserting an element into an array} ~\\
	Sometimes we want to insert some data in between other items, such
	as if we wanted to maintain a sorted order or were ranking the data
	in the array.
	
	~\\
	Let's say we want to insert \texttt{"C"} before the \texttt{"D"} in this array:
	
	\begin{center}
		\begin{tabular}{| l | c | c | c | c | c | c |} \hline
			\textbf{Element}	& \texttt{"A"}  & \texttt{"B"} & \cellcolor{yellow!25}\texttt{"D"} & \texttt{"E"} & \texttt{""} & \texttt{""}
			\\ \hline
			\textbf{Index}	& 0 & 1 & \cellcolor{yellow!25}2 & 3 & 4 & 5
			\\ \hline
		\end{tabular}
	\end{center}
	
	This would be at index 2, but if we just put the \texttt{"C"} at index 2,
	then we would overwrite \texttt{"D"} - we don't want to lose data!
	
	What we're going to have to do is move 2 to 3, but we don't want to overwrite
	\texttt{"E"} either, so we move 3 to 4. But the order we do this matters - 
	we need to start at the \textit{end} of the list and shift everything until 2 right by one,
	to get this array:
	
	\begin{center}
		\begin{tabular}{| l | c | c | c | c | c | c |} \hline
			\textbf{Element}	& \texttt{"A"}  & \texttt{"B"} & \cellcolor{yellow!25}\texttt{""} & \texttt{"D"} & \texttt{"E"} & \texttt{""}
			\\ \hline
			\textbf{Index}	& 0 & 1 & \cellcolor{yellow!25}2 & 3 & 4 & 5
			\\ \hline
		\end{tabular}
	\end{center}
	
	Then we could add our new data in the position we want.
	
	~\\ In code, it would look like this:

\begin{lstlisting}[style=code]
void InsertItem( string newData, int insertIndex, 
	string arr[], int & elementCount )
{
  // Shift right
  for ( int i = elementCount-1; i >= insertIndex; i-- )
  {
	arr[i] = arr[i-1];
  }
  
  // Insert new item
  arr[ insertIndex ] = newData;
  
  elementCount++;
}
\end{lstlisting}

    \section{Multidimensional arrays} %---------------------------------%  
    We can also declare multidimensional arrays, such as 2D or 3D arrays.
    As with a 1D array, there is just one data type that all the elements
    share.
    
\begin{lstlisting}[style=code]
string spreadsheet[32][32];	// rows and columns
int vertices[4][4][4];		// x, y, z
\end{lstlisting}

	Let's say we wanted to turn a day planner into a program. Perhaps
	we originally stored the day plan like this:
	
	\begin{center}
		\begin{tabular}{l l l}
			\textbf{Day}	& \textbf{Time} 	& \textbf{Task} \\ \hline
			Monday			& 8:00				& Work meeting with Bob \\ \hline
			Monday			& 13:00				& Project review \\ \hline
			Tuesday			& 10:00				& Customer meeting \\ \hline
			Wednesday		& 14:00				& Retrospective \\ \hline
		\end{tabular}
	\end{center}
	
	We can convert this into a 2D array of strings (the ``task''),
	with one dimension being for ``day of the week'' and the other
	dimension being for ``hour of the day''...
    
\begin{lstlisting}[style=code]
int DAYS_OF_WEEK = 7;
int HOURS_IN_DAY = 24;

string todo[ DAYS_OF_WEEK ][ HOURS_IN_DAY ];
\end{lstlisting}

	We could ask the user what day they want to set a task for,
	and if they type ``Sunday'' that could translate to 0, and ``Saturday''
	could translate to 6 (or however you want to organize your week)...
    
	\begin{center}
		\begin{tabular}{c c c c c c c}
			Sunday & Monday & Tuesday & Wednesday & Thursday & Friday & Saturday \\
			0 & 1 & 2 & 3 & 4 & 5 & 6
		\end{tabular}
	\end{center}
	
	We could ask them next for what hour the task is at, and that can
	map to the second index: ``0'' for midnight, ``8'' for 8 am, ``13'' for 1 pm, and so on to 23.
    
    With this information, we can get the user's todo task and store it at
    the appropriate indices:
    
\begin{lstlisting}[style=code]
int day, hour;

cout << "Enter the day:" << endl;
cout << "0. Sunday   1. Monday 2. Tuesday 3. Wednesday "
	 << "4. Thursday 5. Friday 6. Saturday" << endl;
cout << "Day: ";
cin >> day;

cout << "Enter the hour (0 to 23): ";
cin >> hour;

cout << "Enter your task: ";
cin >> todo[ day ][ hour ];
\end{lstlisting}

	With the user's week planned out, you could then display it back
	to the user by using a \textbf{nested for loop}: One loop for the day,
	one loop for the hour.
    
\begin{lstlisting}[style=code]
cout << "YOUR SCHEDULE" << endl;

for ( int day = 0; day < DAYS_OF_WEEK; day++ )
{
	for ( int hour = 0; hour < HOURS_IN_DAY; hour++ )
	{
		cout << day << ", "
		     << hour << ": "
		     << todo[ day ][ hour ] << endl;
	}
}
\end{lstlisting}

	Though you'd probably want to do some extra formatting; such as
	determining that if the day is 0, then write ``Sunday'' instead of
	the number ``0''.
    
    
    
    
    
    
    \section{C++ Standard Template Library: Vectors} %------------------%  
    
    The C++ Standard Template Library contains a special structure
    called a \textbf{vector}. A vector is a \textbf{class} (something we'll learn
    about later) and it is implemented on top of a
    \textbf{dynamic array} (which we will learn about later with pointers).
    Basically, it's a resizable array and it has functionality to make
	managing data a bit more managable.
	
	Generally, in Computer Science curriculum, we teach you how to build
	\textbf{data structures} (structures that store data) like vectors,
	lists, and other items because it's important to know how they work
	(thus why we're covering arrays), but for your own projects and
	in the real world, you would probably use a vector over an array.
    
    \paragraph{Declaring a vector:} Vectors are a type of \textbf{templated}
    object, meaning it can store any data type - you just have to specify
    what kind of type the vector stores. The format of a vector declaration
    looks like this:
    
\begin{lstlisting}[style=code]
// Declaring vectors
vector<string>	students;
vector<float>	prices;
\end{lstlisting}

	\paragraph{Adding data:} You can add data to a vector by using
	its \textbf{push\_back} function, passing the data to add as the
	argument:
    
\begin{lstlisting}[style=code]
vector<string>	students;
students.push_back( "Rai" );
\end{lstlisting}

	\paragraph{Getting the size of the vector:} The \textbf{size} function
	will return the amount of elements currently stored in the vector.
    
\begin{lstlisting}[style=code]
cout << "There are " 
	<< students.size() << " students" << endl;
\end{lstlisting}

	\paragraph{Clearing the vector:} You can erase all the data in a
	vector with the \textbf{clear} function:
    
\begin{lstlisting}[style=code]
students.clear();
\end{lstlisting}

	\paragraph{Accessing elements by index:} Accessing an element
	at some index looks just like it does with an array:

\begin{lstlisting}[style=code]
cout << "Student: " << students[0] << endl;
\end{lstlisting}

	\paragraph{Iterating over a vector:} You can use a for loop to
	iterate over all the elements of a vector, similar to an array:
    
\begin{lstlisting}[style=code]
cout << "Students:" << endl;

for ( int i = 0; i < students.size(); i++ )
{
	cout << i << "\t" << students[i] << endl;
}
\end{lstlisting}

	You can also use C++11 style \textbf{range-based for loop}
	if you don't need the index. It allows you to iterate over
	all the elements of \texttt{students}, using an alias of
	\texttt{student} for each element.

\begin{lstlisting}[style=code]
for ( auto & student : students )
{
	cout << student << endl;
}
\end{lstlisting}
