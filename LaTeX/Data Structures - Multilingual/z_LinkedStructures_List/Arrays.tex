Before taking a Data Structures course, you've probably only
        managed data in your program via \textbf{arrays}.

        When we declare an array in C++, we must give it a size (whehter
        we're making a dynamic array or a vanilla array). This ensures
        that memory can be allocated so that each element of the array
        is side-by-side with other elements.

        \section{Investigating arrays and memory addresses:}
        Let's write a simple program to look at how arrays are stored
        in memory, in case you don't remember. ~\\

        First, how big is one element? We can use the \texttt{sizeof}
        function to find out how big, in bytes, a given data type is:
\begin{lstlisting}[style=code]
cout << "The size of a int is: "
     << sizeof( int ) << " bytes." << endl;
\end{lstlisting}

        \vspace{1cm}

\begin{lstlisting}[style=output]
The size of a int is: 4 bytes.
\end{lstlisting}

        \newpage
        One integer is 4 bytes. Next, we can declare an array of integers
        and look at how many bytes that takes up:

\begin{lstlisting}[style=code]
const int SIZE = 10;
int intArr[SIZE];
cout << "Size of a int array with "
     << SIZE << " elements: "
     << sizeof( intArr ) << " bytes." << endl;
\end{lstlisting}

        \vspace{1cm}

\begin{lstlisting}[style=output]
Size of a int array with 10 elements: 40 bytes.
\end{lstlisting}

        \vspace{2cm}

        Now, let's look at the memory addresses of each element:

\begin{lstlisting}[style=code]
cout << "Memory addresses for the int array:" << endl;
for ( int i = 0; i < SIZE; i++ )
{
    cout << "Element " << i << ": " << &(intArr[i]) << endl;
}
\end{lstlisting}

        \vspace{1cm}

\begin{lstlisting}[style=output]
Memory addresses for the int array:
Element 0: 0x7ffff9867d00
Element 1: 0x7ffff9867d04
Element 2: 0x7ffff9867d08
Element 3: 0x7ffff9867d0c
Element 4: 0x7ffff9867d10
Element 5: 0x7ffff9867d14
Element 6: 0x7ffff9867d18
Element 7: 0x7ffff9867d1c
Element 8: 0x7ffff9867d20
Element 9: 0x7ffff9867d24
\end{lstlisting}

        \newpage
        Each time this program runs, we will have different memory addresses
        for the \texttt{intArr}, but those addresses \textbf{will always be
        contiguous in memory, 4 bytes apart.}

        \begin{center}
            \begin{tikzpicture}
                % Rectangle
                \draw (0,0) -- (10,0) -- (10,1) -- (0,1) -- (0,0);
                % Splitters
                \draw (1,0) -- (1,1); \draw (2,0) -- (2,1); \draw (3,0) -- (3,1);
                \draw (4,0) -- (4,1); \draw (5,0) -- (5,1); \draw (6,0) -- (6,1);
                \draw (7,0) -- (7,1); \draw (8,0) -- (8,1); \draw (9,0) -- (9,1);
                % Indices
                \node at (0.5, 1.5) { 0 };
                \node at (1.5, 1.5) { 1 };
                \node at (2.5, 1.5) { 2 };
                \node at (3.5, 1.5) { 3 };
                \node at (4.5, 1.5) { 4 };
                \node at (5.5, 1.5) { 5 };
                \node at (6.5, 1.5) { 6 };
                \node at (7.5, 1.5) { 7 };
                \node at (8.5, 1.5) { 8 };
                \node at (9.5, 1.5) { 9 };
                % Memory addresses
                \node[rotate=75] at (0.3,-1.8) { \texttt{ 0x7ffff9867d00 } };
                \node[rotate=75] at (1.3,-1.8) { \texttt{ 0x7ffff9867d04 } };
                \node[rotate=75] at (2.3,-1.8) { \texttt{ 0x7ffff9867d08 } };
                \node[rotate=75] at (3.3,-1.8) { \texttt{ 0x7ffff9867d0c } };
                \node[rotate=75] at (4.3,-1.8) { \texttt{ 0x7ffff9867d10 } };
                \node[rotate=75] at (5.3,-1.8) { \texttt{ 0x7ffff9867d14 } };
                \node[rotate=75] at (6.3,-1.8) { \texttt{ 0x7ffff9867d18 } };
                \node[rotate=75] at (7.3,-1.8) { \texttt{ 0x7ffff9867d1c } };
                \node[rotate=75] at (8.3,-1.8) { \texttt{ 0x7ffff9867d20 } };
                \node[rotate=75] at (9.3,-1.8) { \texttt{ 0x7ffff9867d24 } };
            \end{tikzpicture}
        \end{center}

        \begin{hint}{Hexadecimal}
            When we write in hexadecimal, we want each number to only
            take up one character space. So, we have 0-9 to be 0 through 9,
            but then 10 is A, 11 is B, 12 is C, 13 is D, 14 is E, and 15 is F.
        \end{hint}

        \begin{hint}{Address of the beginning of the array}
            The address of \texttt{intArr}  is the same as the address of \texttt{intArr[0]}.
        \end{hint}

        \begin{center}
            \includegraphics[width=10cm]{../images/arrays.png}
        \end{center}

        \newpage
        \section{Pros and Cons of arrays for storing data:}

        \paragraph{Pros:}

        \begin{itemize}
            \item   \textbf{Random access is instant:} This means, we can
                    access an element at \textit{any arbitrary index} instantaneously.
                    How? Well, we have the address of the 0th element, and to get
                    an item at position $i$, we just have to add \texttt{i * sizeof(int)}.
                    A simple math operation is virtually instant. ~\\ ~\\
                    \texttt{AddressOf( i ) = AddressOf( 0 ) + i * sizeof( dataType )}
        \end{itemize}

        \paragraph{Cons:}

        \begin{itemize}
            \item   \textbf{Resizing a dynamic array is costly:} Any time our array
                    is full, to resize it we have to allocate memory for a \textit{new array}
                    of a bigger size. Then, we have to copy each element from the old
                    array to the new array. This takes time any time resize occurs.
            \item   \textbf{We have to over-estimate the array size:} We don't
                    want to have to resize the entire array every time a new item
                    is inserted, so we generally will resize it by some quantity
                    each time (not just adding 1 to the size of the array).
                    This means we're allocating more space than we're \textit{actually using}
                    if the array isn't full.
        \end{itemize}

        The study of Data Structures is all about the trade-offs between
        different data types: Some are faster to access data but slower to insert new data and vice versa.
        Instead of storing data in an \underline{array-based structure}, we can make
        a tradeoff of \textbf{less memory used, instant insertion of new data}
        in exchange for \textbf{slower access} using a \underline{linked structure}.

        \begin{tabular}{l l}
            \includegraphics{../images/cat-no.png} & \includegraphics[width=10cm]{../images/dynamicarray.png}
            \\
            \includegraphics{../images/cat-yes.png} & \includegraphics[width=10cm]{../images/linkedlist.png}
        \end{tabular}
