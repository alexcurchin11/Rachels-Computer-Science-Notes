
    \begin{center}
        \includegraphics[width=\textwidth]{images/arrays.png}
    \end{center}
  

With the dynamic array structure, most of it will be similar to the Fixed Array Structure,
except we now have to deal with memory management and pointers.

Instead of throwing exceptions if the array is full, with a dynamic array we can resize it instead.
However, we also now need to check for the array pointer (m\_array in this example) pointing to
\texttt{nullptr}.

\newpage

The class declaration could look like this:

\begin{lstlisting}[style=code]
class SmartDynamicArray
{
public:
    SmartDynamicArray();
    ~SmartDynamicArray();

    void PushBack( T newItem );
    void PushFront( T newItem );
    void PushAt( T newItem, int index );

    void PopBack();
    void PopFront();
    void PopAt( int index );

    T GetBack() const;
    T GetFront() const;
    T GetAt( int index ) const;

    int Search( T item ) const;

    void Display() const;
    int Size() const;
    bool IsEmpty() const;
    void Clear();

private:
    T* m_array;
    int m_arraySize;
    int m_itemCount;

    void ShiftLeft( int index );
    void ShiftRight( int index );

    void AllocateMemory( int size );
    void Resize( int newSize );

    bool IsFull() const;
};
\end{lstlisting}




\newpage
\subsection{Constructor - Preparing the array to be used}
With the constructor here, it is important to assign \texttt{nullptr} to
the pointer so that we don't try to dereference a garbage address.
The array size and item count variables should also be assigned to 0.

\begin{lstlisting}[style=code]
template <typename T>
SmartDynamicArray<T>::SmartDynamicArray()
{
  m_array = nullptr;
  m_itemCount = 0;
  m_arraySize = 0;
}
\end{lstlisting}

\subsection{Destructor - Cleaning up the array}
Having a destructor is also very important since we're working with pointers.
Before our data structure is destroyed, we need to make sure that we free
any allocated memory.

\begin{lstlisting}[style=code]
template <typename T>
SmartDynamicArray<T>::~SmartDynamicArray()
{
  if ( m_array != nullptr )
  {
    delete [] m_array;
    m_array = nullptr;
  }
  m_arraySize = 0;
  m_itemCount = 0;
}
\end{lstlisting}

We can also put this functionality into the Clear() function, and call Clear()
from the destructor.

\newpage
\subsection{void AllocateMemory( int size )}
When the array is not in use, the \texttt{m\_array} pointer will be pointing to
\texttt{nullptr}. In this case, we need to allocate space for a new array before
we can begin putting items into it.

\subparagraph{Error checks:}
\begin{itemize}
    \item   If the \texttt{m\_array} pointer is NOT pointing to nullptr, throw an exception.
\end{itemize}

\subparagraph{Functionality:}
\begin{enumerate}
  \item Allocate space for the array via the \texttt{m\_array} pointer.
  \item Assign \texttt{m\_arraySize} to the size passed in as a parameter.
  \item Set \texttt{m\_itemCount} to 0.
\end{enumerate}

\begin{lstlisting}[style=code]
template <typename T>
void SmartDynamicArray<T>::AllocateMemory( int size )
{
  if ( m_array != nullptr )
  {
    throw logic_error( "Can't allocate memory, m_array is already pointing to a memory address!" );
  }

  m_array = new T[ size ];
  m_arraySize = size;
  m_itemCount = 0;
}
\end{lstlisting}

For this function, if the array is already pointing somewhere, we don't want
to just erase all that data and allocate space for a new array. We want to try
to prevent data loss, so it would be better to throw an exception instead
so that the caller can decide how they want to handle it.

\newpage
\subsection{void Resize( int newSize )}
We can't technically resize an array that has been created. We can, however,
allocate more space for a bigger array and copy all the data over and then update
the pointer to point to the new array. And that's exactly how we ``resize'' our dynamic array.

\subparagraph{Error checks:}
\begin{itemize}
    \item   If the new size is smaller than the old size, throw an exception.
\end{itemize}

\subparagraph{Functionality:}
\begin{enumerate}
  \item If \texttt{m\_array} is pointing to nullptr, then just call AllocateMemory instead.
  \item Otherwise:
  \begin{enumerate}
    \item Allocate space for a new array with the new size using a new pointer.
    \item Iterate over all the elements of the old array, copying each element to the new array.
    \item Free space from the old array.
    \item Update the \texttt{m\_array} pointer to point to the new array address.
    \item Update the \texttt{m\_arraySize} to the new size.
  \end{enumerate}
\end{enumerate}

\begin{lstlisting}[style=code]
template <typename T>
void SmartDynamicArray<T>::Resize( int newSize )
{
  if ( newSize < m_arraySize )
  {
    throw logic_error( "Invalid size!" );
  }
  else if ( m_array == nullptr )
  {
    AllocateMemory( newSize );
  }
  else
  {
    T* newArray = new T[newSize]; // New array

    // Copy values over
    for ( int i = 0; i < m_itemCount; i++ )
    {
      newArray[i] = m_array[i];
    }

    delete [] m_array;      // Deallocate old space
    m_array = newArray;     // Update pointer
    m_arraySize = newSize;  // Update size
  }
}
\end{lstlisting}


\hrulefill
\subsection{Updating other functions}

\begin{itemize}
  \item   \texttt{ShiftLeft}:   If \texttt{m\_array} is pointing to nullptr, throw an exception.
  \item   \texttt{ShiftRight}: 
    \begin{itemize}
      \item If \texttt{m\_array} is pointing to nullptr, throw an exception.
      \item If the array is full, call Resize.
    \end{itemize}
  \item   \texttt{PushBack, PushFront, PushAt}: 
    \begin{itemize}
      \item If \texttt{m\_array} is pointing to nullptr call AllocateMemory.
      \item If the array is full, call Resize.
    \end{itemize}
\end{itemize}



